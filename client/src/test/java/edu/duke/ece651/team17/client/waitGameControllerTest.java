package edu.duke.ece651.team17.client;

import edu.duke.ece651.team17.client.controllers.InGameController;
import edu.duke.ece651.team17.client.controllers.LogInController;
import edu.duke.ece651.team17.client.controllers.waitGameController;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Network;
import edu.duke.ece651.team17.shared.Player;
import edu.duke.ece651.team17.shared.Territory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.framework.junit5.Start;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(ApplicationExtension.class)
public class waitGameControllerTest extends ApplicationTest {
    public waitGameController controller;
    FxRobot robot = new FxRobot();

    @Start
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/waitGame.fxml"));
        loader.setControllerFactory(new Callback<Class<?>, Object>() {
            @Override
            public Object call(Class<?> type) {
                // check if the controller is of the MyController type
                if (type == waitGameController.class) {
                    // create a new instance of MyController with the name parameter
                    return new waitGameController(mock(real_client.class),mock(Player.class));
                } else {
                    // use the default controller factory
                    try {
                        return type.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
        VBox root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        controller = loader.getController();
    }
    Board initializeBoard(int round) {
        Board board = new Board();
        String[] name = new String[]{
                "FL", "MS", "AL", "GA", "SC", "TN", "NC", "KY", "WV", "VA", "IL", "IN", "OH", "PA", "MD", "DE", "NJ",
                "NY", "CT", "RI", "MA", "VT", "NH", "ME"
        };
        for (int i = 0; i < 24; i++) {
            Territory t = new Territory(name[i]);
            t.visibility.put(0, 1);
            board.addTerritory(t);
        }
        board.currentRound = round;
        board.playerFoodResource.put(0, 1);
        board.playerTechResource.put(0, 1);
        board.playerTechLV.put(0, 1);
        return board;
    }
    @Test
    public void waitGameTest() throws Exception {
        Network net = mock(Network.class);
        when(net.recvMessage(any())).thenReturn(true);
        Board b = initializeBoard(1);
        when(controller.client.recvBoard()).thenReturn(b);
        doNothing().when(net).sendMessage(any(), any());
        this.controller.client.net = net;
        robot.clickOn("#wB");
    }
}
