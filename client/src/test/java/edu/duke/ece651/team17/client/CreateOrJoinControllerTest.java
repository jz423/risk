package edu.duke.ece651.team17.client;

import edu.duke.ece651.team17.client.controllers.CreateOrJoinController;
import edu.duke.ece651.team17.client.controllers.LogInController;
import edu.duke.ece651.team17.shared.Player;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

@ExtendWith(ApplicationExtension.class)

public class CreateOrJoinControllerTest {
    public CreateOrJoinController controller;
    FxRobot robot = new FxRobot();
    @Start
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/chooseAction.fxml"));
        VBox root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        controller = loader.getController();
    }
    @Test
    public void testCreate() throws Exception {
        robot.clickOn("#createNewGame");
//        assertThrows(Exception.class, ()->robot.clickOn("#LR"));
    }
    @Test
    public void testJoinGame() throws Exception {
        robot.clickOn("#joinGame");
//        assertThrows(Exception.class, ()->robot.clickOn("#LR"));
    }
}
