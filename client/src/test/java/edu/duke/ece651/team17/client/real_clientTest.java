package edu.duke.ece651.team17.client;
import org.junit.jupiter.api.Test;
import edu.duke.ece651.team17.shared.*;
import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.*;
import java.lang.ModuleLayer.Controller;
import java.util.HashSet;
import static org.mockito.Mockito.*;
public class real_clientTest {
    @Test
    public void test_register() throws Exception{
        real_client c = new real_client(0, 0, null);
        Network net1 = mock(Network.class);
        c.net = net1;
        doNothing().when(net1).sendMessage(any(), any());
        when(net1.recvMessage(any())).thenReturn("Account created. Welcome!");
        //c.register();
        when(net1.recvMessage(any())).thenReturn("aa");
        assertThrows(Exception.class,()->c.register());
    }
    @Test
    public void test_login() throws Exception{
        real_client c = new real_client(0, 0, null);
        Network net1 = mock(Network.class);
        c.net = net1;
        doNothing().when(net1).sendMessage(any(), any());
        when(net1.recvMessage(any())).thenReturn("Log in successfull.");
        //c.Login();
        when(net1.recvMessage(any())).thenReturn("aa");
        assertThrows(Exception.class,()->c.Login());
    }
    @Test
    public void test_CreateGame() throws Exception{
        real_client c = new real_client(0, 0, null, null, null);
        Network net1 = mock(Network.class);
        c.net = net1;
        doNothing().when(net1).sendMessage(any(), any());
        c.createGame("3");
        assertThrows(Exception.class,()->c.createGame("0"));
    }
    @Test
    public void test_getGame() throws Exception{
        real_client c = new real_client(0, 0, null);
        Network net1 = mock(Network.class);
        c.net = net1;
        doNothing().when(net1).sendMessage(any(), any());
        ArrayList<Integer>gameList = new ArrayList<Integer>();
        when(net1.recvMessage(any())).thenReturn(gameList);
        ArrayList<Integer> map= c.getGames();
        assertTrue(map.size() == 0);
    }
    @Test
    public void test_leaveGame() throws Exception{
        real_client c = new real_client(0, 0, null);
        Network net1 = mock(Network.class);
        c.net = net1;
        doNothing().when(net1).sendMessage(any(), any());
        c.leaveGame();
    }
    @Test
    public void test_() throws Exception{
        real_client c = new real_client(0, 0, null);
        Network net1 = mock(Network.class);
        c.net = net1;
        doNothing().when(net1).sendMessage(any(), any());
        ArrayList<Integer>gameList = new ArrayList<Integer>();
        when(net1.recvMessage(any())).thenReturn(gameList);
        ArrayList<Integer> map= c.getGames();
        assertTrue(map.size() == 0);
    }
}