package edu.duke.ece651.team17.client;

        import edu.duke.ece651.team17.client.controllers.CreateGameController;
        import edu.duke.ece651.team17.client.controllers.LogInController;
        import edu.duke.ece651.team17.shared.Network;
        import edu.duke.ece651.team17.shared.Player;
        import javafx.fxml.FXMLLoader;
        import javafx.scene.Scene;
        import javafx.scene.layout.VBox;
        import javafx.stage.Stage;
        import org.junit.jupiter.api.Test;
        import org.junit.jupiter.api.extension.ExtendWith;
        import org.testfx.api.FxRobot;
        import org.testfx.framework.junit5.ApplicationExtension;
        import org.testfx.framework.junit5.Start;

        import static org.mockito.ArgumentMatchers.any;
        import static org.mockito.Mockito.*;

@ExtendWith(ApplicationExtension.class)

public class CreateGameControllerTest {
    public CreateGameController controller;
    FxRobot robot = new FxRobot();
    @Start
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/startNew.fxml"));
        VBox root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        controller = loader.getController();
    }
    @Test
    public void testCreate() throws Exception {
        CreateGameController ccc = new CreateGameController(null,null);
        real_client c = mock(real_client.class);
        Network net = mock(Network.class);
        when(net.recvMessage(any())).thenReturn(2);
        doNothing().when(c).createGame(any());
        c.net = net;
        controller.client = c;
        robot.clickOn("#playerNum").write("3");
        robot.clickOn("#startButton");

//        assertThrows(Exception.class, ()->robot.clickOn("#LR"));
    }
}
