package edu.duke.ece651.team17.client;
import edu.duke.ece651.team17.client.controllers.InGameController;
import edu.duke.ece651.team17.client.controllers.seeHistoryController;
import edu.duke.ece651.team17.shared.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

import static org.mockito.Mockito.*;

@ExtendWith(ApplicationExtension.class)
public class InGameControllerTest extends Application {
    public InGameController controller;
    FxRobot robot = new FxRobot();

    Board initializeBoard(int round) {
        Board board = new Board();
        String[] name = new String[]{
                "FL", "MS", "AL", "GA", "SC", "TN", "NC", "KY", "WV", "VA", "IL", "IN", "OH", "PA", "MD", "DE", "NJ",
                "NY", "CT", "RI", "MA", "VT", "NH", "ME"
        };
        for (int i = 0; i < 24; i++) {
            Territory t = new Territory(name[i]);
            t.visibility.put(0, 1);
            board.addTerritory(t);
        }
        board.currentRound = round;
        board.playerFoodResource.put(0, 1);
        board.playerTechResource.put(0, 1);
        board.playerTechLV.put(0, 1);
        return board;
    }

    Player initalizePlayer() throws IOException {
        Player player = mock(Player.class);
        Board b1 = initializeBoard(0);
        Board b2 = initializeBoard(1);
        Board b3 = initializeBoard(2);
        Board b4 = initializeBoard(3);
        Board b5 = initializeBoard(4);
        Board b = initializeBoard(0);
        b.boardHistory.add(convertBoard2Byte(b1));
        b.boardHistory.add(convertBoard2Byte(b2));
        b.boardHistory.add(convertBoard2Byte(b3));
        b.boardHistory.add(convertBoard2Byte(b4));
        b.boardHistory.add(convertBoard2Byte(b5));
        player.myBoard = b;
        player.playerID = 0;
        return player;
    }

    public byte[] convertBoard2Byte(Board board) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(board);
        byte[] byteArray = outputStream.toByteArray();
        objectOutputStream.close();
        return byteArray;
    }

    @Start
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/Game.fxml"));
        loader.setControllerFactory(new Callback<Class<?>, Object>() {
            @Override
            public Object call(Class<?> type) {
                // check if the controller is of the MyController type
                if (type == InGameController.class) {
                    // create a new instance of MyController with the name parameter
                    try {
                        return new InGameController(initalizePlayer(), mock(real_client.class));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    // use the default controller factory
                    try {
                        return type.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
        VBox root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        controller = loader.getController();
    }

    @Test
    public void testClick() {

        String[] name = new String[]{
                "FL", "MS", "AL", "GA", "SC", "TN", "NC", "KY", "WV", "VA", "IL", "IN", "OH", "PA", "MD", "DE", "NJ",
                "NY", "CT", "RI", "MA", "VT", "NH", "ME"
        };
        for (int i = 0; i < 24; i++) {
            robot.clickOn("#" + name[i]);
        }
    }

    @Test
    public void testSeeHistory2() {
        Player p = new Player(1, 1, "");
        p.playerID = 1;
        InGameController c = new InGameController(null, null, null);
        this.controller.player.myBoard.currentRound = 5;
        robot.clickOn("#PA");
        robot.clickOn("#seeHistory");
    }

    @Test
    public void testRejoin() {
        controller.rejoin();
    }

    @Test
    public void updateMilitary() throws Exception {
        robot.clickOn("#ME");
        robot.doubleClickOn("#c1").write("2");
        Board board = spy(Board.class);
        String[] name = new String[]{
                "FL", "MS", "AL", "GA", "SC", "TN", "NC", "KY", "WV", "VA", "IL", "IN", "OH", "PA", "MD", "DE", "NJ",
                "NY", "CT", "RI", "MA", "VT", "NH", "ME"
        };
        for (int i = 0; i < 24; i++) {
            Territory t = new Territory(name[i]);
            t.visibility.put(0, 1);
            t.setOwnerID(0);
            t.setDefenderMillitaryPower(0, 10);
            board.addTerritory(t);
        }

        board.currentRound = 1;
        board.playerFoodResource = new HashMap<>();
        board.playerTechResource = new HashMap<>();
        board.playerTechLV = new HashMap<>();
        board.playerFoodResource.put(0, 10000);
        board.playerTechResource.put(0, 10000);
        board.playerTechLV.put(0, 10000);

        this.controller.player.myBoard = board;
        ArrayList<Command> list = mock(ArrayList.class);
        this.controller.player.commands = list;
        when(list.add(any(Command.class))).thenReturn(true);
        System.out.println("We have neighourbs: " + this.controller.player.myBoard.getTerritorys().size());
        robot.clickOn("#ulv0");

    }
    @Test
    public void test_confirmButton1() throws Exception {
        Board board = spy(Board.class);
        String[] name = new String[]{
                "FL", "MS", "AL", "GA", "SC", "TN", "NC", "KY", "WV", "VA", "IL", "IN", "OH", "PA", "MD", "DE", "NJ",
                "NY", "CT", "RI", "MA", "VT", "NH", "ME"
        };
        for (int i = 0; i < 24; i++) {
            Territory t = new Territory(name[i]);
            t.visibility.put(0, 1);
            t.setOwnerID(0);
            t.setDefenderMillitaryPower(0, 10);
            board.addTerritory(t);
        }

        board.currentRound = 1;
        board.playerFoodResource = new HashMap<>();
        board.playerTechResource = new HashMap<>();
        board.playerTechLV = new HashMap<>();
        board.playerFoodResource.put(0, 10000);
        board.playerTechResource.put(0, 10000);
        board.playerTechLV.put(0, 10000);
        doNothing().when(this.controller.player).playOneRound(any());
        this.controller.player.myBoard = board;
        robot.clickOn("#ME");
        robot.clickOn("#actionSelect");
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.ENTER).release(KeyCode.ENTER);
        Board b = initializeBoard(2);
        when(this.controller.client.recvBoard()).thenReturn(b);
        robot.clickOn("#confirmAction");
    }
    @Test
    public void test_confirmButton2() throws Exception {
        Board board = spy(Board.class);
        String[] name = new String[]{
                "FL", "MS", "AL", "GA", "SC", "TN", "NC", "KY", "WV", "VA", "IL", "IN", "OH", "PA", "MD", "DE", "NJ",
                "NY", "CT", "RI", "MA", "VT", "NH", "ME"
        };
        for (int i = 0; i < 24; i++) {
            Territory t = new Territory(name[i]);
            t.visibility.put(0, 1);
            t.setOwnerID(1);
            t.setDefenderMillitaryPower(0, 10);
            board.addTerritory(t);
        }

        board.currentRound = 1;
        board.playerFoodResource = new HashMap<>();
        board.playerTechResource = new HashMap<>();
        board.playerTechLV = new HashMap<>();
        board.playerFoodResource.put(0, 10000);
        board.playerTechResource.put(0, 10000);
        board.playerTechLV.put(0, 10000);
        doNothing().when(this.controller.player).playOneRound(any());
        this.controller.player.myBoard = board;
        robot.clickOn("#ME");
        robot.clickOn("#actionSelect");
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        robot.press(KeyCode.ENTER).release(KeyCode.ENTER);
        Board b = initializeBoard(2);
        when(this.controller.client.recvBoard()).thenReturn(b);
        this.controller.player.gameOver = true;
        Network net = mock(Network.class);
        doNothing().when(net).sendMessage(any(), any());
        this.controller.client.net = net;
        robot.clickOn("#confirmAction");
        robot.clickOn("#watch");
    }
}
