package edu.duke.ece651.team17.client;

import edu.duke.ece651.team17.client.controllers.LogInController;
import edu.duke.ece651.team17.client.controllers.seeHistoryController;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Player;
import edu.duke.ece651.team17.shared.Territory;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.framework.junit5.Start;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(ApplicationExtension.class)

public class SeeHistoryControllerTest extends Application {
    public seeHistoryController controller;
    FxRobot robot = new FxRobot();
    Board initializeBoard(int round){
        Board board = new Board();
        String[]name = new String[]{
                "FL","MS","AL","GA","SC","TN","NC","KY","WV","VA","IL","IN","OH","PA","MD","DE","NJ",
                "NY","CT","RI","MA","VT","NH","ME"
        };
        for(int i = 0; i < 24; i++){
            Territory t = new Territory(name[i]);
            board.addTerritory(t);
        }
        board.currentRound = round;
        board.playerFoodResource.put(0,1);
        board.playerTechResource.put(0,1);
        board.playerTechLV.put(0,1);
        return board;
    }

    Player initalizePlayer() throws IOException {
        Player player = mock(Player.class);
        Board b1 = initializeBoard(0);
        Board b2 = initializeBoard(1);
        Board b3 = initializeBoard(2);
        Board b4 = initializeBoard(3);
        Board b5 = initializeBoard(4);
        Board b = new Board();
        b.boardHistory.add(convertBoard2Byte(b1));
        b.boardHistory.add(convertBoard2Byte(b2));
        b.boardHistory.add(convertBoard2Byte(b3));
        b.boardHistory.add(convertBoard2Byte(b4));
        b.boardHistory.add(convertBoard2Byte(b5));
        player.myBoard = b;
        return player;
    }
    @Start
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/seeHistory.fxml"));
        loader.setControllerFactory(new Callback<Class<?>, Object>() {
            @Override
            public Object call(Class<?> type) {
                // check if the controller is of the MyController type
                if (type == seeHistoryController.class) {
                    // create a new instance of MyController with the name parameter
                    try {
                        return new seeHistoryController(initalizePlayer(), mock(real_client.class));
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    } catch (ClassNotFoundException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    // use the default controller factory
                    try {
                        return type.newInstance();
                    } catch (InstantiationException | IllegalAccessException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        });
        VBox root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        controller = loader.getController();
    }

    public byte[] convertBoard2Byte(Board board) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(board);
        byte[] byteArray = outputStream.toByteArray();
        objectOutputStream.close();
        return byteArray;
    }

    @Test
    public void testGoBack() throws Exception {
        robot.clickOn("#NY");
        robot.clickOn("#ME");
        robot.clickOn("#goBack");
    }
    @Test
    public void testGoNext() throws Exception {
        robot.clickOn("#ME");
        robot.clickOn("#goNext");
    }


}

