package edu.duke.ece651.team17.client;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

import edu.duke.ece651.team17.client.controllers.LogInController;
import edu.duke.ece651.team17.shared.Network;
import edu.duke.ece651.team17.shared.Player;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.ApplicationTest;
import org.testfx.framework.junit5.Start;

import java.io.IOException;


@ExtendWith(ApplicationExtension.class)
public class LoginControllerTest extends ApplicationTest {
    public LogInController controller;
    FxRobot robot = new FxRobot();
    @Start
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/logIN.fxml"));
        VBox root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        controller = loader.getController();
    }
    @Test
    public void testLogin() throws Exception {
        real_client c = mock(real_client.class);
        Player p = mock(Player.class);
        this.controller.player = p;
        doNothing().when(c).Login();
        this.controller.client = c;
        robot.clickOn("#name").write("testuser");
        robot.clickOn("#password").write("testpassword");
        robot.clickOn("#LR");
//        assertThrows(Exception.class, ()->robot.clickOn("#LR"));
    }
    @Test
    public void testRegister() throws Exception {
     
        real_client c = mock(real_client.class);
        Player p = mock(Player.class);
        this.controller.player = p;
        doNothing().when(c).register();

        this.controller.client = c;
        robot.clickOn("#name").write("testuser");
        robot.clickOn("#password").write("testpassword");
        robot.clickOn("#RS");

    }
    public void stop() throws Exception {
        FxToolkit.cleanupStages();
    }
}
