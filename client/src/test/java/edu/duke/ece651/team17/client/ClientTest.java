package edu.duke.ece651.team17.client;

import org.junit.jupiter.api.Test;
import edu.duke.ece651.team17.shared.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.io.*;
import java.util.HashSet;
import static org.mockito.Mockito.*;
/**The test of client*/
public class ClientTest extends Network {
  public Board test_board(){
    Board b=new Board();
    Territory one=new Territory("A");
    Territory two=new Territory("B");
    Territory three=new Territory("C");
    one.addNeighbour(two);
    two.addNeighbour(one);
    //two.addNeighbour(three);
    //three.addNeighbour(two);
    // one.addNeighbour(three);
    //three.addNeighbour(one);
    b.addTerritory(one);
    b.addTerritory(two);
    // b.addTerritory(three);
    one.setOwnerID(1);
    two.setOwnerID(2);
    //three.setOwnerID(2);
    ArrayList<Integer> ids=new ArrayList<>();
    for(int i=1;i<=2;i++){
      ids.add(i);
    }
    one.initializeProjectedPower(ids);
    two.initializeProjectedPower(ids);
    //three.initializeProjectedPower(ids);
    one.setDefenderMillitaryPower(20);
    one.setDefenderMillitaryPower(20);
    return b;
  }
  public Client initialClient(String inputData, OutputStream bytes) throws Exception{
    PrintStream output = new PrintStream(bytes, true);
    BufferedReader input = new BufferedReader(new StringReader(inputData)); 
    Client thisClient=new Client(0,4444,"127.0.0.1", input, output);
    return thisClient;
}
  
  @Test
  void test_makeplay_board()throws Exception{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    Client x = initialClient("", bytes);
    Board board = new Board();
    Territory t1 = new Territory("AA");
    boolean a = t1.setDefenderMillitaryPower(10);
    t1.setOwnerID(1);
    Territory t2 = new Territory("BB");
    a = t2.setDefenderMillitaryPower(20);
    t2.setOwnerID(2);
    t1.addNeighbour(t2);
    t2.addNeighbour(t1);
    board.addTerritory(t1);
    board.addTerritory(t2);
    String display=x.makeServerBoard(board);
    StringBuilder sb = new StringBuilder();
    sb.append("Player ID: 1\n-----------------\n10 units in AA (next to: BB)\n\nPlayer ID: 2\n-----------------\n20 units in BB (next to: AA)\n\n");
    assertEquals(sb.toString(), display);
  }
  
  @Test
  void testRecvmsg() throws Exception{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Client thisClient=initialClient( "", bytes);
        Network net1 = mock(Network.class);
        
        Network net2 = mock(Network.class);
        when(net1.recvMessage(any())).thenReturn(true);
        doThrow(Exception.class).when(net2).recvMessage(any());
      
        thisClient.net = net1;
        thisClient.recvReadymsg();
        thisClient.recvOvermsg();
        thisClient.net = net2;
        thisClient.recvReadymsg();
        thisClient.recvOvermsg();
    }
  @Test
  public void test_check_win(){
    Board myBoard=test_board();
    Client x=new Client(1,4444,"127.0.0.1",new BufferedReader(new InputStreamReader(System.in)),new PrintStream(System.out));
    x.checkWin(myBoard);
    myBoard.getTerritorys().get(1).setOwnerID(2);
  }
  @Test
  public void test_valid_watch_game() throws IOException{
    String x1="y";
    ByteArrayInputStream bytes=new ByteArrayInputStream(x1.getBytes());
    System.setIn(bytes);
    Client x=new Client(1,4444,"127.0.0.1",new BufferedReader(new InputStreamReader(System.in)),new PrintStream(System.out));
    x.watchGame();
    String x2="n";
    ByteArrayInputStream bytes2=new ByteArrayInputStream(x2.getBytes());
    System.setIn(bytes2);
    Client y=new Client(1,4444,"127.0.0.1",new BufferedReader(new InputStreamReader(System.in)),new PrintStream(System.out));
    y.watchGame();
  }
  @Test
  public void test_command_entry() throws Exception {
    Board myBoard = test_board();
    Territory territory = new Territory("E");
    territory.setOwnerID(1);
    Territory one = myBoard.getTerritorys().get(0);
    Territory two = myBoard.getTerritorys().get(1);
    territory.setDefenderMillitaryPower(100);
    territory.addNeighbour(one);
    one.addNeighbour(territory);
    myBoard.addTerritory(territory);
    String string = "M" + "\n";
    string += "x" + "\n";
    string += "E A 10" + "\n";
    string += "A E 10" + "\n";
    string += "D" + "\n";
    string += "F" + "\n";
    ByteArrayInputStream bs = new ByteArrayInputStream(string.getBytes());
    System.setIn(bs);
    Client a = new Client(1, 4444, "127.0.0.1", new BufferedReader(new InputStreamReader(System.in)), new PrintStream(System.out));
    a.commandEntry(myBoard);
  }

    @Test
  void testRecvBoard() throws Exception{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Client thisClient=initialClient( "", bytes);
        Network net1 = mock(Network.class);
        
        Network net2 = mock(Network.class);
        Board b= new Board();
        when(net1.recvMessage(any())).thenReturn(b);
        doThrow(Exception.class).when(net2).recvMessage(any());
        Board recv=null;
        thisClient.net = net1;
        recv=thisClient.recvBoard();
        assertEquals(recv,b);
        thisClient.net = net2;
        recv=thisClient.recvBoard();
        assertEquals(recv,null);

    }
    @Test
  void testRecvID() throws Exception{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Client thisClient=initialClient( "", bytes);
        Network net1 = mock(Network.class);
        when(net1.recvMessage(any())).thenReturn(2);
        doNothing().when(net1).sendMessage(any(), any());
        thisClient.net = net1;
        thisClient.recvID();
       
    }
    @Test
    void testPlaceUnit() throws Exception{
          ByteArrayOutputStream bytes = new ByteArrayOutputStream();
          Client thisClient=initialClient( "daw\n3\n3\n3\n10\n1\n", bytes);
          Network net1 = mock(Network.class);
          doNothing().when(net1).sendMessage(any(), any());
          thisClient.net = net1; 
          ArrayList<Territory> tr=new ArrayList<Territory>();
          tr.add(new Territory("a"));
          tr.add(new Territory("b"));
          thisClient.placeUnit(tr,10);
          assertEquals(6,tr.get(0).getDefenderMillitaryPower().getMinPower());
          assertEquals(4,tr.get(1).getDefenderMillitaryPower().getMinPower());
      }
    
      @Test
    void testPickTerritory() throws Exception{
          ByteArrayOutputStream bytes = new ByteArrayOutputStream();
          Client thisClient=initialClient( "daw\n100\n", bytes);
          Network net1 = mock(Network.class);
          doNothing().when(net1).sendMessage(any(), any());
          when(net1.recvMessage(any())).thenReturn(2);
          doNothing().when(net1).sendMessage(any(), any());
          thisClient.net = net1;
          thisClient.recvID();
          Board b= new Board();  
          Territory t=new Territory("a");
          t.setOwnerID(2);
          b.addTerritory(t);
          b.addTerritory(new Territory("b"));
          thisClient.pickTerritory(b);
          assertEquals(100,t.getDefenderMillitaryPower().getMinPower());
      }
      @Test
      public void test_win_lose(){
          Board board=test_board();
          ArrayList<Territory> ts=board.getTerritorys();
          Territory one=ts.get(1);
          Territory two=ts.get(1);
          Client x=new Client(2,4444,"127.0.0.1",new BufferedReader(new InputStreamReader(System.in)),new PrintStream(System.out));
          x.checkLose();
      }
    @Test
    void testPlayOneRoundWin() throws Exception {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Client thisClient = initialClient("daws\nM\nA C 10\nM\nB C 10\nA\nB A 100\nA\nB A 1\nD\n", System.out);
        Network net1 = mock(Network.class);
        when(net1.recvMessage(any())).thenReturn(2);
        doNothing().when(net1).sendMessage(any(), any());
        thisClient.net = net1;
        thisClient.recvID();
        Network net2 = mock(Network.class);
        doNothing().when(net2).sendMessage(any(), any());
        thisClient.net = net2;
        Territory one = new Territory("A");
        Territory two = new Territory("B");
        Territory three = new Territory("C");
        Board b=new Board();

        one.addNeighbour(two);
        two.addNeighbour(one);
        two.addNeighbour(three);
        three.addNeighbour(two);
        one.addNeighbour(three);
        three.addNeighbour(one);
        b.addTerritory(one);
        b.addTerritory(two);
        b.addTerritory(three);
        one.setOwnerID(1);
        two.setOwnerID(2);
        three.setOwnerID(2);
        ArrayList<Integer> ids = new ArrayList<>();
        when(net2.recvMessage(any())).thenReturn(b);
        for (int i = 1; i <= 2; i++) {
            ids.add(i);
        }
        one.initializeProjectedPower(ids);
        two.initializeProjectedPower(ids);
        three.initializeProjectedPower(ids);
        one.setDefenderMillitaryPower(20);
        two.setDefenderMillitaryPower(20);
        thisClient.recvBoard();
        Board b2=new Board();
        b2.addTerritory(two);
        Network net3 = mock(Network.class);
        when(net3.recvMessage(any())).thenReturn(b2);
        doNothing().when(net3).sendMessage(any(), any());
        thisClient.net = net3;
        thisClient.playOneRound();
    }

    @Test
    void testPlayOneRoundLose() throws Exception {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        Client thisClient = initialClient("daws\nM\nA C 10\nM\nB C 10\nA\nB A 100\nA\nB A 1\nD\n", System.out);
        Network net1 = mock(Network.class);
        when(net1.recvMessage(any())).thenReturn(2);
        doNothing().when(net1).sendMessage(any(), any());
        thisClient.net = net1;
        thisClient.recvID();
        Network net2 = mock(Network.class);
        doNothing().when(net2).sendMessage(any(), any());
        thisClient.net = net2;
        Territory one = new Territory("A");
        Territory two = new Territory("B");
        Territory three = new Territory("C");
        Board b=new Board();

        one.addNeighbour(two);
        two.addNeighbour(one);
        two.addNeighbour(three);
        three.addNeighbour(two);
        one.addNeighbour(three);
        three.addNeighbour(one);
        b.addTerritory(one);
        b.addTerritory(two);
        b.addTerritory(three);
        one.setOwnerID(1);
        two.setOwnerID(2);
        three.setOwnerID(2);
        ArrayList<Integer> ids = new ArrayList<>();
        when(net2.recvMessage(any())).thenReturn(b);
        for (int i = 1; i <= 2; i++) {
            ids.add(i);
        }
        one.initializeProjectedPower(ids);
        two.initializeProjectedPower(ids);
        three.initializeProjectedPower(ids);
        one.setDefenderMillitaryPower(20);
        two.setDefenderMillitaryPower(20);
        thisClient.recvBoard();
        Board b2=new Board();
        b2.addTerritory(one);
        Network net3 = mock(Network.class);
        when(net3.recvMessage(any())).thenReturn(b2);
        doNothing().when(net3).sendMessage(any(), any());
        thisClient.net = net3;
        thisClient.playOneRound();
    }

}
