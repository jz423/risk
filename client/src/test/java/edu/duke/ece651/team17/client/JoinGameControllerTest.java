package edu.duke.ece651.team17.client;

import edu.duke.ece651.team17.client.controllers.CreateGameController;
import edu.duke.ece651.team17.client.controllers.JoinGameController;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Network;
import edu.duke.ece651.team17.shared.Player;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.testfx.api.FxRobot;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

import java.util.ArrayList;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(ApplicationExtension.class)

public class JoinGameControllerTest {
    public JoinGameController controller;
    FxRobot robot = new FxRobot();
    @Start
    public void start(Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/joinGame.fxml"));
        VBox root = loader.load();
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
        controller = loader.getController();
    }
    @Test
    public void testCreate() throws Exception {

        // Player p = mock(Player.class);
        // real_client c = mock(real_client.class);
        // Network net = mock(Network.class);
        // when(c.recvBoard()).thenReturn(new Board());
        // ArrayList<Integer>list = new ArrayList<Integer>();
        // list.add(1);
        // list.add(2);
        // when(c.getGames()).thenReturn(list);
        // doNothing().when(c).chooseGame(anyInt());
        // c.net = net;
        // controller.player = p;
        // controller.client = c;
        // controller.games = list;
        // for(int i = 0; i < 2;i ++){
        //     controller.gameChoose.getItems().add(i);
        // }
        // robot.clickOn("#gameChoose");
        // robot.press(KeyCode.DOWN).release(KeyCode.DOWN);
        // robot.press(KeyCode.ENTER).release(KeyCode.ENTER);
        // robot.clickOn("#confirmChoose");

//        assertThrows(Exception.class, ()->robot.clickOn("#LR"));
    }
}
