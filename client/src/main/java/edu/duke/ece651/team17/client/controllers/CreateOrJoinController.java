package edu.duke.ece651.team17.client.controllers;
import edu.duke.ece651.team17.client.real_client;
import edu.duke.ece651.team17.shared.Player;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.HashMap;

public class CreateOrJoinController {
    @FXML
    Button createNewGame;
    @FXML
    Button joinGame;
    @FXML
    Button exit2;
    public real_client client;
    public Player player;
    public CreateOrJoinController(){}
    public CreateOrJoinController(real_client client, Player player){
        this.client=client;
        this.player=player;
    }
    @FXML
    public void createGameHandler(ActionEvent event) throws Exception{
        Object source=event.getSource();
        if(source instanceof Button){
              //page jumps to  create game page
            Scene scene=createNewGame.getScene();
           // scene.setRoot(null);
            FXMLLoader loader= new FXMLLoader(getClass().getResource("/UI/startNew.fxml"));
            HashMap<Class<?>, Object> controllers = new HashMap<>();
            controllers.put(CreateGameController.class,new CreateGameController(this.client,this.player) );
            loader.setControllerFactory((c) -> {
                return controllers.get(c);
            });
            loader.load();
            scene.setRoot(loader.getRoot());
        }
    }
    @FXML
    public void joinGameHandler(ActionEvent event) throws Exception{
        Object source=event.getSource();
        if(source instanceof Button){
              //page jumps to join game page
            Scene scene=joinGame.getScene();
            //scene.setRoot(null);
            FXMLLoader loader= new FXMLLoader(getClass().getResource("/UI/joinGame.fxml"));
            HashMap<Class<?>, Object> controllers = new HashMap<>();
            controllers.put(JoinGameController.class,new JoinGameController(this.client,this.player) );
            loader.setControllerFactory((c) -> {
                return controllers.get(c);
            });
            loader.load();
            scene.setRoot(loader.getRoot());
        }
    }
//    @FXML
//    public void backToLogIN(ActionEvent event) throws Exception{
//        Object source=event.getSource();
//        if(source instanceof Button){
//            //page jumps to log IN page.
//            Scene scene=exit2.getScene();
//            //scene.setRoot(null);
//            FXMLLoader loader= new FXMLLoader(getClass().getResource("/UI/logIN.fxml"));
//            HashMap<Class<?>, Object> controllers = new HashMap<>();
//            this.client.leaveGame();
//            this.client.socket.close();
//            controllers.put(LogInController.class,new LogInController(this.client,this.player) );
//            loader.setControllerFactory((c) -> {
//                return controllers.get(c);
//            });
//            loader.load();
//            scene.setRoot(loader.getRoot());
//        }
//    }
}
