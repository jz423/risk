package edu.duke.ece651.team17.client.controllers;

import edu.duke.ece651.team17.client.real_client;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Command;
import edu.duke.ece651.team17.shared.Player;
import edu.duke.ece651.team17.shared.Territory;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.*;
import javafx.event.*;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;
public class waitGameController implements Initializable {
    public real_client client;
    public Player player;
    public waitGameController(real_client client,Player player){
        this.client=client;
        this.player=player;
    }
    public waitGameController(){}
    @FXML
    Button wB;

    @FXML
    public void joinCreatedGame(ActionEvent event) throws Exception{
       
        Object ae=event.getSource();
        if(ae instanceof Button){
            Command command=new Command();
            command.operationType="checkFULL";
            this.client.net.sendMessage(this.client.socket,command);
            this.client.net.sendMessage(this.client.socket, this.client.myGameID);
            boolean isFULL=this.client.net.recvMessage(this.client.socket);
            boolean isReady=this.client.net.recvMessage(this.client.socket);
            player.myBoard=this.client.recvBoard();
            
           
            this.client.player=player;
            if(isFULL&&isReady){
               
                HashMap<Class<?>, Object> controllers = new HashMap<>();
                controllers.put(InGameController.class,new InGameController(player,client));
               
                FXMLLoader loader=new FXMLLoader(getClass().getResource("/UI/Game.fxml"));
                loader.setControllerFactory((c) -> {
                    return controllers.get(c);
                });
                loader.load();
               InGameController igc = (InGameController)controllers.get(InGameController.class);
               igc.rejoin();
              
                wB.getScene().setRoot(loader.getRoot());
            }else if(isFULL&&!isReady){
                HashMap<Class<?>, Object> controllers = new HashMap<>();
                controllers.put(PlaceUnitController.class,new PlaceUnitController(client,player));
                
                FXMLLoader loader=new FXMLLoader(getClass().getResource("/UI/placeunit.fxml"));
                loader.setControllerFactory((c) -> {
                    return controllers.get(c);
                });
                
                loader.load();
               
                wB.getScene().setRoot(loader.getRoot());
            }else{
                throw new Exception("The Game is loading, Please Wait!");
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
