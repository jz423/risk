package edu.duke.ece651.team17.client.controllers;
import edu.duke.ece651.team17.client.controllers.*;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;


import edu.duke.ece651.team17.client.real_client;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Player;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class WatchGameController {
    public Player player;
    public real_client client;
    HashMap<Integer,Integer> colormap;
    public WatchGameController(Player player, real_client client,HashMap<Integer,Integer> colormap){
        this.player = player;
        this.client = client;
        this.colormap=colormap;
    }

    @FXML
    Button exit4;
    @FXML
    Button watch;
    
@FXML
    public void watchGame(ActionEvent event) throws Exception{
        this.player.isWatching=true;
        Object source=event.getSource();
        if(source instanceof Button){
            Scene scene=watch.getScene();
            // scene.setRoot(null);
            FXMLLoader loader=new FXMLLoader(getClass().getResource("/UI/Game.fxml"));
            // scene.setRoot(loader.load());
            HashMap<Class<?>, Object> controllers = new HashMap<>();
            controllers.put(InGameController.class, new InGameController(player, client,this.colormap));
            loader.setControllerFactory((c) -> {
                return controllers.get(c);
            });
            loader.load();
            InGameController igc = (InGameController)controllers.get(InGameController.class);
            Platform.runLater(() -> {
                scene.setRoot(loader.getRoot());
            });
        
            // Execute igc.watch() in a separate thread
            Task<Void> task = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    igc.watch();
                    return null;
                }
            };
            new Thread(task).start();
        }

    }
}
