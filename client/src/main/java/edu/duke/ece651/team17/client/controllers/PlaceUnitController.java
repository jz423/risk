package edu.duke.ece651.team17.client.controllers;

import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Player;
import edu.duke.ece651.team17.shared.Territory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import edu.duke.ece651.team17.client.real_client;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

public class PlaceUnitController implements Initializable {
    public Player player;
    public real_client client;
    public ArrayList<Territory> territories;

    public PlaceUnitController(real_client client, Player player) {
        this.player = player;
        this.territories = new ArrayList<>();
        this.client = client;
        this.player.socket = this.client.socket;
    }

    @FXML
    Label tName;
    @FXML
    ChoiceBox choice1;
    @FXML
    Button confirmOne;
    @FXML
    Button exit3;

    // public void setTerritories(ArrayList<Territory> territories){
    // this.territories.addAll(territories);
    // }
    @FXML
    private void placeConfirm(ActionEvent event) throws Exception {
        Object source = event.getSource();
        System.out.println("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP");
        if (source instanceof Button) {
            System.out.println("PPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPPP");
            ArrayList<String> names=new ArrayList<>();
            for(Territory t: territories){
                names.add(t.name);
            }
            System.out.println("exist!!!!!!!"+names.contains(tName.getText()));
            System.out.println("tname is:    "+tName);
            int selectedIndex = choice1.getSelectionModel().getSelectedIndex();
            int unitNumber = (Integer) choice1.getItems().get(selectedIndex);
            int total_number = this.player.total_units_in_the_game;
            boolean found = false;
            int i = 0;
            while (i < territories.size()) {
                Territory t = territories.get(i);
                //this.player.net.sendMessage(this.player.socket, "error");
                System.out.println("hello");
                if (t.name.equals(tName.getText())) {
                    System.out.println("llllllllllllllllllllllllllllllllll");
                    t.setDefenderMillitaryPower(unitNumber);
                    total_number -= unitNumber;
                    this.player.total_units_in_the_game = total_number;
                    System.out.println("total number is:"+total_number);
                    if (total_number > 0) {
                        i = (i + 1) % territories.size();
                        tName.setText(territories.get(i).name);
                        ObservableList<Integer> units = FXCollections.observableArrayList();
                        for (int j = 1; j <= total_number; j++) {
                            units.add(j);
                        }
                        choice1.setItems(units);
                        System.out.println("do new choice "+choice1.getItems().size());
                       // this.player.net.sendMessage(this.player.socket, "error");
                        break;
                    } else {
                      //  this.player.net.sendMessage(this.player.socket, this.territories);
                        this.player.net.sendMessage(this.player.socket, this.territories);
                        // jump to game start page;
                        Scene scene = confirmOne.getScene();
                        // scene.setRoot(null);
                        FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/Game.fxml"));
                        HashMap<Class<?>, Object> controllers = new HashMap<>();
                        System.out.println("HashMap<Class<?>, Objsdghsdhsdect> controllers = new HashMap<>();");
                        // this.player.myBoard = this.client.recvBoard();
                        Object b = this.client.net.recvMessage(client.socket);
                        this.player.myBoard = this.client.recvBoard();
                        for(Territory s:this.player.myBoard.getTerritorys()){
                            System.out.println("This territory is owned"+s.getOwnerID()+"by"+this.player.playerID);
                        }
                        // System.out.println(b == );
                        this.client.player = this.player;
                        controllers.put(InGameController.class, new InGameController(player, client));
                        loader.setControllerFactory((c) -> {
                            return controllers.get(c);
                        });
                        loader.load();
                        InGameController igc = (InGameController)controllers.get(InGameController.class);
                        igc.initialize1();
                        scene.setRoot(loader.getRoot());
                        // Jump to Game page!!!!!1
                        /**
                         * 
                         * FXMLLoader loader= new
                         * FXMLLoader(getClass().getResource("/UI/startNew.fxml"));
                         * HashMap<Class<?>, Object> controllers = new HashMap<>();
                         * controllers.put(CreateGameController.class,new
                         * CreateGameController(this.client,this.player) );
                         * loader.setControllerFactory((c) -> {
                         * return controllers.get(c);
                         * });
                         * loader.load();
                         * scene.setRoot(loader.getRoot());
                         */
                    }
                    break;
                }
                i += 1;
            }
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            player.playerID = client.playerID;
            System.out.println("This player id is : "+player.playerID);
            System.out.println("Place is callled!!!!!!!");
            this.territories = player.pickTerritory(player.myBoard);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        System.out.println("success!!!!!!!!!!" + territories.size());
        System.out.println("Board size::::::::" + player.myBoard.getTerritorys().size());
        if (territories.size() > 0) {
            tName.setText(territories.get(0).name);
            ObservableList<Integer> units = FXCollections.observableArrayList(0);
            choice1.setItems(units);
            for (int i = 0; i <= player.total_units_in_the_game; i++) {
                choice1.getItems().add(i);
            }
        }
    }

}
