package edu.duke.ece651.team17.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.*;

import edu.duke.ece651.team17.client.controllers.waitGameController;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Command;
import edu.duke.ece651.team17.shared.Network;
import edu.duke.ece651.team17.shared.Territory;
import edu.duke.ece651.team17.shared.*;
import javafx.fxml.FXMLLoader;

public class real_client {
  // private Game game;
  public int myGameID;
  public Socket socket;
  private int port;
  private Board myBoard;
  
  public int playerID;
  private String address;
  private String playerName;
  private boolean hasWinGame = false;
  private boolean readyToplay = false;
  private boolean gameOver = false;
  private boolean isLogin = false;
  private HashMap<Integer, Player> players;
  private ArrayList<Territory> startTerritory;
  private PrintStream out;
  private BufferedReader reader;
  public Network net = new Network();
  public Player player;
  private final String login_successful = "Log in successfull.";
  private final String register_successful = "Account created. Welcome!";
  private final String join_full = "Game is already full. Please try joining a different game.";
  public String accountName;
  public String passWord;

  /** The constructor for client side of RISK GAME */
  public real_client(int playerID, int port, String address, BufferedReader inputSource, PrintStream out) {
    this.myBoard = new Board();
    this.playerID = playerID;
    this.port = port;
    this.address = address;
    this.reader = inputSource;
    this.out = out;
    this.players = new HashMap<>();
    
  }

  public real_client(int playerID, int port, String address) {
    System.out.println("You have load successfull!!!!!!!");
    this.playerID = playerID;
    this.myBoard = new Board();
    this.port = port;
    this.address = address;
    // this.players=new HashMap<>();
  }

    public real_client() {

    }

    /**
   * User has to register before log in
   * 
   * @throws Exception
   */
  public void register() throws Exception {
    Command rCommand = new Command();
    rCommand.operationType = "Register";
    rCommand.username = this.accountName;
    rCommand.password = this.passWord;
    // if(rCommand.username.equals("")||rCommand.password.equals("")){
    // throw new Exception("Invalid register ,try again!!!!!!!");
    // }
    while (true) {
      net.sendMessage(socket, rCommand);
      String recv = net.recvMessage(socket);
      // this.playerID=net.recvMessage(socket);
      System.out.println("recv is:" + recv);
      if (recv.contains(this.register_successful)) {
        this.playerID = net.recvMessage(socket);
        this.isLogin = true;
        break;
      } else {
        throw new Exception(recv);
      }
    }
  }

  /**
   * User has to Login before choose Game
   */
  public void Login() throws Exception {
    System.out.println("log in called!!!!");
    Command rCommand = new Command();
    rCommand.operationType = "Login";
    rCommand.username = this.accountName;
    rCommand.password = this.passWord;
    while (true) {
      net.sendMessage(socket, rCommand);
      String recv = net.recvMessage(socket);
      // this.playerID=net.recvMessage(socket);
      if (recv.equals(this.login_successful)) {
        this.playerID = net.recvMessage(socket);
        this.isLogin = true;
        break;
      } else {
        System.out.println("what do you say?" + recv);
        throw new Exception(recv);
      }
    }
  }

  public boolean queryGame() throws Exception {
    Command rCommand = new Command();
    rCommand.operationType = "checkFULL";
    this.net.sendMessage(socket, rCommand);
    this.net.sendMessage(this.socket, this.myGameID);
    boolean isFULL = this.net.recvMessage(this.socket);
    return isFULL;
  }

  public void createGame(String playerNums) throws Exception {
    int playerNum = Integer.parseInt(playerNums);
    boolean good = playerNum >= 2 && playerNum <= 4;
    if (good) {
      Command rCommand = new Command();
      rCommand.operationType = "Create";
      rCommand.playerNum = playerNum;
      net.sendMessage(socket, rCommand);
    } else {
      throw new Exception("Invalid playerNum!You can only start a game with 2-4 players");
    }
  }

  /**
   * User needs to choose Game after Login/register
   * 
   * @throws Exception
   */
  public void chooseGame(int gameID) throws Exception {
    while (true) {
      Command rcommand = new Command();
      rcommand.operationType = "Choose";
      net.sendMessage(socket, rcommand);
      net.sendMessage(socket, gameID);
      if (!net.recvMessage(socket).equals(join_full)) {
        break;
      } else {
        throw new Exception(join_full);
      }
    }
    // System.out.println(this.socket==null);
    // this.myBoard = recvBoard();
    // this.player = net.recvMessage(socket);
    // System.out.println("Receive yes!!!!!!!!!!!");
  }

  public ArrayList<Integer> getGames() throws Exception {
    Command rCommand = new Command();
    rCommand.operationType = "get";
    net.sendMessage(socket, rCommand);
    ArrayList<Integer> gameList = net.recvMessage(socket);
    // this.myBoard = recvBoard();
    return gameList;
  }

  public void leaveGame() throws Exception {
    Command rCommand = new Command();
    rCommand.operationType = "Leave";
    net.sendMessage(socket, rCommand);
  }

  public void afterLeave(String name) throws Exception {
    int cnt = 0;
    while (cnt <= 100) {
      try {
        net.sendMessage(socket, name);
      } catch (Exception e) {

      }
      cnt += 1;
    }
  }

  /**
   * Receive ready message from server when unit placement is done
   */
  public void recvReadymsg() {
    try {
      readyToplay = net.recvMessage(this.socket);
    } catch (Exception e) {
      // System.out.println("Cannot send ready Toplay: " + e);
    }
  }

  /**
   * Receive gameOver message from server when unit placement is done
   */
  public void recvOvermsg() {
    try {
      gameOver = net.recvMessage(this.socket);
    } catch (Exception e) {

    }
  }

  /** The function of check win at client side */
  public boolean checkWin(Board board) {
    ArrayList<Territory> territories = board.getTerritorys();
    boolean isWin = true;
    for (Territory t : territories) {
      if (t.getOwnerID() != this.playerID) {
        isWin = false;
        break;
      }
    }
    return isWin;
  }

  /**
   * Receive the board from server
   */
  public Board recvBoard() {
    Board b = null;
    System.out.println("Hello yesyes" + (this.socket == null));
    try {
      b = net.recvMessage(this.socket);
      // System.out.println("Hello yesyes"+(this.socket==null));
    } catch (Exception e) {
      e.printStackTrace();
      // System.out.println("Cannot receive board! " + e);
    }
    this.myBoard = b;

    return b;
  }

  
  /** Recv ID */
  public void recvID() throws Exception {
    this.playerID = net.recvMessage(this.socket);
  }

  /**
   * It is for the client to build connection to the server
   */
  public void buildConnection() {
    while (true) {
      try {
        socket = new Socket(this.address, this.port);
        System.out.println("Client connected");
        // recvID();
        break;
      } catch (Exception e) {
        // System.err.println("The address: " + this.address + " and port: " + this.port
        // + " is invalid\n" + e);
        // System.out.println("Please start the server to set player num in
        // server!!!!!!1");
      }
    }
  }

  public Command leave() {
    Command c = new Command();
    c.operationType = "Leave";
    return c;
  }
  // public void playGame() throws Exception{
  // Player player = new Player(this.playerID, port, address);
  // player.myBoard=this.myBoard;
  // player.playGame();
  // }

  /** The entry of client */
  public void exec() throws Exception {
    buildConnection();
  }
}
