package edu.duke.ece651.team17.client.controllers;

import javafx.fxml.Initializable;
import javafx.scene.paint.Color;
import javafx.event.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.swing.plaf.metal.MetalBorders.TextFieldBorder;

import edu.duke.ece651.team17.client.real_client;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Command;
import edu.duke.ece651.team17.shared.Player;
import edu.duke.ece651.team17.shared.Territory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import edu.duke.ece651.team17.shared.Player;
import org.checkerframework.checker.units.qual.A;

public class seeHistoryController implements Initializable {
    public Player player;
    public real_client client;
    public HashMap<Integer, Integer> colormap = new HashMap<>();
    public ArrayList<byte[]>boardHistory;
    public int whichBoard;
    public Board boardForDisplay;
    public ArrayList<Board>trueBoard;

    public seeHistoryController(Player player, real_client client) throws IOException, ClassNotFoundException {
        this.player = player;
        this.client = client;
        this.colormap.put(player.playerID, 0);
        trueBoard = new ArrayList<>();
        boardForDisplay = this.player.myBoard;
        boardHistory = this.player.myBoard.boardHistory;
        whichBoard = 0;
        for(byte[]b : boardHistory){
            trueBoard.add(byte2Board(b));
        }
        boardForDisplay = this.getBoard(0);
        System.out.println("True board size is:" + trueBoard.size());
    }

    @FXML
    Button confirmAction;
    @FXML
    Button ulv0;
    @FXML
    Button ulv1;
    @FXML
    Button ulv2;
    @FXML
    Button ulv3;
    @FXML
    Button ulv4;
    @FXML
    Button ulv5;
    @FXML
    Label tName1;

    @FXML
    Label fResource;
    @FXML
    Label tResource;
    @FXML
    Label lv0;
    @FXML
    Label lv1;
    @FXML
    Label lv2;
    @FXML
    Label lv3;
    @FXML
    Label lv4;
    @FXML
    Label lv5;
    @FXML
    Label lv6;
    @FXML
    ChoiceBox actionSelect;
    @FXML
    TextField targetSelect;
    @FXML
    Label sourceInput;
    @FXML
    ChoiceBox mType;
    @FXML
    TextField mNumber;
    @FXML
    Label rInfo;
    @FXML
    Label FL;
    @FXML
    Label MS;
    @FXML
    Label AL;
    @FXML
    Label GA;
    @FXML
    Label SC;
    @FXML
    Label TN;
    @FXML
    Label NC;
    @FXML
    Label KY;
    @FXML
    Label WV;
    @FXML
    Label VA;
    @FXML
    Label IL;
    @FXML
    Label IN;
    @FXML
    Label OH;
    @FXML
    Label PA;
    @FXML
    Label MD;
    @FXML
    Label DE;
    @FXML
    Label NJ;
    @FXML
    Label NY;
    @FXML
    Label CT;
    @FXML
    Label RI;
    @FXML
    Label MA;
    @FXML
    Label VT;
    @FXML
    Label NH;
    @FXML
    Label ME;
    @FXML
    Button exit4;
    public Board byte2Board(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        Object yourObject = objectInputStream.readObject();
        Board board = (Board)yourObject;
        objectInputStream.close();
        return board;
    }
    public Board getBoard(int idx){
        if(trueBoard.get(trueBoard.size() - 1).currentRound - 3 < idx){
            return trueBoard.get(0);
        }
        for(Board b:trueBoard){
            if(b.currentRound==idx){
                return b;
            }
        }
        return trueBoard.get(0);
    }
    @FXML
    public void goBack(ActionEvent ae) throws Exception {
        System.out.println(1);
        Object source = ae.getSource();
        if(source instanceof Button) {
            this.boardForDisplay = this.getBoard(this.boardForDisplay.currentRound - 1);
            colorUpdate();
            canvasUpdate();
        }
    }
    @FXML
    public void goNext(ActionEvent ae) throws Exception{
        Object source = ae.getSource();
        if(source instanceof Button) {
            this.boardForDisplay = this.getBoard(this.boardForDisplay.currentRound + 1);
            colorUpdate();
            canvasUpdate();
        }
    }

    @FXML
    public void onTerritoryLabel(MouseEvent ae) throws Exception {
        Object source = ae.getSource();
        if (source instanceof Label) {
            Label lbl = (Label) source;
            String tname = lbl.getText();
            tName1.setText("Territory:" + tname);
            sourceInput.setText(tname);
            canvasUpdate();
        } else {
            throw new IllegalArgumentException("Invalid source " +
                    source +
                    " for MouseEvent");
        }
    }






    void getIDs() {
        int i = 1;
        for (Territory t : this.boardForDisplay.getTerritorys()) {
            if (!this.colormap.containsKey(t.getOwnerID())) {
                System.out.println("territory id: " + t.getOwnerID() + "---------------------------------------");
                this.colormap.put(t.getOwnerID(), i);
                i++;
            }
        }
    }

    void colorUpdate() {
        for (Territory t : this.boardForDisplay.getTerritorys()) {
            Integer c = this.colormap.get(t.getOwnerID());
            if (c == 0) {
                changeColor(t.name, Color.RED);
            } else if (c == 1) {
                changeColor(t.name, Color.BLUE);
            } else if (c == 2) {
                changeColor(t.name, Color.GREEN);
            } else if (c == 3) {
                changeColor(t.name, Color.YELLOW);
            }
        }
        System.out.println("color finished!!!!!!!!");
    }

    void changeColor(String name, Color c) {
        if (name.equals("FL")) {
            FL.setTextFill(c);
        } else if (name.equals("MS")) {
            MS.setTextFill(c);
        } else if (name.equals("AL")) {
            AL.setTextFill(c);
        } else if (name.equals("GA")) {
            GA.setTextFill(c);
        } else if (name.equals("SC")) {
            SC.setTextFill(c);
        } else if (name.equals("TN")) {
            TN.setTextFill(c);
        } else if (name.equals("NC")) {
            NC.setTextFill(c);
        } else if (name.equals("KY")) {
            KY.setTextFill(c);
        } else if (name.equals("WV")) {
            WV.setTextFill(c);
        } else if (name.equals("VA")) {
            VA.setTextFill(c);
        } else if (name.equals("IL")) {
            IL.setTextFill(c);
        } else if (name.equals("IN")) {
            IN.setTextFill(c);
        } else if (name.equals("OH")) {
            OH.setTextFill(c);
        } else if (name.equals("PA")) {
            PA.setTextFill(c);
        } else if (name.equals("MD")) {
            MD.setTextFill(c);
        } else if (name.equals("DE")) {
            DE.setTextFill(c);
        } else if (name.equals("NJ")) {
            NJ.setTextFill(c);
        } else if (name.equals("NY")) {
            NY.setTextFill(c);
        } else if (name.equals("CT")) {
            CT.setTextFill(c);
        } else if (name.equals("RI")) {
            RI.setTextFill(c);
        } else if (name.equals("MA")) {
            MA.setTextFill(c);
        } else if (name.equals("VT")) {
            VT.setTextFill(c);
        } else if (name.equals("NH")) {
            NH.setTextFill(c);
        } else if (name.equals("ME")) {
            ME.setTextFill(c);
        }

    }

    void canvasUpdate() throws Exception {
        int id = this.player.playerID;
        String tname = tName1.getText();
        if (tname.equals("Territoy:Please choose Territory on left")) {
            throw new Exception("Please choose a Territory first");
        }
        tname = sourceInput.getText();
        Territory t = this.boardForDisplay.getTerritory(new Territory(tname));
        Integer fr = t.getFoodResource();
        fResource.setText(fr.toString());
        Integer tr = t.getTechResource();
        tResource.setText(tr.toString());
        lv0.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(0)));
        lv1.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(1)));
        lv2.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(2)));
        lv3.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(3)));
        lv4.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(4)));
        lv5.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(5)));
        lv6.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(6)));
        if(t.cloak==0){
            tName1.setText("Territory: "+sourceInput.getText()+" Cloak:None"+" Spies:"+String.valueOf(t.spies.get(id)));
        }
        else{
            tName1.setText("Territory: "+sourceInput.getText()+" Cloak:"+String.valueOf(t.cloak-1)+" Spies:"+String.valueOf(t.spies.get(id)));
        }
        resourceUpdate();

    }

    void resourceUpdate() {
        int id = this.player.playerID;
        Integer fr = this.boardForDisplay.getplayerFoodResource(id);
        Integer tr = this.boardForDisplay.getplayerTechResource(id);
        Integer tech = this.boardForDisplay.getPlayerTechLV(id);
        Integer cost = 0;
        if (tech >= 6) {
        } else {
            cost = 1;
            for (int i = 0; i < tech; i++) {
                cost *= 2;
            }
            cost *= 10;
        }
        StringBuilder str = new StringBuilder();
        str.append("Color: Red");
        str.append("Tech lvl:");
        str.append(tech.toString());
        str.append("\nFood: " + fr.toString() + "Money: " + tr.toString());
        rInfo.setText(str.toString());
    }



    @Override
    public void initialize(URL location, ResourceBundle resources) {
        getIDs();
        colorUpdate();
        System.out.println("colored!!!!!!!");
        System.out.println("Geted!!!!!!");
        resourceUpdate();
    }
}
