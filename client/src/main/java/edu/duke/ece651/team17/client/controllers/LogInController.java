package edu.duke.ece651.team17.client.controllers;
import edu.duke.ece651.team17.client.real_client;
import edu.duke.ece651.team17.shared.Player;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;

public class LogInController implements Initializable {
    @FXML
    TextField name;
    @FXML
    TextField password;
    @FXML
    Button LR;
    @FXML
    Button RS;
    public real_client client;
    public Player player;
    public LogInController() {}

    public LogInController(real_client client,Player player){
        this.client=client;
        this.player=player;
    }
    @FXML
    private void logInConfirm(ActionEvent event) throws Exception{
        Object source=event.getSource();
        if(source instanceof Button) {
            client.accountName=name.getText();
            client.passWord=password.getText();
            client.Login();
            player.playerID = client.playerID;
            Scene scene=LR.getScene();
            //scene.setRoot(null);
            FXMLLoader loader= new FXMLLoader(getClass().getResource("/UI/chooseAction.fxml"));
            HashMap<Class<?>, Object> controllers = new HashMap<>();
            controllers.put(CreateOrJoinController.class,new CreateOrJoinController(this.client,this.player) );
            loader.setControllerFactory((c) -> {
                return controllers.get(c);
            });
            loader.load();
            scene.setRoot(loader.getRoot());
        }
    }
    @FXML
    private void registerConfirm(ActionEvent event) throws Exception{
        Object source=event.getSource();
        if(source instanceof Button) {
            client.accountName=name.getText();
            System.out.println(client.accountName);
            client.passWord=password.getText();
            client.register();
            player.playerID = client.playerID;
            Scene scene=RS.getScene();
            //scene.setRoot(null);
            player.playerName= client.accountName;
            FXMLLoader loader= new FXMLLoader(getClass().getResource("/UI/chooseAction.fxml"));
              //CreateOrJoinController controller=new CreateOrJoinController(this.client,this.player);
            HashMap<Class<?>, Object> controllers = new HashMap<>();
            controllers.put(CreateOrJoinController.class,new CreateOrJoinController(client,player));
            loader.setControllerFactory((c) -> {
                return controllers.get(c);
            });
//            CreateOrJoinController controller=new CreateOrJoinController(this.client,this.player);
//            loader.setController(controller);
          //  scene.setRoot(null);
            loader.load();
            scene.setRoot(loader.getRoot());
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
       try{
           this.client.exec();
       }catch(Exception e){
           System.out.println("Hello error!!!!!!");
       }
    }
}
