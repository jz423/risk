package edu.duke.ece651.team17.client.controllers;
import edu.duke.ece651.team17.client.controllers.*;
import edu.duke.ece651.team17.shared.Player;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import edu.duke.ece651.team17.client.real_client;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Set;
import edu.duke.ece651.team17.shared.Player;
import javafx.collections.ObservableList;
public class JoinGameController implements Initializable {
    public real_client client;
    public Player player;
    @FXML
    public ChoiceBox gameChoose;
    public ArrayList<Integer> games = new ArrayList<>();
    @FXML
    Button confirmChoose;
    @FXML
    Button exitGame;
    public JoinGameController(real_client client, Player player){
      this.client=client;
      this.games=new ArrayList<>();
      this.player=player;
    }
    public JoinGameController(){}
    @FXML
    private void joinConfirm(ActionEvent event) throws Exception{
        Object source=event.getSource();
        if(source instanceof Button){
            int selectedIndex=gameChoose.getSelectionModel().getSelectedIndex();
            String ID=gameChoose.getItems().get(selectedIndex).toString();

            System.out.println("choose yes!!!!!!! for game"+ID);
            client.myGameID=Integer.parseInt(ID);
            System.out.println("client game id is : "+this.client.myGameID);
            // jump to game page
            client.chooseGame(client.myGameID);
            Scene scene=confirmChoose.getScene();
            player.myBoard=this.client.recvBoard();
            this.client.player=player;
            FXMLLoader loader= new FXMLLoader(getClass().getResource("/UI/waitGame.fxml"));
            HashMap<Class<?>, Object> controllers = new HashMap<>();
            controllers.put(waitGameController.class,new waitGameController(this.client,this.player) );
            loader.setControllerFactory((c) -> {
                return controllers.get(c);
            });
            loader.load();
            scene.setRoot(loader.getRoot());
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources){
       try{
           System.out.println("GET!!!!!!!!!");
           ArrayList<Integer> games=client.getGames();
           System.out.println("GET2!!!!!!!!!");
           this.games.addAll(games);
       }catch(Exception e){
           System.out.println(e.getMessage());
       }
     ObservableList<String> items= FXCollections.observableArrayList("PLease select one game to join");
     gameChoose.setItems(items);
     for(int i = 0; i < games.size();i ++){
         gameChoose.getItems().add(Integer.toString(games.get(i)));
     }
    }

}
