package edu.duke.ece651.team17.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.*;

import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Command;
import edu.duke.ece651.team17.shared.Network;
import edu.duke.ece651.team17.shared.Territory;

import static java.lang.System.exit;

public class Client {
  private Socket socket;
  private int port;
  private Board myBoard;
  private int playerID;
  private String address;
  private String playerName;
  private boolean hasWinGame = false;
  private boolean readyToplay= false;
  private boolean gameOver = false;
  private ArrayList<Territory> startTerritory;
  private PrintStream out;
  private BufferedReader reader;
  public Network net = new Network();
  /**The constructor for client side of RISK GAME*/
  public Client(int playerID, int port, String address, BufferedReader inputSource, PrintStream out) {
    this.myBoard = new Board();
    this.playerID = playerID;
    this.port = port;
    this.address = address;
    this.reader = inputSource;
    this.out = out;
  }

    /**
   *Receive ready message from server when unit placement is done
   */
  public void recvReadymsg(){
    try{
      readyToplay = net.recvMessage(this.socket);
    }catch(Exception e){
      System.out.println("Cannot send ready Toplay: " + e);
    }
  }

  /**
   *Receive gameOver message from server when unit placement is done
   */
  public void recvOvermsg(){
    try{
      gameOver = net.recvMessage(this.socket);
    }catch(Exception e){
      System.out.println("Cannot recvOvermsg: " + e);
    }
  }
  /**The function of check win at client side*/
  public boolean checkWin(Board board){
      ArrayList<Territory> territories=board.getTerritorys();
      boolean isWin=true;
      for(Territory t: territories){
          if(t.getOwnerID()!=this.playerID){
              isWin=false;
              break;
          }
      }
      return isWin;
  }
  /**
   * Receive the board from server
   */
  public Board recvBoard() {
    Board b = null;
    try{
      b = net.recvMessage(this.socket);
    }catch(Exception e){
      System.out.println("Cannot receive board! " + e);
    }
    this.myBoard=b;
    return b;
  }

  /**
   * This is used to classify the territory for each player in the board.
   * 
   * @param: the board that is going to get classified
   */
  private HashMap<Integer, ArrayList<Territory>> playerClassify(Board board) {
    ArrayList<Territory> list = board.getTerritorys();
    HashMap<Integer, ArrayList<Territory>> map = new HashMap<>();
    for (Territory t : list) {
      int currID = t.getOwnerID();
      if (!map.containsKey(currID)) {
        map.put(currID, new ArrayList<>());
      }
      map.get(currID).add(t);
    }
    return map;
  }

  /**
   * This method is used to display board
   * 
   * @param out   is the PrintStream to output the display of the board
   * @param board is the board want to display
   */

  public void displayServerBoard(PrintStream out, Board board) {
    out.println(makeServerBoard(board));
  }

  /**
   * It is used to display board
   */
  public String makeServerBoard(Board board) {
    HashMap<Integer, ArrayList<Territory>> map = playerClassify(board);
    StringBuilder sb = new StringBuilder();

    for (Map.Entry<Integer, ArrayList<Territory>> entry : map.entrySet()) { // each player
      sb.append("Player ID: " + entry.getKey() + "\n" + "-----------------" + "\n");
      for (Territory t : entry.getValue()) { // each territory
        String name = t.toString();
        String unit = Integer.toString(t.getDefenderMillitaryPower().getLVPower(0));
        Iterator<Territory> neighborList = t.getNeighbours();
        sb.append(unit + " units in " + name + " (next to: ");
        boolean hasNeighbor = neighborList.hasNext();
        while (neighborList.hasNext()) { // each neighbour
          Territory currTerritory = neighborList.next();
          sb.append(currTerritory.toString() + ", ");
        }
        if (hasNeighbor) {
          sb = sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(")\n\n");
      }
    }
    return sb.toString();
  }
 

  /**Recv ID*/
  public void recvID() throws Exception{
    this.playerID=net.recvMessage(this.socket);
  }
  /**
   * It is for the client to build connection to the server
   */
  public void buildConnection() {
    while(true){
        try {
            socket = new Socket(this.address, this.port);
            out.println("Client connected");
            break;
        } catch (Exception e) {
           // System.err.println("The address: " + this.address + " and port: " + this.port + " is invalid\n" + e);
           out.println("Please start the server to set player num in server!!!!!!1");
        }
    }
  }
  /**Unit Pacement for Client*/
  public ArrayList<Territory> placeUnit(ArrayList<Territory> tr,int totalUnits) throws Exception {
    out.println("Please Place Your Units Into Your territories");
     int unitsTotal=totalUnits;
     String errorMessage="error!!";
     int i=0;
     while(unitsTotal!=0){
             Territory thisTerritory=tr.get(i);
             out.println("You have totally "+unitsTotal+" units, How many units do you want to place on territory "+thisTerritory.name+"?  Please Input your choice in Integer format.");
             while(true){
                 String num= reader.readLine();
                 try{
                     int choice_num=Integer.parseInt(num);
                     if(unitsTotal-choice_num<0){
                         net.sendMessage(this.socket,errorMessage);
                         out.println("Please input again! You don't have enough units!!!!!");
                     }else{
                         unitsTotal-=choice_num;
                         thisTerritory.setDefenderMillitaryPower(0,thisTerritory.getDefenderMillitaryPower().getLVPower(0)+choice_num);
                         break;
                     }
                 }catch(Exception e) {
                     net.sendMessage(this.socket, errorMessage);
                     out.println("Please Input a Integer!!!!!!!!");
                 }
             }
         i=(i+1)%(tr.size());
     }
     return tr;
  }
  /**Choose Group of Start Territoris and call placeUnit
   */
  public void pickTerritory(Board board) throws Exception{
    ArrayList<Territory> chosenOnes=new ArrayList<Territory>();
    ArrayList<Territory> allTerritory=board.getTerritorys();
    for(Territory territory:allTerritory){
      if(territory.getOwnerID()==this.playerID){
        chosenOnes.add(territory);
      }
    }
    this.startTerritory=placeUnit(chosenOnes,100);
  }
  /**Input command*/
  public Command commandEntry(Board myBoard) throws Exception{
      // PrintStream out=new PrintStream(System.out);
      displayServerBoard(out,myBoard);
      Command me=null;
      out.println("What would you like to do?");
      out.println("(M)ove");
      out.println("(A)ttack");
      out.println("(D)one");
      while(true){
          String Input= reader.readLine();
          boolean a=Input.equalsIgnoreCase("M")||Input.equalsIgnoreCase("A")||Input.equalsIgnoreCase("D");
          if(!a){
              out.println("Please Input M or A or D!!!!!!");
          }else{
              String moveType="move";
              if(Input.equalsIgnoreCase("D")){
                  return null;
              }else if(Input.equalsIgnoreCase("M")){
                  moveType="move";
              }else if(Input.equalsIgnoreCase("A")){
                  moveType="attack";
              }
              out.println("Please Input your source Territory Name, target Territory Name, Unit power In the format of SourceName TargetName UnitPower, eg, AZ NC 10");
              while(true){
                  String commandString= reader.readLine();
                  try{
                      me=Command.toCommand(moveType,this.playerID,commandString,myBoard.getTerritorys());
                      break;
                  }catch(Exception e){
                      out.println(e.getMessage());
                  }
              }
              break;
          }
      }
      return me;
  }
  /**The function for letting user decide whether to continue game watching or not*/
  public boolean watchGame() throws IOException{
      out.println("Do you want to continue watching? Please Input Y or N");
      while(true){
          String yes=reader.readLine();
          if(yes.equalsIgnoreCase("Y")){
              return true;
          }else if(yes.equalsIgnoreCase("N")){
              return false;
          }else{
              out.println("Please input Y or N?");
          }
      }
  }
  /**The logic of playing game*/
  public void playGame() throws Exception{
    preGameStart(this.out);
    while(!gameOver && !this.hasWinGame){
      playOneRound();
    }
    if(this.hasWinGame){
      return;
    }
    // if this player loses the game
    while(true){
      displayServerBoard(out,this.myBoard);
      boolean over=false;
      if(myBoard.isPlayerLose(this.playerID)){
        out.println("The game is over!!");
        over=true;
      }
      boolean exitGame=false;
      if(watchGame()){
          ArrayList<Command> commands= new ArrayList<>();
          net.sendMessage(this.socket,commands);
          this.myBoard=net.recvMessage(this.socket);
      }else{
          exitGame=true;
      }
      if(over&&exitGame){
          break;
      }
    }
  }
  /**The logic of playing one round*/
  public void playOneRound() throws Exception{
    ArrayList<Command> commands=new ArrayList<>();
    while(true){
        Command command=commandEntry(this.myBoard);
        if(command!=null){
            if(command.getMoveType().equalsIgnoreCase("move")){
                try{
                    boolean validMove=this.myBoard.move(command);
                    if(validMove==false){
                        out.println("That is a invalid move! try again!!!!");
                    }else{
                        commands.add(command);
                    }
                }catch(Exception e){
                    out.println(e.getMessage());
                }
            }else if(command.getMoveType().equalsIgnoreCase("attack")){
                try{
                    PlayRound x=new PlayRound(command,this.myBoard);
                    x.attack(command);
                    commands.add(command);
                }catch(Exception e){
                    out.println(e.getMessage());
                }
            }
        }else{
            break;
        }
    }
    net.sendMessage(this.socket,commands);
    this.myBoard=net.recvMessage(this.socket);
    checkLose();
    boolean hasWin=checkWin(this.myBoard);
    if(hasWin){
        System.out.println("Congratulations, you are the king of the Game!!!!");
        this.hasWinGame = true;
        return;
    }
    PrintWin(this.myBoard);
  }
  /**Print winner If exist*/
  public void PrintWin(Board myBoard){
      HashSet<Integer> set=new HashSet<>();
      ArrayList<Territory> t=myBoard.getTerritorys();
      int winnerID=1;
      for(Territory t2:t){
          winnerID=t2.getOwnerID();
          set.add(t2.getOwnerID());
      }
      if(set.size()==1){
          System.out.println("The game has end ! The winner is Player "+winnerID);
          this.gameOver=true;
      }
  }
  /**
   * If the territory on the board does not have id that belongs to this player, this player's game is over
   */
  public void checkLose(){
    for(Territory t: this.myBoard.getTerritorys()){
      if(t.getOwnerID() == this.playerID){
        return;
      }
    }
    this.gameOver = true;
  }
  /**
   * This function starts before the action of game
   * @throws Exception
   */
  public void preGameStart(PrintStream out) throws Exception {
      buildConnection();
      recvID();
    this.myBoard= recvBoard();
    HashMap<Integer,String> colorMapping=new HashMap<>();
    colorMapping.put(1,"Red");
    colorMapping.put(2,"Green");
    colorMapping.put(3,"Yellow");
    colorMapping.put(4,"Blue");
    this.playerName=colorMapping.get(this.playerID);
    // display the blank board without any units
    displayServerBoard(out, this.myBoard);
    out.println("\nYour color is: " + this.playerName + ", and you are Player " +playerID + ", Please assign unit for your territories");
    // assign unit here
    pickTerritory(this.myBoard);
    out.println("You have done the placement phase!!!!!!!!Please wait the server to tell you the game start!!!!");
    out.println("This is your Current board!!!!!!");
    displayServerBoard(out,this.myBoard);
    net.sendMessage(this.socket,this.startTerritory);
    out.println("\n\n");
    recvReadymsg();
    out.println("The game has start! ");
    /**Display current whole board(after Initial placement) from server*/
    this.myBoard=recvBoard();
  }
}
