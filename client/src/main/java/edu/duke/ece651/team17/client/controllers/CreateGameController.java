package edu.duke.ece651.team17.client.controllers;
import edu.duke.ece651.team17.client.controllers.*;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.ResourceBundle;


import edu.duke.ece651.team17.client.real_client;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Player;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class CreateGameController implements Initializable {
    public real_client client;
    public Player player;
    public CreateGameController(){}
    public CreateGameController(real_client client, Player player){
        this.client=client;
        this.player=player;
    }
    @FXML
    Button startButton;
    @FXML
    TextField playerNum;
    @FXML
    Button back1;
    @FXML
    private void startGameConfirmation(ActionEvent event) throws Exception{
        Object source=event.getSource();
        if(source instanceof Button){
            client.createGame(playerNum.getText());
            client.myGameID=client.net.recvMessage(client.socket);
            System.out.println("CREATE GAME FINISHED!!!!!!");
            //jump to game page
            Scene scene=startButton.getScene();
            //scene.setRoot(null);
            HashMap<Class<?>, Object> controllers = new HashMap<>();
            controllers.put(waitGameController.class,new waitGameController(client,player));
            FXMLLoader loader=new FXMLLoader(getClass().getResource("/UI/waitGame.fxml"));
            loader.setControllerFactory((c) -> {
                return controllers.get(c);
            });
            System.out.println("load finished2!!!!");
            loader.load();
            System.out.println("load finished!!!!");
            scene.setRoot(loader.getRoot());
            System.out.println("load finished3!!!!");
        }
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
//    @FXML
//    public void backToLogIN(ActionEvent event) throws Exception{
//        Object source=event.getSource();
//        if(source instanceof Button){
//            //page jumps to log IN page.
//            Scene scene=back1.getScene();
//            //scene.setRoot(null);
//            FXMLLoader loader= new FXMLLoader(getClass().getResource("/UI/logIN.fxml"));
//            HashMap<Class<?>, Object> controllers = new HashMap<>();
//            this.client.socket.close();
//            controllers.put(LogInController.class,new LogInController(this.client,this.player) );
//            loader.setControllerFactory((c) -> {
//                return controllers.get(c);
//            });
//            loader.load();
//            scene.setRoot(loader.getRoot());
//        }
//    }


}
