package edu.duke.ece651.team17.client.controllers;

import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.event.*;

import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.plaf.metal.MetalBorders.TextFieldBorder;

import edu.duke.ece651.team17.client.real_client;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Command;
import edu.duke.ece651.team17.shared.Player;
import edu.duke.ece651.team17.shared.Territory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import edu.duke.ece651.team17.shared.Player;

public class InGameController {
    public Player player;
    public real_client client;
    public HashMap<Integer, Integer> colormap = new HashMap<>();

    public InGameController(Player player, real_client client) {
        this.player = player;
        this.client = client;
        this.colormap.put(player.playerID, 0);
    }
    public InGameController(Player player, real_client client, HashMap<Integer, Integer> colormap) {
        this.player = player;
        this.client = client;
        this.colormap = colormap;
    }

    @FXML
    Button confirmAction;
    @FXML
    Button ulv0;
    @FXML
    Button ulv1;
    @FXML
    Button ulv2;
    @FXML
    Button ulv3;
    @FXML
    Button ulv4;
    @FXML
    Button ulv5;
    @FXML
    Label tName1;
    @FXML
    TextField c1;
    @FXML
    TextField c2;
    @FXML
    TextField c3;
    @FXML
    TextField c4;
    @FXML
    TextField c5;
    @FXML
    TextField c6;
    @FXML
    Label fResource;
    @FXML
    Label tResource;
    @FXML
    Label lv0;
    @FXML
    Label lv1;
    @FXML
    Label lv2;
    @FXML
    Label lv3;
    @FXML
    Label lv4;
    @FXML
    Label lv5;
    @FXML
    Label lv6;
    @FXML
    public ChoiceBox actionSelect;
    @FXML
    TextField targetSelect;
    @FXML
    Label sourceInput;
    @FXML
    ChoiceBox mType;
    @FXML
    TextField mNumber;
    @FXML
    Label rInfo;
    @FXML
    Label FL;
    @FXML
    Label MS;
    @FXML
    Label AL;
    @FXML
    Label GA;
    @FXML
    Label SC;
    @FXML
    Label TN;
    @FXML
    Label NC;
    @FXML
    Label KY;
    @FXML
    Label WV;
    @FXML
    Label VA;
    @FXML
    Label IL;
    @FXML
    Label IN;
    @FXML
    Label OH;
    @FXML
    Label PA;
    @FXML
    Label MD;
    @FXML
    Label DE;
    @FXML
    Label NJ;
    @FXML
    Label NY;
    @FXML
    Label CT;
    @FXML
    Label RI;
    @FXML
    Label MA;
    @FXML
    Label VT;
    @FXML
    Label NH;
    @FXML
    Label ME;
    @FXML
    Button exit4;


    @FXML
    public void seeHistory(ActionEvent ae) throws Exception {
        if(this.player.myBoard.currentRound < 3){
            throw new Exception("Please wait after round 3");
        }
        Object source = ae.getSource();
        if(source instanceof Button){
            FXMLLoader loader=new FXMLLoader(getClass().getResource("/UI/seeHistory.fxml"));
            HashMap<Class<?>, Object> controllers = new HashMap<>();
            controllers.put(seeHistoryController.class,new seeHistoryController(player, client));
            loader.setControllerFactory((c) -> {
                return controllers.get(c);
            });
            Stage newStage=new Stage();
            Scene newScene=new Scene(loader.load(),1000,800);
            newScene.getStylesheets().add(getClass().getResource("/UI/style.css").toExternalForm());
            newStage.setScene(newScene);
            newStage.show();
        }
    }

    @FXML
    public void onTerritoryLabel(MouseEvent ae) throws Exception {
        Object source = ae.getSource();
        if (source instanceof Label) {
            Label lbl = (Label) source;
            String tname = lbl.getText();
            tName1.setText("Territory:" + tname);
            sourceInput.setText(tname);
            canvasUpdate();
        } else {
            throw new IllegalArgumentException("Invalid source " +
                    source +
                    " for MouseEvent");
        }
    }

    @FXML
    public void onUnitUpgradeButton(ActionEvent ae) throws Exception {
        Object source = ae.getSource();
        if (source instanceof Button) {
            Button btn = (Button) source;
            final Node node = (Node) source;
            String id = node.getId();
            String tname = tName1.getText();
            if (tname.equals("Territoy:Please choose Territory on left")) {
                throw new Exception("Please choose a Territory first");
            }
            tname = sourceInput.getText();
            Territory t = this.player.myBoard.getTerritory(new Territory(tname));
            id = id.split("v")[1];
            int lvl = Integer.parseInt(id);
            int unitpower = 0;
            if (lvl == 0) {
                unitpower = Integer.parseInt(c1.getText());
            } else if (lvl == 1) {
                unitpower = Integer.parseInt(c2.getText());
            } else if (lvl == 2) {
                unitpower = Integer.parseInt(c3.getText());
            } else if (lvl == 3) {
                unitpower = Integer.parseInt(c4.getText());
            } else if (lvl == 4) {
                unitpower = Integer.parseInt(c5.getText());
            } else if (lvl == 5) {
                unitpower = Integer.parseInt(c6.getText());
            }
            Command cmd = new Command(this.player.playerID, "upgrade_unit", t, t, unitpower, lvl);
            // for(Territory tt: this.player.myBoard.getTerritorys()){
            // System.out.println(tt.name);
            // }
            this.player.myBoard.upgradeUnit(cmd);
            this.player.commands.add(cmd);

        } else {
            throw new IllegalArgumentException("Invalid source " +
                    source +
                    " for ActionEvent");
        }
        canvasUpdate();
    }

    @FXML
    public void onConfirmButton(ActionEvent ae) throws Exception {
        int id = this.player.playerID;
        String action = (String) actionSelect.getValue();
        System.out.println("action is: " + action);
        if (action.equalsIgnoreCase("done")) {

            if (tName1.getText().equals("Territoy:Please choose Territory on left")) {
                throw new Exception("Please choose a Territory first");
            }
            this.player.sendAllCommands(this.client.socket);
            this.player.myBoard = this.client.recvBoard();
            this.player.commands = new ArrayList<Command>();
        } else {
            // this.player.gameOver= true;
            Territory source = new Territory((String) sourceInput.getText());
            Territory target = new Territory((String) targetSelect.getText());
            int level = 0;
            int number = 0;
            String levelStr = (String) mType.getValue();
            String numberStr = mNumber.getText();
            if (levelStr.equals("")) {
                level = 0;
            }
            if (numberStr.equals("")) {
                number = 0;
            } else {
                try {
                    level = Integer.parseInt((String) mType.getValue());
                    number = Integer.parseInt(mNumber.getText());
                } catch (Exception e) {
                    throw new Exception("Please input correct format of digit value");
                }
            }
            Command cmd = new Command(id, action, source, target, number, level);
            this.player.playOneRound(cmd);
        }
        this.player.checkLose();
        if (this.player.gameOver) {
            jumpToLoosePage();
        }
        this.player.checkWin(this.player.myBoard);
        if (this.player.hasWinGame) {
            throw new Exception("you has win!!!!!!!");
        }
        colorUpdate();
        canvasUpdate();
    }

    void jumpToLoosePage() throws Exception {
        Scene scene = rInfo.getScene();
        // scene.setRoot(null);
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/UI/Watchgame.fxml"));
        // scene.setRoot(loader.load());
        HashMap<Class<?>, Object> controllers = new HashMap<>();
        controllers.put(WatchGameController.class, new WatchGameController(player, client, this.colormap));
        loader.setControllerFactory((c) -> {
            return controllers.get(c);
        });
        loader.load();
        scene.setRoot(loader.getRoot());

    }

    void watchCanvas() {
        tName1.setText("Territory:" + this.player.myBoard.getTerritorys().get(0).name);
        rInfo.setText("You have lost! \n Watching now");
        ulv0.setVisible(false);
        ulv1.setVisible(false);
        ulv2.setVisible(false);
        ulv3.setVisible(false);
        ulv4.setVisible(false);
        ulv5.setVisible(false);
        confirmAction.setVisible(false);
        c1.setEditable(false);
        c2.setEditable(false);
        c3.setEditable(false);
        c4.setEditable(false);
        c5.setEditable(false);
        c6.setEditable(false);
        targetSelect.setEditable(false);
        mNumber.setEditable(false);
        actionSelect.hide();
        mType.hide();

    }

    void watch() throws Exception {
        System.out.println("watch()called!!!!!!!!!!!!!");
        watchCanvas();
        System.out.println("canvas called!!!!!!!!!!!!!");
        // this.player.watchGame(this.client.socket);
        while (true) {
            System.out.println("Game over??????????");
            ArrayList<Command> cs = new ArrayList<>();
            Command command = new Command();
            command.moveType = "done";
            cs.add(command);
            System.out.println("The player is watching");
            this.client.net.sendMessage(this.client.socket, cs);
            this.player.myBoard = this.client.recvBoard();
            System.out.println("The player received watched board!!!!!");
            colorUpdateOld();
            canvasUpdate();
            System.out.println("Canvas finished!!!!!!!");
            resourceUpdate();
            System.out.println("Update finished");
            this.player.gameOver = false;
            this.player.PrintWin(this.player.myBoard);
            if (this.player.gameOver) {
                throw new Exception("Game is over!! someone has win!!!!!!!!!");
            }
        }
    }

    void getIDs() {
        int i = 1;
        for (Territory t : this.player.myBoard.getTerritorys()) {
            if (!this.colormap.containsKey(t.getOwnerID())) {
                System.out.println("territory id: " + t.getOwnerID() + "---------------------------------------");
                this.colormap.put(t.getOwnerID(), i);
                i++;
            }
        }
    }

    void colorUpdate() {
        this.player.recordBoard();
        int id = this.player.playerID;
        for (Territory t : this.player.myBoard.getTerritorys()) {
            Integer c = this.colormap.get(t.getOwnerID());
            if (t.visibility.get(id) == -1) {
                changeColor(t.name, Color.WHITE);
            } else if (t.visibility.get(id) < this.player.myBoard.currentRound) {
                changeColor(t.name, Color.GRAY);
            } else if (c == 0) {
                changeColor(t.name, Color.RED);
            } else if (c == 1) {
                changeColor(t.name, Color.BLUE);
            } else if (c == 2) {
                changeColor(t.name, Color.GREEN);
            } else if (c == 3) {
                changeColor(t.name, Color.YELLOW);
            }
        }
        System.out.println("color finished!!!!!!!!");
    }

    void colorUpdateOld() {
        for (Territory t : this.player.myBoard.getTerritorys()) {
            Integer c = this.colormap.get(t.getOwnerID());
            if (c == 0) {
                changeColor(t.name, Color.RED);
            } else if (c == 1) {
                changeColor(t.name, Color.BLUE);
            } else if (c == 2) {
                changeColor(t.name, Color.GREEN);
            } else if (c == 3) {
                changeColor(t.name, Color.YELLOW);
            }
        }
        System.out.println("color finished!!!!!!!!");
    }

    void changeColor(String name, Color c) {
        if (name.equals("FL")) {
            FL.setTextFill(c);
        } else if (name.equals("MS")) {
            MS.setTextFill(c);
        } else if (name.equals("AL")) {
            AL.setTextFill(c);
        } else if (name.equals("GA")) {
            GA.setTextFill(c);
        } else if (name.equals("SC")) {
            SC.setTextFill(c);
        } else if (name.equals("TN")) {
            TN.setTextFill(c);
        } else if (name.equals("NC")) {
            NC.setTextFill(c);
        } else if (name.equals("KY")) {
            KY.setTextFill(c);
        } else if (name.equals("WV")) {
            WV.setTextFill(c);
        } else if (name.equals("VA")) {
            VA.setTextFill(c);
        } else if (name.equals("IL")) {
            IL.setTextFill(c);
        } else if (name.equals("IN")) {
            IN.setTextFill(c);
        } else if (name.equals("OH")) {
            OH.setTextFill(c);
        } else if (name.equals("PA")) {
            PA.setTextFill(c);
        } else if (name.equals("MD")) {
            MD.setTextFill(c);
        } else if (name.equals("DE")) {
            DE.setTextFill(c);
        } else if (name.equals("NJ")) {
            NJ.setTextFill(c);
        } else if (name.equals("NY")) {
            NY.setTextFill(c);
        } else if (name.equals("CT")) {
            CT.setTextFill(c);
        } else if (name.equals("RI")) {
            RI.setTextFill(c);
        } else if (name.equals("MA")) {
            MA.setTextFill(c);
        } else if (name.equals("VT")) {
            VT.setTextFill(c);
        } else if (name.equals("NH")) {
            NH.setTextFill(c);
        } else if (name.equals("ME")) {
            ME.setTextFill(c);
        }

    }

    void canvasUpdate() throws Exception {
        int id = this.player.playerID;
        c1.setText("0");
        c2.setText("0");
        c3.setText("0");
        c4.setText("0");
        c5.setText("0");
        c6.setText("0");
        String tname = tName1.getText();
        if (tname.equals("Territoy:Please choose Territory on left")) {
            throw new Exception("Please choose a Territory first");
        }
        tname = sourceInput.getText();
        Territory t = this.player.myBoard.getTerritory(new Territory(tname));
        int idx = t.visibility.get(id);
        if (idx < 0) {
            t = new Territory(tname);
            ArrayList<Integer> tmp = new ArrayList<>();
            tmp.add(id);
            t.initializeProjectedPower(tmp);
        } else if (idx < this.player.myBoard.currentRound) {
            Board b = this.player.getBoard(idx);
            if (b == null) {
                t = new Territory(tname);
                ArrayList<Integer> tmp = new ArrayList<>();
                tmp.add(id);
                t.initializeProjectedPower(tmp);
            } else {
                t = b.getTerritory(t);
            }

        }
        Integer fr = t.getFoodResource();
        fResource.setText(fr.toString());
        Integer tr = t.getTechResource();
        tResource.setText(tr.toString());
        actionSelect.setValue("");
        targetSelect.setText("");
        mType.setValue("");
        mNumber.setText("");
        lv0.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(0)));
        lv1.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(1)));
        lv2.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(2)));
        lv3.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(3)));
        lv4.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(4)));
        lv5.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(5)));
        lv6.setText(String.valueOf(t.getDefenderMillitaryPower().getLVPower(6)));
        if(t.cloak==0){
            tName1.setText("Territory: "+sourceInput.getText()+" Cloak:None"+" Spies:"+String.valueOf(t.spies.get(id)));
        }
        else{
            tName1.setText("Territory: "+sourceInput.getText()+" Cloak:"+String.valueOf(t.cloak-1)+" Spies:"+String.valueOf(t.spies.get(id)));
        }
        resourceUpdate();

    }

    void resourceUpdate() {
        int id = this.player.playerID;
        Integer fr = this.player.myBoard.getplayerFoodResource(id);
        Integer tr = this.player.myBoard.getplayerTechResource(id);
        Integer tech = this.player.myBoard.getPlayerTechLV(id);
        Integer cost = 0;
        if (tech >= 6) {
        } else {
            cost = 1;
            for (int i = 0; i < tech; i++) {
                cost *= 2;
            }
            cost *= 10;
        }
        StringBuilder str = new StringBuilder();
        str.append("Color: Red");
        str.append(" Tech lvl:");
        str.append(tech.toString());
        str.append("\nFood: " + fr.toString() + " Money: " + tr.toString());
        rInfo.setText(str.toString());
    }

    public void initialize1() {
        if (!player.isWatching) {
            getIDs();
            colorUpdate();
            System.out.println("colored!!!!!!!");
            actionSelect.getItems().add("move");
            actionSelect.getItems().add("attack");
            actionSelect.getItems().add("upgrade_tech");
            actionSelect.getItems().add("train spy: cost 30");
            actionSelect.getItems().add("move spy");
            actionSelect.getItems().add("cloak: cost 50");
            actionSelect.getItems().add("done");
            mType.getItems().add("0");
            mType.getItems().add("1");
            mType.getItems().add("2");
            mType.getItems().add("3");
            mType.getItems().add("4");
            mType.getItems().add("5");
            mType.getItems().add("6");
            System.out.println("Geted!!!!!!");
            resourceUpdate();
        }
    }

    public void rejoin() {
        getIDs();
        colorUpdate();
        System.out.println("colored!!!!!!!");
        actionSelect.getItems().add("move");
        actionSelect.getItems().add("attack");
        actionSelect.getItems().add("upgrade_tech");
        actionSelect.getItems().add("train spy: cost 30");
        actionSelect.getItems().add("move spy");
        actionSelect.getItems().add("cloak: cost 50");
        actionSelect.getItems().add("done");
        mType.getItems().add("0");
        mType.getItems().add("1");
        mType.getItems().add("2");
        mType.getItems().add("3");
        mType.getItems().add("4");
        mType.getItems().add("5");
        mType.getItems().add("6");
        System.out.println("Geted!!!!!!");
        resourceUpdate();
    }



}
