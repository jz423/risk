package edu.duke.ece651.team17.client;

import java.util.Scanner;

import edu.duke.ece651.team17.shared.AttackChecker;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Command;
import edu.duke.ece651.team17.shared.Territory;

public class PlayRound{
    private Command command;
    private int playId;
    private Board myBoard;
    private AttackChecker attackChecker;
    /** constructor for playround*/
    public PlayRound(Command command,Board myBoard){
        this.command = command;
        attackChecker = new AttackChecker();
        this.myBoard=myBoard;

    }
    /**attack method*/
    public void attack(Command command) throws Exception{
        // 1. check
        attackChecker.checkValidation(command.getPlayerID(), command.getSource(), command.getTarget(), command.getUnitsPower(),command.getLevel());
        // 2. attack
        for(Territory t: myBoard.getTerritorys()){
            if(t.toString() == command.getSource().toString()){
                t.setDefenderMillitaryPower(command.getLevel(),t.getDefenderMillitaryPower().getLVPower(command.getLevel()) - command.getUnitsPower());
                break;
            }
        }
    }
}