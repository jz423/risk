package edu.duke.ece651.team17.shared;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

public class TerritoryTest {
  public Territory get4Territory() {
    Territory t1 = new Territory("t1");
    Territory t2 = new Territory("t2");
    Territory t3 = new Territory("t3");
    Territory t4 = new Territory("t4");
    t1.addNeighbour(t2);
    t1.addNeighbour(t3);
    t3.addNeighbour(t4);
    t4.addNeighbour(t3);
    t3.addNeighbour(t1);
    t2.addNeighbour(t1);
    return t1;
  }

  public Territory getTerritoryByName(Territory t, String name) {
    Iterator<Territory> it = t.getNeighbours();
    while (it.hasNext()) {
      Territory tmp = it.next();
      if (tmp.name == name) {
        return tmp;
      }
    }
    return null;
  }

  public boolean compareNeighboursToSet(Territory t, HashSet<Territory> set) {
    Iterator<Territory> it = t.getNeighbours();
    set = (HashSet<Territory>) set.clone();
    while (it.hasNext()) {
      Territory tmp = it.next();
      if (!set.contains(tmp)) {
        return false;

      }
      set.remove(tmp);
    }
    if (!set.isEmpty()) {
      return false;
    }
    return true;

  }

  @Test
  public void test_constructor() {
    Territory t1 = new Territory("t1");
    assertEquals("t1", t1.name);
  }

  @Test
  public void test_equal_and_hash() {
    Territory t1 = get4Territory();
    Territory t2 = new Territory("t2");
    Territory t3 = new Territory("t1");
    assertEquals(true, t1.equals(t3));
    assertNotEquals(t1, t2);
    assertEquals(t1, t1);
    assertEquals(t1.toString(), t3.toString());
    assertNotEquals(t1.toString(), t2.toString());
    assertEquals(t1.hashCode(), t1.hashCode());
    assertEquals(t1.hashCode(), t3.hashCode());
    assertNotEquals(t1.hashCode(), t2.hashCode());
    assertNotEquals(t1, "t1");
  }

  @Test
  public void test_neighbour() {
    Territory t1 = get4Territory();
    Territory t2 = new Territory("t2");
    Territory t3 = new Territory("t3");
    Territory t4 = new Territory("t4");
    HashSet<Territory> target_set = new HashSet<Territory>();
    target_set.add(t2);
    assertEquals(false, compareNeighboursToSet(t1, target_set));
    target_set.add(t3);
    assertEquals(true, compareNeighboursToSet(t1, target_set));
    t1.addNeighbour(t1);
    t1.addNeighbour(t2);
    assertEquals(true, compareNeighboursToSet(t1, target_set));
    target_set.add(t4);
    assertEquals(false, compareNeighboursToSet(t1, target_set));
    target_set = new HashSet<Territory>();
    target_set.add(t1);
    target_set.add(t4);
    Territory t5 = getTerritoryByName(t1, "2");
    assertEquals(t5, null);
    t5 = getTerritoryByName(t1, "t3");
    assertEquals(true, compareNeighboursToSet(t5, target_set));
    assertEquals(true, t1.isNeighbour(t2));
    assertEquals(false, t1.isNeighbour(t4));

  }

  @Test
  public void test_owner() {
    Territory t1 = new Territory("t1");
    assertEquals(-1, t1.getOwnerID());
    t1.setOwnerID(1);
    assertEquals(1, t1.getOwnerID());
  }

  @Test
  public void test_defense_millitary() {
    Territory t1 = new Territory("t1");
    assertEquals(0, t1.getDefenderMillitaryPower().getMinPower());
    assertEquals(false, t1.setDefenderMillitaryPower(0,-1));
    assertEquals(true, t1.setDefenderMillitaryPower(0,2));
    assertEquals(2, t1.getDefenderMillitaryPower().getMinPower());
  }

  @Test
  public void test_initProjectMillitaryPower() {
    Territory t1 = new Territory("t1");
    ArrayList<Integer> ids = new ArrayList<Integer>();
    ids.add(0);
    ids.add(1);
    ids.add(2);
    t1.initializeProjectedPower(ids);
    assertEquals(0, t1.getProjectedPower(1).getMinPower());
    t1.setProjectedPower(-1, 0,2);
    t1.setProjectedPower(0, 0,2);
    assertEquals(null, t1.getProjectedPower(-1));
    assertEquals(0, t1.getProjectedPower(1).getMinPower());
    assertEquals(2, t1.getProjectedPower(0).getMinPower());
  }
  @Test
  public void test_initProjectMillitaryPower2() {
    Territory t1 = new Territory("t1");
    ArrayList<Integer> ids = new ArrayList<Integer>();
    ids.add(0);
    ids.add(1);
    ids.add(2);
    t1.initializeProjectedPower(ids);
    t1.setOwnerID(0);
    t1.setDefenderMillitaryPower(0,0);
    t1.setProjectedPower(1,0,5);
    t1.decideAttackWinner();
    assertEquals(1,t1.getOwnerID());
    assertFalse(t1.trainSpy());
    t1.setDefenderMillitaryPower(1,1);
    assertTrue(t1.trainSpy());
    assertTrue(t1.spies.get(1).equals(1));
  }
  @Test
  public void test_initProjectMillitaryPower_Alternate() {
    Territory t1 = new Territory("t1");
    ArrayList<Integer> ids = new ArrayList<Integer>();
    ids.add(0);
    ids.add(1);
    ids.add(2);
    t1.initializeProjectedPower(ids);
    t1.setOwnerID(0);
    t1.setDefenderMillitaryPower(0,2);
    t1.setProjectedPower(1,6,100);
    t1.setProjectedPower(1,3,10);
    t1.setProjectedPower(2, 1, 4);
    t1.decideAttackWinner();
    t1.spyBufferMerge();
    assertTrue(t1.getOwnerID()==1);
  }
  @Test
  public void test_helpers() {
    Territory t1 = new Territory("t1");
    //t1.decideAttackWinner();
    
    t1.setTechResource(1);
    assertEquals(1,t1.getTechResource());
    t1.setFoodResource(1);
    assertEquals(1,t1.getFoodResource());
    assertEquals(23,t1.getDice(2));
    assertEquals(28,t1.getDice(4));
    assertEquals(31,t1.getDice(5));
    assertEquals(0,t1.getDice(9));
  }
}
