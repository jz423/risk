package edu.duke.ece651.team17.shared;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TroopsTest {
  @Test
  public void test_change_troops() {
      Troops t=new Troops();
      assertEquals(0, t.getMaxLevel());
      assertEquals(0, t.getMaxPower());
      assertEquals(0, t.getMinLevel());
      assertEquals(0, t.getMinPower());
      assertEquals(0, t.getLVPower(3));
      assertFalse(t.changeTroops(-1, 2));
      assertFalse(t.changeTroops(0, -1));
      assertEquals(0, t.getMaxLevel());
      assertEquals(0, t.getMaxPower());
      assertEquals(0, t.getMinLevel());
      assertEquals(0, t.getMinPower());
      assertEquals(0, t.getLVPower(3));
      assertTrue(t.changeTroops(3,4));
      assertEquals(3, t.getMaxLevel());
      assertEquals(4, t.getMaxPower());
      assertEquals(3, t.getMinLevel());
      assertEquals(4, t.getMinPower());
      assertEquals(4, t.getLVPower(3));
      assertTrue(t.changeTroops(1,5));
      assertEquals(3, t.getMaxLevel());
      assertEquals(4, t.getMaxPower());
      assertEquals(1, t.getMinLevel());
      assertEquals(5, t.getMinPower());
      assertEquals(4, t.getLVPower(3));
     
  }
  @Test
  public void test_lvUp(){
    Troops t=new Troops();
    t.changeTroops(0, 3);
    t.lvUp(0,1);
    assertEquals(1, t.getMaxLevel());
    assertEquals(1, t.getMaxPower());
    assertEquals(0, t.getMinLevel());
    assertEquals(2, t.getMinPower());
    t.lvUp(1,1);
    t.lvUp(1,1);
    t.lvUp(-1,-1);
    assertEquals(2, t.getMaxLevel());
    assertEquals(1, t.getMaxPower());
    assertEquals(0, t.getMinLevel());
    assertEquals(2, t.getMinPower());
  }

  @Test
  public void test_reduce(){
    Troops t=new Troops();
    t.reduceMaxPower();
    t.reduceMinPower();
    t.changeTroops(0, 3);
    t.changeTroops(6, 3);
    t.changeTroops(1, 0);
    assertFalse(t.trainSpy());
    t.changeTroops(1, 1);
    assertTrue(t.trainSpy());
    assertEquals(0, t.getLVPower(1));
    t.reduceMaxPower();
    assertEquals(6, t.getMaxLevel());
    assertEquals(2, t.getMaxPower());
    assertEquals(0, t.getMinLevel());
    assertEquals(3, t.getMinPower());
    t.reduceMinPower();
    assertEquals(6, t.getMaxLevel());
    assertEquals(2, t.getMaxPower());
    assertEquals(0, t.getMinLevel());
    assertEquals(2, t.getMinPower());
    
  }
}
