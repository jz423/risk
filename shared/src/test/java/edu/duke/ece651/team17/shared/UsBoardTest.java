package edu.duke.ece651.team17.shared;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class UsBoardTest {
  @Test
  public void test_usboard() throws FileNotFoundException, IOException{
    UsBoard b = new UsBoard();
    ArrayList<Territory> m = b.getTerritorys(); 
    assertEquals(24, m.size());
    assertEquals("ME",m.get(0).name);
    assertEquals("NH",m.get(1).name);
    assertEquals("FL",m.get(23).name);
  }

}
