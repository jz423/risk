package edu.duke.ece651.team17.shared;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

public class AttackCheckerTest {
    @Test
    public void checkValidation() throws Exception{
        Territory t1 = new Territory("AA");
        Territory t2 = new Territory("BB");
        Territory t3 = new Territory("CC");
        t1.addNeighbour(t2);
        t2.addNeighbour(t1);
        t2.addNeighbour(t3);
        t3.addNeighbour(t2);
        t1.setOwnerID(1);
        t2.setOwnerID(1);
        t3.setOwnerID(2);
        t1.setDefenderMillitaryPower(10);
        t2.setDefenderMillitaryPower(10);
        t3.setDefenderMillitaryPower(10);
        AttackChecker ac = new AttackChecker();   
        assertTrue(ac.checkValidation(1, t2, t3, 10));
        try{
            ac.checkValidation(1, t1, t2, 10);
        }catch(Exception e){
            assertEquals(e.getMessage(), "You cannot attack your own territory! Please input again: ");
        }
        try{
            ac.checkValidation(1, t2, t3, 15);
        }catch(Exception e){
            assertEquals(e.getMessage(), "The unit you you want attack is larger than the territory, which must be less or equals to 10! Please enter again:");
        }
        try{
            ac.checkValidation(1, t3, t2, 10);
        }catch(Exception e){
            assertEquals(e.getMessage(), "The attack source is not your own territory! Please input again: ");
        }
        try{
            ac.checkValidation(1, t1, t3, 10);
        }catch(Exception e){
            assertEquals(e.getMessage(), "The source and target territories are not neighbours! Please enter again: ");
        }
    }

}
