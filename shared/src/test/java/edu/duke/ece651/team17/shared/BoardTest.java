package edu.duke.ece651.team17.shared;

import static org.junit.jupiter.api.Assertions.*;

import java.security.cert.TrustAnchor;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

public class BoardTest {

  @Test
  public void test_add_territory() {
    Board b = new Board();
    b.addTerritory(new Territory("t1"));
    b.addTerritory(new Territory("t1"));
    b.addTerritory(new Territory("t2"));
    ArrayList<Territory> m = b.getTerritorys();

    assertEquals(2, m.size());
    assertEquals("t1", m.get(0).name);
    assertEquals("t2", m.get(1).name);

  }

  @Test
  public void test_connectBoard() {
    Board b = new Board();
    b.copyBoard(b);
    b.addTerritory(new Territory("t1"));
    b.addTerritory(new Territory("t2"));
    b.addTerritory(new Territory("t3"));
    Boolean[][] A = { { false, true, false },
        { true, false, true },
        { false, false, false } };
    b.connectNeighbours(A);
    Iterator<Territory> it = b.getTerritorys().get(1).getNeighbours();
    Territory t1 = it.next();
    assertEquals("t1", t1.name);
    assertEquals("t3", it.next().name);
    it = t1.getNeighbours();
    assertEquals("t2", it.next().name);
    assertEquals(false, it.hasNext());
  }

  @Test
  public void test_move() throws Exception {
    Board b = new Board();
    b.addTerritory(new Territory("t1"));
    b.addTerritory(new Territory("t2"));
    b.addTerritory(new Territory("t3"));
    b.addTerritory(new Territory("t4"));
    Boolean[][] A = { { false, true, false, true },
        { true, false, true, true },
        { false, false, false, true },
        { true, true, true, true } };
    b.connectNeighbours(A);
    Integer[][] A1 = { { 0, 2, 0, 5 },
        { 1, 0, 2, 1 },
        { 0, 0, 0, 4 },
        { 3, 1, 1, 1 } };
    b.connectCosts(A1);
    ArrayList<Integer> ids=new ArrayList<Integer>();
    ids.add(0);
    ids.add(1);
    b.initializePlayers(ids);
    ArrayList<Territory> list = b.getTerritorys();
    list.get(0).setOwnerID(0);
    list.get(1).setOwnerID(0);
    list.get(2).setOwnerID(0);
    list.get(3).setOwnerID(1);
    for (Territory t : list) {
      t.setDefenderMillitaryPower(5);
    }
    Command cmd = new Command(0, "move", new Territory("t1"), new Territory("t3"), 2);
    assertTrue(b.move(cmd));
    assertTrue(list.get(0).getDefenderMillitaryPower().getMinPower() == 3);
    assertTrue(list.get(2).getDefenderMillitaryPower().getMinPower() == 7);
    final Command cmd1 = new Command(0, "move", new Territory("t1"), new Territory("t4"), 2);
    assertThrows(Exception.class, () -> b.move(cmd1));
    assertTrue(list.get(0).getDefenderMillitaryPower().getMinPower() == 3);
    final Command cmd2 = new Command(0, "move", new Territory("t1"), new Territory("t3"), 5);
    assertThrows(Exception.class, () -> b.move(cmd2));
    assertTrue(list.get(0).getDefenderMillitaryPower().getMinPower() == 3);
    final Command cmd3 = new Command(0, "move", new Territory("t3"), new Territory("t1"), 5);
    assertThrows(Exception.class, () -> b.move(cmd3));
    assertTrue(list.get(0).getDefenderMillitaryPower().getMinPower() == 3);
    final Command cmd4 = new Command(0, "move", null, new Territory("t1"), 5);
    assertThrows(Exception.class, () -> b.move(cmd4));
    
    assertTrue(list.get(0).getDefenderMillitaryPower().getMinPower() == 3);
    final Command cmd5 = new Command(0, "attack", new Territory("t1"), new Territory("t1"), 5);
    assertFalse(b.move(cmd5));
    assertTrue(list.get(0).getDefenderMillitaryPower().getMinPower() == 3);
  }

  @Test
  public void test_player_lose() {
    Board b = new Board();
    b.addTerritory(new Territory("t1"));
    b.addTerritory(new Territory("t1"));
    b.addTerritory(new Territory("t2"));
    ArrayList<Territory> m = b.getTerritorys();
    for (Territory t : m) {
      t.setOwnerID(2);
    }
    assertTrue(b.isPlayerLose(0));
    assertFalse(b.isPlayerLose(2));
    assertEquals(2, b.countTerritoryByPlayerID(2));
    assertEquals(0, b.countTerritoryByPlayerID(0));
  }

  @Test
  public void test_attack() throws Exception {
    Territory A = new Territory("A");
    Territory B = new Territory("B");
    A.addNeighbour(B);
    B.addNeighbour(A);
    A.setOwnerID(1);
    B.setOwnerID(2);
    ArrayList<Integer> all = new ArrayList<>();
    all.add(1);
    all.add(2);
    A.initializeProjectedPower(all);
    B.initializeProjectedPower(all);
    A.setDefenderMillitaryPower(10);
    B.setDefenderMillitaryPower(10);
    Board board = new Board();
    board.addTerritory(A);
    board.addTerritory(B);
    board.initializePlayers(all);
    Command cr = new Command(2, "attack", B, A, 10);
    board.prepareForAtack(cr);
    board.doAttack();
  }

  @Test
  public void test_attack2() throws Exception {
    Territory A = new Territory("A");
    Territory B = new Territory("B");
    A.addNeighbour(B);
    B.addNeighbour(A);
    A.setOwnerID(1);
    B.setOwnerID(2);
    ArrayList<Integer> all = new ArrayList<>();
    all.add(1);
    all.add(2);
    A.initializeProjectedPower(all);
    B.initializeProjectedPower(all);
    A.setDefenderMillitaryPower(1);
    B.setDefenderMillitaryPower(100);
    Board board = new Board();
    board.addTerritory(A);
    board.addTerritory(B);
    Command cr = new Command(2, "attack", B, A, 8);
    board.initializePlayers(all);
    board.prepareForAtack(cr);
    board.doAttack();
  }

  @Test
  public void test_attack3() throws Exception {
    Territory A = new Territory("A");
    Territory B = new Territory("B");
    A.addNeighbour(B);
    B.addNeighbour(A);
    A.setOwnerID(1);
    B.setOwnerID(2);
    ArrayList<Integer> all = new ArrayList<>();
    all.add(1);
    all.add(2);
    A.initializeProjectedPower(all);
    B.initializeProjectedPower(all);
    A.setDefenderMillitaryPower(100);
    B.setDefenderMillitaryPower(1);
    Board board = new Board();
    board.addTerritory(A);
    board.addTerritory(B);
    Command cr = new Command(1, "attack", A, B, 5);
    board.initializePlayers(all);
    board.prepareForAtack(cr);
    Command cr2 = new Command(1, "attack", A, B, 1);
    board.prepareForAtack(cr2);
    board.doAttack();
  }

    @Test
  public void test_attack4() throws Exception {
    Territory A = new Territory("A");
    Territory B = new Territory("B");
     Territory C = new Territory("C");
    A.addNeighbour(B);
    A.addNeighbour(C);
    B.addNeighbour(C);
    B.addNeighbour(A);
    C.addNeighbour(A);
    C.addNeighbour(B);
    A.setOwnerID(1);
    B.setOwnerID(2);
    C.setOwnerID(3);
    ArrayList<Integer> all = new ArrayList<>();
    all.add(1);
    all.add(2);
    all.add(3);
    A.initializeProjectedPower(all);
    B.initializeProjectedPower(all);
    C.initializeProjectedPower(all);
    A.setDefenderMillitaryPower(10);
    B.setDefenderMillitaryPower(1);
    C.setDefenderMillitaryPower(100);
    Board board = new Board();
    
    board.addTerritory(A);
    board.addTerritory(B);
    board.addTerritory(C);
    board.initializePlayers(all);
    Command cr = new Command(1, "attack", A, B, 6);
    board.prepareForAtack(cr);
    Command cr2 = new Command(3, "attack", C, B, 8);
    board.prepareForAtack(cr2);
    board.doAttack();
  }


  
  @Test
  public void test_attack5() throws Exception {
    Territory A = new Territory("A");
    Territory B = new Territory("B");
    A.addNeighbour(B);
    B.addNeighbour(A);
    A.setOwnerID(1);
    B.setOwnerID(2);
    ArrayList<Integer> all = new ArrayList<>();
    all.add(1);
    all.add(2);
    A.initializeProjectedPower(all);
    B.initializeProjectedPower(all);
    A.setDefenderMillitaryPower(100);
    B.setDefenderMillitaryPower(1);
    Board board = new Board();
    board.addTerritory(A);
    board.addTerritory(B);
    board.initializePlayers(all);
    Command cr = new Command(1, "attack", A, B, 5);
    board.prepareForAtack(cr);
    Command cr2 = new Command(1, "attack", A, B, 3);
    board.prepareForAtack(cr2);
    board.doAttack();
  }

  @Test
  public void test_invalid_attack() throws Exception {
    Territory A = new Territory("A");
    Territory B = new Territory("B");
    A.addNeighbour(B);
    B.addNeighbour(A);
    A.setOwnerID(1);
    B.setOwnerID(2);
    ArrayList<Integer> all = new ArrayList<>();
    all.add(1);
    all.add(2);
    A.initializeProjectedPower(all);
    B.initializeProjectedPower(all);
    A.setDefenderMillitaryPower(10);
    B.setDefenderMillitaryPower(10);
    Board board = new Board();
    board.addTerritory(A);
    board.addTerritory(B);
    try {
      Command cr = new Command(2, "attack", B, A, 100);
      board.prepareForAtack(cr);
      // board.doAttack();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  @Test
  public void test_invalid_attack2() throws Exception {
    Territory A = new Territory("A");
    Territory B = new Territory("B");
    A.addNeighbour(B);
    B.addNeighbour(A);
    A.setOwnerID(1);
    B.setOwnerID(2);
    ArrayList<Integer> all = new ArrayList<>();
    all.add(1);
    all.add(2);
    A.initializeProjectedPower(all);
    B.initializeProjectedPower(all);
    A.setDefenderMillitaryPower(10);
    B.setDefenderMillitaryPower(10);
    Board board = new Board();
    board.addTerritory(A);
    board.addTerritory(B);
    board.initializePlayers(all);
    try {
      Command cr = new Command(2, "invalid", B, A, 100);
      board.prepareForAtack(cr);
      // board.doAttack();
    } catch (Exception e) {
    }
    assertEquals(1, board.getPlayerTechLV(1));
    assertEquals(100, board.getplayerTechResource(1));
    assertEquals(3,board.getUnitCost(0));
    assertEquals(8,board.getUnitCost(1));
    assertEquals(19,board.getUnitCost(2));
    assertEquals( 25,board.getUnitCost(3));
    assertEquals(35,board.getUnitCost(4));
    assertEquals(50,board.getUnitCost(5));
    assertEquals(0,board.getUnitCost(6));
  }

  @Test
  public void test_upgrade() throws Exception {
    Territory A = new Territory("A");
    Territory B = new Territory("B");
    A.setTechResource(100);
    A.addNeighbour(B);
    B.addNeighbour(A);
    A.setOwnerID(1);
    B.setOwnerID(2);
    ArrayList<Integer> all = new ArrayList<>();
    all.add(1);
    all.add(2);
    A.initializeProjectedPower(all);
    B.initializeProjectedPower(all);
    A.setDefenderMillitaryPower(100);
    B.setDefenderMillitaryPower(100);
    Board board = new Board();
    board.addTerritory(A);
    board.addTerritory(B);
    board.initializePlayers(all);
    Command cmd1=new Command(1,"upgrade_tech",null,null,50);
    Command cmd2=new Command(1,"upgrade_unit",A,null,5,0);
    Command cmd3=new Command(1,"upgrade_unit",A,null,5,1);
    Command cmd4=new Command(1,"upgrade_unit",B,null,5,0);
    Command cmd5=new Command(1,"upgrade_unit",A,null,5,2);
    Command cmd6=new Command(1,"upgrade_unit",A,null,5,3);
    Command cmd7=new Command(1,"upgrade_unit",A,null,5,4);
    Command cmd8=new Command(1,"upgrade_unit",A,null,5,5);
    Command cmd9=new Command(1,"upgrade_unit",A,null,5,6);
    board.upgradeTech(cmd2);
    board.upgradeUnit(cmd1);
    assertThrows(Exception.class,()->board.upgradeUnit(cmd3));
    board.upgradeUnit(cmd2);
    assertThrows(Exception.class,()->board.upgradeUnit(cmd3));
    assertThrows(Exception.class,()->board.upgradeUnit(cmd4));
    board.upgradeTech(cmd1);
    assertThrows(Exception.class,()->board.upgradeTech(cmd1));
    board.endTurn();
    board.upgradeTech(cmd1);
    board.endTurn();
    board.upgradeTech(cmd1);
    board.endTurn();
    board.upgradeTech(cmd1);
    board.endTurn();
    assertThrows(Exception.class,()->board.upgradeTech(cmd1));
    board.endTurn();
    board.endTurn();
    board.endTurn();
    board.endTurn();
    board.upgradeTech(cmd1);
    board.endTurn();
    assertThrows(Exception.class,()->board.upgradeTech(cmd1));
    board.upgradeUnit(cmd3);
    assertThrows(Exception.class,()->board.upgradeUnit(cmd3));
    board.upgradeUnit(cmd5);
    board.upgradeUnit(cmd6);
    assertThrows(Exception.class,()->board.upgradeUnit(cmd7));
    board.endTurn();
    board.endTurn();
    board.endTurn();
    board.endTurn();
    board.endTurn();
    board.endTurn();
    board.endTurn();
    board.upgradeUnit(cmd7);
    board.upgradeUnit(cmd8);
    assertThrows(Exception.class,()->board.upgradeUnit(cmd9));
}
}
