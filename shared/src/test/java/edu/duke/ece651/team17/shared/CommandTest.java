package edu.duke.ece651.team17.shared;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;
public class CommandTest {
    public ArrayList<Territory> getTerrioryList(){
        ArrayList<Territory> tList=new ArrayList<>();
        Territory t1=new Territory("AZ");
        Territory t2=new Territory("NC");
        Territory t3=new Territory("FL");
        tList.add(t1);
        tList.add(t2);
        tList.add(t3);
        return tList;
    }
    @Test
    public void test_constructor(){
        Command cmd=new Command(0, null, null, null, 10,5);
        assertEquals(5, cmd.getLevel());
    }
    @Test
    public void test_invalid___command() throws Exception{
        ArrayList<Territory> ts=getTerrioryList();
        String commandString="";
        try{
            Command me=Command.toCommand("move",0,commandString,ts);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        String commandString2="1 2 3 4 5 6 7 8";
        try{
            Command me=Command.toCommand("move",0,commandString2,ts);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        String commandString3="1 2 3";
        try{
            Command me=Command.toCommand("mov",0,commandString2,ts);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    @Test
    public void test_not_exist_source() throws Exception{
        ArrayList<Territory> ts=getTerrioryList();
        String commandString="S AZ 10";
        try{
            Command me=Command.toCommand("move",0,commandString,ts);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    @Test
    public void test_not_exist_target() throws Exception{
        ArrayList<Territory> ts=getTerrioryList();
        String commandString="AZ S 10";
        try{
            Command me=Command.toCommand("move",0,commandString,ts);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    @Test
    public void test_invalid_num() throws Exception{
        ArrayList<Territory> ts=getTerrioryList();
        String commandString="AZ NC T";
        try{
            Command me=Command.toCommand("move",0,commandString,ts);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    @Test
    public void test_normal() throws Exception{
        ArrayList<Territory> ts=getTerrioryList();
        String commandString="AZ NC 10";
        try{
            Command me=Command.toCommand("move",0,commandString,ts);
            System.out.println(me.getPlayerID()+" "+me.getMoveType()+" "+me.getSource().name+" "+me.getTarget().name+" "+me.getUnitsPower());
            assertEquals(0, me.getLevel());
        }catch(Exception e){
        }
        
    }
    @Test
    public void test_abnormal() throws Exception{
        ArrayList<Territory> ts=getTerrioryList();
        String commandString="AZ NC 10";
        try{
            Command me=Command.toCommand("m",0,commandString,ts);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

      @Test
    public void test_abnormal2() throws Exception{
        ArrayList<Territory> ts=getTerrioryList();
        String commandString="AZ NC 10 10";
        try{
            Command me=Command.toCommand("m",0,commandString,ts);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    @Test 
    public void test_getters(){
        Command cmd=new Command(1,"move",null,null,2,2);
        cmd.operationType="s";
        assertEquals("s",cmd.getOperationType());
        assertEquals("s",cmd.getType());
        cmd.gameID=1;
        assertEquals(1,cmd.getGameID());
        cmd.password="123";
        assertEquals("123",cmd.getPassword());
        cmd.username="ymk";
        assertEquals("ymk",cmd.getUsername());
        cmd.playerNum=3;
        assertEquals(3,cmd.getPlayerNum());


    }
}
