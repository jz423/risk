package edu.duke.ece651.team17.shared;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class ReadFileTest {
  @Test
  public void test_ReadFile() throws FileNotFoundException, IOException {
    
    ArrayList<String> lines=ReadFile.readFileByLines(System.getProperty("user.dir")+"/../shared/mapinfo/readtest.txt");
    assertEquals("a", lines.get(0));
    assertEquals("b", lines.get(1));
    assertEquals(2,lines.size());
    
  }
  @Test
  public void test_build_map() throws FileNotFoundException, IOException{
    Board b=new Board();
    ArrayList<String> lines=ReadFile.readFileByLines(System.getProperty("user.dir")+"/../shared/mapinfo/state.txt");
    b.addTerritoryFromStrings(lines);
    lines=ReadFile.readFileByLines(System.getProperty("user.dir")+"/../shared/mapinfo/neighbour.txt");
    Boolean[][] matrix=ReadFile.getConnectionMatrix(lines);
    b.connectNeighbours(matrix);
    lines=ReadFile.readFileByLines(System.getProperty("user.dir")+"/../shared/mapinfo/A.txt");
    Integer[][] matrix1=ReadFile.getCostMatrix(lines);
    b.connectCosts(matrix1);
    ArrayList<Territory> territories=b.getTerritorys();
    Territory t=territories.get(0);
    assertEquals("ME", t.name);
    assertEquals("NH",t.getNeighbours().next().name);
    Territory t1=new Territory("NH");
    assertEquals(10, t.getNeighbourCost().get(t.getNeighbours().next()));
  }
}
