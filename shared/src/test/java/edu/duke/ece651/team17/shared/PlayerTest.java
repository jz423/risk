package edu.duke.ece651.team17.shared;

import com.sun.org.apache.xpath.internal.functions.FuncFalse;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
   /*Player test*/
   @Test
   public void test_check_win(){
       Player player=new Player(1,4444,"",new BufferedReader(new InputStreamReader(System.in)),new PrintStream(System.out));
       Territory t1=new Territory("A");
       Territory t2=new Territory("B");
       t1.setOwnerID(1);
       t2.setOwnerID(2);
       player.myBoard.addTerritory(t1);
       player.myBoard.addTerritory(t2);
       boolean iswin= player.checkWin(player.myBoard);
       assertFalse(iswin);
       t2.setOwnerID(1);
       boolean winis= player.checkWin(player.myBoard);
       assertTrue(winis);
   }
   @Test
   public void test_pick_territory() throws Exception{
       Player player=new Player(1,4444,"",new BufferedReader(new InputStreamReader(System.in)),new PrintStream(System.out));
       Territory t1=new Territory("A");
       Territory t2=new Territory("B");
       t1.setOwnerID(1);
       t2.setOwnerID(2);
       player.myBoard.addTerritory(t1);
       player.myBoard.addTerritory(t2);
       ArrayList<Territory> tsize=player.pickTerritory(player.myBoard);
       assertEquals(1,tsize.size());
       t1.setOwnerID(0);
       ArrayList<Territory> tsize2=player.pickTerritory(player.myBoard);
       assertEquals(0,tsize2.size());
   }
   @Test
   public void test_print_win() throws Exception{
       Player player=new Player(1,4444,"",new BufferedReader(new InputStreamReader(System.in)),new PrintStream(System.out));
       Territory t1=new Territory("A");
       Territory t2=new Territory("B");
       t1.setOwnerID(1);
       t2.setOwnerID(2);
       player.myBoard.addTerritory(t1);
       player.myBoard.addTerritory(t2);
       player.PrintWin(player.myBoard);
       assertFalse(player.gameOver);
       t1.setOwnerID(2);
       player.PrintWin(player.myBoard);
       assertTrue(player.gameOver);
   }
   @Test
   public void test_check_lose() throws Exception{
       Player player=new Player(1,4444,"",new BufferedReader(new InputStreamReader(System.in)),new PrintStream(System.out));
       Territory t1=new Territory("A");
       Territory t2=new Territory("B");
       t1.setOwnerID(1);
       t2.setOwnerID(2);
       player.myBoard.addTerritory(t1);
       player.myBoard.addTerritory(t2);
       player.gameOver= false;
       player.checkLose();
       assertFalse(player.gameOver);
       t1.setOwnerID(2);
       player.checkLose();
       assertTrue(player.gameOver);
   }
   @Test
   public void test_record_board(){
       Player player=new Player(1,4444,"",new BufferedReader(new InputStreamReader(System.in)),new PrintStream(System.out));
       Territory t1=new Territory("A");
       Territory t2=new Territory("B");
       t1.setOwnerID(1);
       t2.setOwnerID(2);
       player.myBoard.addTerritory(t1);
       player.myBoard.addTerritory(t2);
       Board A=new Board();
       A.currentRound=0;
       Board B=new Board();
       B.currentRound=1;
       player.myBoard.currentRound=1;
       player.boardHistory=new ArrayList<>();
       player.boardHistory.add(A);
       player.boardHistory.add(B);
       player.recordBoard();
       assertEquals(2,player.boardHistory.size());
       player.myBoard.currentRound=2;
       player.recordBoard();
       assertEquals(3,player.boardHistory.size());
   }
   @Test
   public void test_get_board(){
       Player player=new Player(1,4444,"",new BufferedReader(new InputStreamReader(System.in)),new PrintStream(System.out));
       Territory t1=new Territory("A");
       Territory t2=new Territory("B");
       t1.setOwnerID(1);
       t2.setOwnerID(2);
       player.myBoard.addTerritory(t1);
       player.myBoard.addTerritory(t2);
       Board A=new Board();
       A.currentRound=0;
       Board B=new Board();
       B.currentRound=1;
       player.myBoard.currentRound=1;
       player.boardHistory=new ArrayList<>();
       player.boardHistory.add(A);
       player.boardHistory.add(B);
       Board board=player.getBoard(1);
       assertEquals(1,B.currentRound);
       assertNull(player.getBoard(3));
   }
}