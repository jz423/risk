package edu.duke.ece651.team17.shared;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;

public class ReadFile {
    public static ArrayList<String> readFileByLines(String filePath) throws FileNotFoundException, IOException {
        File file = new File(filePath);
        BufferedReader reader = null;
        ArrayList<String> lines=new ArrayList<String>();
        reader = new BufferedReader(new FileReader(file));
        String tempString = null;
        tempString = reader.readLine();
        while (tempString != null) {
            lines.add(tempString);
            tempString = reader.readLine();
            
        }
        reader.close();
        return lines;

    }
    public static Boolean[][] getConnectionMatrix(ArrayList<String> lines){
        Boolean[][] matrix= new Boolean[lines.size()][lines.size()];
        for(int i=0;i<lines.size();i++){
            String line=lines.get(i);
            for(int j=0;j<line.length();j++){
                matrix[i][j]=(line.charAt(j)=='1');
            }
        }
        return matrix;
    }

    public static Integer[][] getCostMatrix(ArrayList<String> lines){
        Integer[][] matrix= new Integer[lines.size()][lines.size()];
        for(int i=0;i<lines.size();i++){
            String[] line=lines.get(i).split(",");
            for(int j=0;j<line.length;j++){
                matrix[i][j]=Integer.valueOf(line[j]);
                
            }
        }
        return matrix;
    }
}
