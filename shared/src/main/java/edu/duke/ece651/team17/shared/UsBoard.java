package edu.duke.ece651.team17.shared;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * The UsBoard class store all the territorys and order info. It is based on the east half of the map
 * of United States. There are totally 24 territories in this board.
 */
public class UsBoard extends Board {

  /**
   * Constructor of the UsBoard, it will read the text file and construct the board accordingly
   * @throws Exception if file cannot be opened
   */
  public UsBoard() throws FileNotFoundException, IOException{
    super();
    ArrayList<String> lines=ReadFile.readFileByLines(System.getProperty("user.dir")+"/../shared/mapinfo/state.txt");
    this.addTerritoryFromStrings(lines);
    lines=ReadFile.readFileByLines(System.getProperty("user.dir")+"/../shared/mapinfo/neighbour.txt");
    Boolean[][] matrix=ReadFile.getConnectionMatrix(lines);
    this.connectNeighbours(matrix);
    // lines=ReadFile.readFileByLines(System.getProperty("user.dir")+"/../shared/mapinfo/A.txt");
    // Integer[][] matrix1=ReadFile.getCostMatrix(lines);
    // this.connectCosts(matrix1);


  }
}
