package edu.duke.ece651.team17.shared;

import java.io.Serializable;
import java.util.HashMap;

public class Troops implements Serializable {
    private HashMap<Integer, Integer> troops;

    /* Initiallize troops */
    public Troops() {
        this.troops = new HashMap<Integer, Integer>();
        for (int i = 0; i < 7; i += 1) {
            this.troops.put(i, 0);
        }

    }
    /**
     * Change the troops
     * @param level the level to be changed
     * @param unit the number of the units to be set for that level 
     * @return true if changed successfully, false otherwise.
     */
    public boolean changeTroops(int level,int unit){
        if(level<0||level>=7||unit<0){
            return false;
        }
        this.troops.put(level,unit);
        return true;
    }
    /* Get max level that has units */
    public int getMaxLevel(){
        int i=6;
        while(i>=0){
            if(this.troops.get(i)>0){
                return i;
            }
            i--;
        }
        return 0;
    }
    /* Get the units of max level that has units*/
    public int getMaxPower(){
        int i=6;
        while(i>=0){
            if(this.troops.get(i)>0){
                return this.troops.get(i);
            }
            i--;
        }
        return 0;
    }

    /* Reduce the unit of min level that has units*/
    public void reduceMaxPower(){
        int i=6;
        while(i>=0){
            if(this.troops.get(i)>0){
                int tmp=this.troops.get(i);
                tmp--;
                this.troops.put(i, tmp);
                return;
            }
            i--;
        }
        return;
    }

    /* Get min level that has units */
    public int getMinLevel(){
        int i=0;
        while(i<7){
            if(this.troops.get(i)>0){
                return i;
            }
            i++;
        }
        return 0;
    }
    /* Get the units of min level that has units*/
    public int getMinPower(){
        int i=0;
        while(i<7){
            if(this.troops.get(i)>0){
                return this.troops.get(i);
            }
            i++;
        }
        return 0;
    }
    /* Reduce the unit of min level that has units*/
    public void reduceMinPower(){
        int i=0;
        while(i<7){
            if(this.troops.get(i)>0){
                int tmp=this.troops.get(i);
                tmp--;
                this.troops.put(i, tmp);
                return;
            }
            i++;
        }
        return;
    }
    /* Get power by level */
    public int getLVPower(int level){
        return this.troops.get(level);
    }
    /**
     * Level up 1 unit of the target level
     * @param level the target level
     */
    public void lvUp(int level, int units){
        if(level<0||level>=6){
            return;
        }
        int tmp=this.troops.get(level);
        if(tmp<units||units<1){
            return;
        }
        this.troops.put(level,tmp-units);
        tmp=this.troops.get(level+1);
        this.troops.put(level+1,tmp+units);
    }
    /**
     * Train a spy
     * @return true if trainnable, false otherwise.
     */
    public boolean trainSpy(){
        int power=this.troops.get(1);
        if(power>0){
            this.troops.put(1, power-1);
            return true;
        }
        return false;
    }

}
