package edu.duke.ece651.team17.shared;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.*;
import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Command;
import edu.duke.ece651.team17.shared.Network;
import edu.duke.ece651.team17.shared.Territory;

import static java.lang.System.exit;

public class Player {
  public Socket socket;
  private int port;
  public Board myBoard;
  public ArrayList<Board> boardHistory = new ArrayList<Board>();;
  public int playerID;
  private String address;
  public String playerName;
  public boolean hasWinGame = false;
  private boolean readyToplay= false;
  public boolean gameOver = false;
  private ArrayList<Territory> startTerritory;
  private PrintStream out;
  private BufferedReader reader;
  public ArrayList<Command> commands = new ArrayList<>();
  public Network net = new Network();
  public String tempAccountName;
  public String tempPassWord;
  public int total_units_in_the_game=20;
  public boolean isWatching=false;
  /**The constructor for client side of RISK GAME*/
  public Player(int playerID, int port, String address, BufferedReader inputSource, PrintStream out) {
    this.myBoard = new Board();
    this.playerID = playerID;
    this.port = port;
    this.address = address;
    this.reader = inputSource;
    this.out = out;
    this.playerName="";
  }
  public Player(int playerID,int port,String address){
      this.myBoard=new Board();
      this.port=port;
      this.address=address;
      this.playerID=playerID;
  }
  

 
  /**The function of check win at client side*/
  public boolean checkWin(Board board){
      ArrayList<Territory> territories=board.getTerritorys();
      boolean isWin=true;
      for(Territory t: territories){
          if(t.getOwnerID()!=this.playerID){
              isWin=false;
              break;
          }
      }
      return isWin;
  }
  

 

  
  /**Choose Group of Start Territoris and call placeUnit
   */
  public ArrayList<Territory> pickTerritory(Board board) throws Exception{
    ArrayList<Territory> chosenOnes=new ArrayList<Territory>();
    ArrayList<Territory> allTerritory=board.getTerritorys();
    for(Territory territory:allTerritory){
      if(territory.getOwnerID()==this.playerID){
        chosenOnes.add(territory);
      }
    }
   // this.startTerritory=placeUnit(chosenOnes,100);
    return chosenOnes;
  }

  
 
  public void sendAllCommands(Socket socket) throws Exception{
     this.net.sendMessage(socket,commands);
  }
  /**The logic of playing one round*/
  public void playOneRound(Command command) throws Exception{
    if(command!=null){
      this.myBoard.cloak(command);
      this.myBoard.upgradeSpy(command);
      this.myBoard.moveSpy(command);
      this.myBoard.upgradeTech(command);
      this.myBoard.upgradeUnit(command);
      this.myBoard.move(command);
      this.myBoard.prepareForAtack(command);
      }else{

      }
      commands.add(command);
   
  }
  /**Print winner If exist*/
  public void PrintWin(Board myBoard){
      HashSet<Integer> set=new HashSet<>();
      ArrayList<Territory> t=myBoard.getTerritorys();
      int winnerID=1;
      for(Territory t2:t){
          winnerID=t2.getOwnerID();
          set.add(t2.getOwnerID());
      }
      if(set.size()==1){
          System.out.println("The game has end ! The winner is Player "+winnerID);
          this.gameOver=true;
      }
  }
  /**
   * If the territory on the board does not have id that belongs to this player, this player's game is over
   */
  public void checkLose(){
    for(Territory t: this.myBoard.getTerritorys()){
      if(t.getOwnerID() == this.playerID){
        return;
      }
    }
    this.gameOver = true;
  }



  /**
   * Records the current state of the board by adding it to the board history.
   */
  public void recordBoard() {
    for(Board b:boardHistory){
      if(b.currentRound==this.myBoard.currentRound){
        return;
      }
    }
    boardHistory.add(this.myBoard);
  }

  /**
   * Returns the board at a given round index.
   *
   * @param idx The index of the round to retrieve the board for.
   * @return The board at the given round index, or null if it does not exist.
   */
  public Board getBoard(int idx){
    for(Board b:boardHistory){
      if(b.currentRound==idx){
        return b;
      }
    }
    return null;
  }
}
