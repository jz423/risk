package edu.duke.ece651.team17.shared;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Represents a game board with territories and player resources.
 */

public class Board implements Serializable {
  public ArrayList<byte[]>boardHistory;
  protected ArrayList<Territory> territorys;
  public boolean isOver = false;
  public ArrayList<Integer> playerIDs = new ArrayList<>();
  public HashMap<Integer, Integer> playerTechLV;
  public HashMap<Integer, Integer> playerTechResource;
  public HashMap<Integer, Integer> playerFoodResource;
  public HashMap<Integer, Integer> playerTechUP;
  public int currentRound;

  /**
   * initialize board
   */
  public Board() {
    this.territorys = new ArrayList<Territory>();
    playerTechLV = new HashMap<Integer, Integer>();
    playerTechResource = new HashMap<Integer, Integer>();
    playerFoodResource = new HashMap<Integer, Integer>();
    playerTechUP = new HashMap<Integer, Integer>();
    boardHistory = new ArrayList<>();
    currentRound = 0;
  }

  public void copyBoard(Board inputBoard) {
    this.territorys = inputBoard.territorys;
    this.isOver = inputBoard.isOver;
    this.playerIDs = inputBoard.playerIDs;
    this.playerTechResource = inputBoard.playerTechResource;
    this.playerFoodResource = inputBoard.playerFoodResource;
    this.playerTechLV = inputBoard.playerTechLV;
    this.playerTechUP = inputBoard.playerTechUP;
    this.currentRound = inputBoard.currentRound;
  }

  public int getPlayerTechLV(int id) {
    return playerTechLV.get(id);
  }

  public int getplayerTechResource(int id) {
    return playerTechResource.get(id);
  }

  public int getplayerFoodResource(int id) {
    
    return playerFoodResource.get(id);
  }

  /**
   * 
   * @param newTerritory Territory name
   */
  public void addTerritory(Territory newTerritory) {
    if (this.territorys.contains((newTerritory))) {
      return;
    }
    this.territorys.add(newTerritory);
  }

  /**
   * 
   * @param territoryNames Territory names
   */
  public void addTerritoryFromStrings(ArrayList<String> territoryNames) {
    for (String name : territoryNames) {
      this.addTerritory(new Territory(name));
    }

  }

  /**
   * Initialize player levels and resources
   * 
   * @param playerIDs The player IDs
   */
  public void initializePlayers(ArrayList<Integer> playerIDs) {
    this.playerIDs = playerIDs;
    for (Territory t : territorys) {
      t.initializeProjectedPower(playerIDs);
    }
    for (Integer ID : playerIDs) {
      this.playerTechResource.put(ID, 100);
      this.playerFoodResource.put(ID, 100);
      this.playerTechLV.put(ID, 1);
      this.playerTechUP.put(ID, 0);
    }
    this.updateVisibility();
  }

  public ArrayList<Territory> getTerritorys() {
    return this.territorys;
  }

  /**
   * The function for quick connect of territorys,
   * which assumes that the territorys are already added,
   * and the adjacency matrix is valid.
   * 
   * @param A the adjacency matrix, the index of A is also
   *          the index how you add in territorys
   * 
   *          eg. territorys: a b c d
   *          a has path to b , b has path to a, b has path to c
   *          then the addjacency matrix A is:
   *          a b c d
   *          a f t f f
   *          b t f t f
   *          c f f f f
   *          d f f f f
   */
  public void connectNeighbours(Boolean[][] A) {
    for (int i = 0; i < this.territorys.size(); i++) {
      for (int j = 0; j < this.territorys.size(); j++) {
        if (A[i][j]) {
          this.territorys.get(i).addNeighbour(this.territorys.get(j));
        }
      }
    }
  }

  /**
   * The function for quick connect of territorys,
   * which assumes that the territorys are already added,
   * and the adjacency matrix is valid.
   * 
   * @param A the adjacency matrix with cost, the index of A is also
   *          the index how you add in territorys
   * 
   *          eg. territorys: a b c d
   *          a has path to b , b has path to a, b has path to c
   *          then the addjacency matrix A is:
   *          a b c d
   *          a 0 2 0 0
   *          b 2 0 2 0
   *          c 0 0 0 0
   *          d 0 0 0 0
   */
  public void connectCosts(Integer[][] A) {
    for (int i = 0; i < this.territorys.size(); i++) {
      for (int j = 0; j < this.territorys.size(); j++) {
        if (A[i][j] > 0) {
          this.territorys.get(i).addNeighbourCost(this.territorys.get(j), A[i][j]);
        }
      }
    }
  }

  /**
   * getTerritory will get the territory with same name from the board
   * 
   * @param target the target territory with name, which may not be in the board
   * @return the territory with the same name form the board with neighbours
   *         connected
   */
  public Territory getTerritory(Territory target) {
    if (target != null) {
      for (Territory t : this.territorys) {
        if (t.equals(target)) {
          return t;
        }
      }
    }
    return null;
  }

  public int getUnitCost(int level) {
    switch (level) {
      case 0:
        return 3;
      case 1:
        return 8;
      case 2:
        return 19;
      case 3:
        return 25;
      case 4:
        return 35;
      case 5:
        return 50;
    }
    return 0;
  }

  /**
   * Upgrades a unit based on the given command.
   *
   * @param cmd The command containing the necessary information for the upgrade.
   * @return True if the upgrade was successful, false otherwise.
   * @throws Exception If there is insufficient tech level, funds, or units, or if
   *                   the player does not own the territory.
   */
  public boolean upgradeUnit(Command cmd) throws Exception {
    if (!cmd.getMoveType().equals("upgrade_unit")) {
      return false;
    }
    int id = cmd.getPlayerID();
    int resource = playerTechResource.get(id);
    int currlevel = playerTechLV.get(id);
    int level = cmd.getLevel();
    int units = cmd.getUnitsPower();
    System.out.println("Current units are: " + units);
    if (level + 1 > currlevel) {
      throw new Exception("Insufficient tech level");
    }
    int cost = getUnitCost(level) * units;
    if (cost > resource) {
      throw new Exception("Insufficient fund");
    }
    Territory start = getTerritory(cmd.getSource());
    if (start.getOwnerID() != id) {
      throw new Exception("you don't own this territory");
    }
    int curr_units = start.getDefenderMillitaryPower().getLVPower(level);
    if (curr_units < units) {
      throw new Exception("Insufficient unit");
    }
    playerTechResource.put(id, resource - cost);
    start.getDefenderMillitaryPower().lvUp(level, units);
    
    return true;

  }

  /**
   * upgradeTech will deal with upgrade_tech command.
   * 
   * @param cmd
   * @return ture if success,false if not success.
   * @throws Exception
   */
  public boolean upgradeTech(Command cmd) throws Exception {
    if (!cmd.getMoveType().equals("upgrade_tech")) {
      return false;
    }
    int id = cmd.getPlayerID();
    int resource = playerTechResource.get(id);
    int currlevel = playerTechLV.get(id);
    if (playerTechUP.get(id) > 0) {
      throw new Exception("you already upgraded this turn");
    }
    if (currlevel >= 6) {
      throw new Exception("you already at max level");
    }
    int cost = 1;
    for (int i = 0; i < currlevel; i++) {
      cost *= 2;
    }
    
    cost *= 10;
    if (cost > resource) {
      throw new Exception("Insufficient fund.");
    }
    playerTechResource.put(id, resource - cost);
    playerTechUP.put(id, 1);
    
    return true;
  }

  /**
   * moveSpy will do the movespy command
   *
   * @param cmd the command to be parsed
   * @return true if move successfully, false if failed
   * @throws Exception invalid calls
   */
  public boolean moveSpy(Command cmd) throws Exception {
    if (!cmd.getMoveType().equals("move spy")) {
      return false;
    }
    Territory start = getTerritory(cmd.getSource());
    Territory target = getTerritory(cmd.getTarget());
    int id = cmd.getPlayerID();
    if (start == null || target == null) {
      throw new Exception("Invalid target territory.");
    }
    if (start.spies.get(id) < 1) {
      throw new Exception("No enough spy to move.");
    }
    if (start.getOwnerID() == id && target.getOwnerID() == id) {
      isPath(start, target, cmd.getPlayerID());
      start.spies.put(id, start.spies.get(id) - 1);
      target.spies.put(id, target.spies.get(id) + 1);
      return true;
    } else {
      if (!start.isNeighbour(target)) {
        throw new Exception("Spy can't move too far.");
      }
      start.spies.put(id, start.spies.get(id) - 1);
      target.spiesBuffer.put(id, target.spiesBuffer.get(id) + 1);
      return true;
    }

  }

  /**
   * Upgrades a spy in a territory if the player has enough resources and owns the
   * territory.
   *
   * @param cmd The command object containing the move type and source territory.
   * @return True if the spy was successfully upgraded, false otherwise.
   * @throws Exception If the player does not own the territory, does not have
   *                   enough resources, or if the territory does not have a level
   *                   1 spy.
   */
  public boolean upgradeSpy(Command cmd) throws Exception {
    if (!cmd.getMoveType().equals("train spy: cost 30")) {
      return false;
    }
    Territory start = getTerritory(cmd.getSource());
    int id = cmd.getPlayerID();
    if (start.getOwnerID() != id) {
      throw new Exception("You don't own this land.");
    }
    int resource = this.playerTechResource.get(id) - 30;
    if (resource < 0) {
      throw new Exception("Insufficient fund.");
    }
    if (!start.trainSpy()) {
      throw new Exception("Insufficient LV1");
    }
    this.playerTechResource.put(id, resource);
    return true;
  }

  /**
   * Cloaks the player in the given command if the move type is "cloak".
   * The player must have a tech level of at least 3 and own the territory they
   * are cloaking in.
   * Additionally, the player must have at least 50 resources to cloak.
   *
   * @param cmd The command to execute
   * @return true if the player was cloaked, false otherwise
   * @throws Exception if the player does not meet the requirements to cloak
   */
  public boolean cloak(Command cmd) throws Exception {
    if (!cmd.getMoveType().equals("cloak: cost 50")) {
      return false;
    }
    Territory start = getTerritory(cmd.getSource());
    int id = cmd.getPlayerID();
    if (this.playerTechLV.get(id) < 3) {
      throw new Exception("Insufficient Tech lv");
    }
    if (start.getOwnerID() != id) {
      throw new Exception("You don't own this land");
    }
    int resource = this.playerTechResource.get(id) - 50;
    if (resource < 0) {
      throw new Exception("Insufficient fund");
    }
    this.playerTechResource.put(id, resource);
    start.cloak = 4;
    return true;
  }

  /**
   * move will do the move command
   *
   * @param cmd the command to be parsed
   * @return true if move successfully, false if failed
   * @throws Exception invalid calls
   */
  public boolean move(Command cmd) throws Exception {
    if (!cmd.getMoveType().equals("move")) {
      return false;
    }
    Territory start = getTerritory(cmd.getSource());
    Territory target = getTerritory(cmd.getTarget());
    if (start == null || target == null) {
      throw new Exception("Invalid target territory.");
    }
    isPath(start, target, cmd.getPlayerID());
    int distance = getDistance(start, target, cmd.getPlayerID());
    int unitsToMove = cmd.getUnitsPower();
    int levelToMove = cmd.getLevel();
    if (unitsToMove > start.getDefenderMillitaryPower().getLVPower(levelToMove) || unitsToMove < 0) {
      throw new Exception(
          "The units to be moved is invalid, should be smaller than your source defense units and be larger or equal to 0.");
    }
    int cost = distance * unitsToMove;
    if (cost > this.playerFoodResource.get(cmd.getPlayerID())) {
      throw new Exception(
          "Insufficient fund.");
    }
    this.playerFoodResource.put(cmd.getPlayerID(), this.playerFoodResource.get(cmd.getPlayerID()) - cost);
    start.setDefenderMillitaryPower(levelToMove,
        start.getDefenderMillitaryPower().getLVPower(levelToMove) - unitsToMove);
    target.setDefenderMillitaryPower(levelToMove,
        target.getDefenderMillitaryPower().getLVPower(levelToMove) + unitsToMove);
    return true;
  }

  /**
   * isPath will check if there is a path from start to target
   * 
   * @param start    the start territory
   * @param target   the end territory
   * @param playerID the player ordering the move
   * @return true if there is a path, false otherwise
   * @throws Exception invalid calls
   */
  public boolean isPath(Territory start, Territory target, int playerID) throws Exception {
    if (start.getOwnerID() != playerID || target.getOwnerID() != playerID) {
      throw new Exception("The source territory or target territory is not owned by you");

    }

    HashSet<Territory> visited = new HashSet<Territory>();
    LinkedList<Territory> nextToVisit = new LinkedList<Territory>();
    nextToVisit.add(start);
    visited.add(start);
    while (nextToVisit.size() > 0) {
      Territory current = nextToVisit.pop();
      if (current.equals(target)) {
        return true;
      }
      Iterator<Territory> neighbours = current.getNeighbours();
      while (neighbours.hasNext()) {
        Territory neighbour = neighbours.next();
        if (visited.contains(neighbour) || neighbour.getOwnerID() != playerID) {
          continue;
        }
        visited.add(neighbour);
        nextToVisit.add(neighbour);
      }

    }
    throw new Exception("There is not path between your territorys");
  }

  /**
   * getDistance will get distance between two territories belongs to a player.
   * 
   * @param start    the start territory
   * @param target   the end territory
   * @param playerID the player ordering the move
   * @return the distance
   * @throws Exception invalid calls
   */
  public int getDistance(Territory start, Territory target, int playerID) {
    HashSet<Territory> visited = new HashSet<Territory>();
    HashMap<Territory, Integer> distanceList = new HashMap<Territory, Integer>();
    ArrayList<Territory> nextToVisit = new ArrayList<Territory>();
    nextToVisit.add(start);
    distanceList.put(start, 0);
    while (nextToVisit.size() > 0) {
      Territory current = minHelper(distanceList, nextToVisit);
      Iterator<Territory> neighbours = current.getNeighbours();
      while (neighbours.hasNext()) {
        Territory neighbour = neighbours.next();
        if (visited.contains(neighbour) || neighbour.getOwnerID() != playerID) {
          continue;
        }
        // if(distanceList.containsKey(neighbour)&&
        // current.getNeighbourCost().get(neighbour)
        // +distanceList.get(current)>=distanceList.get(neighbour)){
        // continue;
        // }
        if (distanceList.containsKey(neighbour) && 10 + distanceList.get(current) >= distanceList.get(neighbour)) {
          continue;
        } else {
          nextToVisit.add(neighbour);
          distanceList.put(neighbour, 10 + distanceList.get(current));
        }

      }

    }
    return distanceList.get(target);

  }

  private Territory minHelper(HashMap<Territory, Integer> distanceList, ArrayList<Territory> nextToVisit) {
    Territory ans = nextToVisit.get(0);
    int minDist = distanceList.get(ans);
    for (Territory t : nextToVisit) {
      if (distanceList.get(t) < minDist) {
        ans = t;
        minDist = distanceList.get(t);
      }
    }
    nextToVisit.remove(ans);
    return ans;
  }

  /**
   * This function will validate the attack command and update the unit for each
   * attack command
   * 
   * @param curr is the Command being processed
   */
  public void prepareForAtack(Command curr) throws Exception {
    String moveType = curr.getMoveType();
    System.out.println("moveType is: " + moveType);
    System.out.println("The level is: " + curr.getLevel());
    if (moveType.equals("attack")) {
      AttackChecker ac = new AttackChecker();
      Territory source = getTerritory(curr.getSource());
      Territory target = getTerritory(curr.getTarget());
      int unit = curr.getUnitsPower();
      int id = curr.getPlayerID();
      int level = curr.getLevel();
      // check if the attack command is valid
      System.out.println("source is: " + source);
      System.out.println("target is: " + target);
      System.out.println("-------------------");
      Iterator<Territory> iter = source.getNeighbours();
      while (iter.hasNext()) {
        System.out.println(iter.next());
      }
      System.out.println("-------------------");
      Iterator<Territory> iter2 = target.getNeighbours();
      while (iter2.hasNext()) {
        System.out.println(iter2.next());
      }
      ac.checkValidation(id, source, target, unit, level);

      // int dist=source.getNeighbourCost().get(target);
      int dist = 10;
      int cost = unit * level + unit * dist;
      int resource = this.getplayerFoodResource(id);
      if (cost > resource) {
        throw new Exception("No enough resources.");
      }
      // update source unit
      int defenceUnit = source.getDefenderMillitaryPower().getLVPower(level);
      defenceUnit -= unit;
      source.setDefenderMillitaryPower(level, defenceUnit);
      // update target unit
      int attackUnit = target.getProjectedPower(id).getLVPower(level);
      attackUnit += unit;
      target.setProjectedPower(id, level, attackUnit);
      // update resource
      this.playerFoodResource.put(id, resource - cost);
    }

  }

  /**
   * This function will do all the attack phase on the board in one round and
   * update the result of
   * the combat in each Territory.
   */
  public void doAttack() {
    for (int i = 0; i < territorys.size(); i++) {
      Territory curr = territorys.get(i);
      curr.decideAttackWinner();
    }
  }

  /**
   * Count the number of territorys owned by a player
   * 
   * @param playerID the player's id which is int
   * @return the number of territorys owned by a player
   */
  public int countTerritoryByPlayerID(int playerID) {
    int count = 0;
    for (Territory t : this.territorys) {
      if (t.getOwnerID() == playerID) {
        count++;
      }
    }
    return count;
  }

  /**
   * Detect if a player lose
   * 
   * @param playerID the player's id which is int
   * @return true if player lose, false otherwise
   */
  public boolean isPlayerLose(int playerID) {
    return countTerritoryByPlayerID(playerID) == 0;
  }

  /**
   * Updates the visibility of territories for each player in the game.
   * If a player owns a territory or has spies in a territory, they can see it.
   * If a territory is cloaked, it is not visible to any player.
   * If a player does not own a territory or have spies in it, but has a
   * neighboring territory that they own,
   * they can see the neighboring territory.
   */
  public void updateVisibility() {
    for (int id : this.playerIDs) {
      for (Territory t : this.territorys) {
        if (t.spies.get(id) > 0 || t.getOwnerID() == id) {
          t.visibility.put(id, this.currentRound);
        } else if (t.cloak > 0) {
          continue;
        } else {
          Iterator<Territory> it = t.getNeighbours();
          while (it.hasNext()) {
            Territory neighbour = it.next();
            if (neighbour.getOwnerID() == id) {
              t.visibility.put(id, this.currentRound);
            }
          }
        }
      }
    }
  }

  /**
   * Executes a list of commands in the game.
   *
   * @param cmd The list of commands to execute.
   */
  public void doCommands(ArrayList<Command> cmd) {
    for (Command command : cmd) {

      try {
        this.cloak(command);
        this.upgradeSpy(command);
        this.moveSpy(command);
        this.upgradeTech(command);
        this.upgradeUnit(command);
        this.move(command);
        this.prepareForAtack(command);
      } catch (Exception e) {
        // TODO Auto-generated catch block
        System.out.println(e.getMessage());
      }

    }
  }

  /**
   * Update board with resource and new units for each turn
   */
  public void endTurn() {
    System.out.println("end turn is called!!!!!!!");
    for (int id : this.playerIDs) {
      if (this.countTerritoryByPlayerID(id) == 0) {
        continue;
      }
      int techResource = this.playerTechResource.get(id);
      int foodResource = this.playerFoodResource.get(id);
      for (Territory t : this.territorys) {
        if (t.getOwnerID() == id) {
          if (t.cloak > 0) {
            t.cloak--;
          }
          techResource += t.getTechResource();
          foodResource += t.getFoodResource();
          int pow = t.getDefenderMillitaryPower().getLVPower(0);
          t.getDefenderMillitaryPower().changeTroops(0, pow + 1);
          t.spyBufferMerge();
        }
      }
      this.playerTechResource.put(id, techResource);
      this.playerFoodResource.put(id, foodResource);
      if (this.playerTechUP.get(id) > 0) {
        this.playerTechUP.put(id, 0);
        this.playerTechLV.put(id, this.playerTechLV.get(id) + 1);
      }
    }
    this.currentRound += 1;
    updateVisibility();
  }

}
