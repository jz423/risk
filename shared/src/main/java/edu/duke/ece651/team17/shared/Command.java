package edu.duke.ece651.team17.shared;

import java.io.Serializable;
import java.util.ArrayList;

public class Command implements Serializable{
  private int playerID;
  public String moveType;
  private Territory source; 
  private Territory target;
  private int unitsPower;
  private int level;
  public String operationType;
  public String username;
  public String password;
  public int gameID;
  public int playerNum;
  /* Initializer for Command */
  public Command(int playerID, String moveType, Territory source, Territory target, int unitsPower, int level){
    this.playerID = playerID;
    this.moveType = moveType;
    this.source = source;
    this.target = target;
    this.unitsPower=unitsPower;
    this.level=level;
  }
  public Command(){}
  public int getPlayerNum(){
    return this.playerNum;
  }
  public String getType(){
    return this.operationType;
  }
  public String getUsername(){
    return this.username;
  }
  public String getPassword(){
    return this.password;
  }
  public int getGameID(){
    return this.gameID;
  }
  /* Wrapper initializer*/
  public Command(int playerID, String moveType, Territory source, Territory target, int unitsPower){
    this.playerID = playerID;
    this.moveType = moveType;
    this.source = source;
    this.target = target;
    this.unitsPower=unitsPower;
    this.level=0;
  }
  public String getOperationType(){
    return this.operationType;
  }
  /**
     return the command's player ID
  */
  public int getPlayerID(){
    return this.playerID;
  }
  /**
     return the command's move type
  */ 
  public String getMoveType(){
    return this.moveType;
  }
   /**
      return the command's source
   */
  public Territory getSource(){
    return this.source;
  }
   /**
      return the command's target
   */
  public Territory getTarget(){
    return this.target;
  }
  /**
      return the command's units power
   */
  public int getUnitsPower(){
    return this.unitsPower;
  }

  /**
      return the command's target level
   */
  public int getLevel(){
    return this.level;
  }
  /** convert Input string to command*/
  public static Command toCommand(String moveType,int playerID,String commandString, ArrayList<Territory> myTerritory) throws Exception{
     if(commandString.length()==0){
       throw new IllegalArgumentException("Command String must have length>0");
     }
     String[]commandArray=commandString.split(" ");
     ArrayList<String> names=new ArrayList<>();
     for(Territory t:myTerritory){
       names.add(t.name);
     }
     System.out.println(commandString+" "+commandArray.length);
     if(commandArray.length!=3){
         throw new Exception("Please provide valid command as required!!!!!!!!");
     }
     Command thisCommand=new Command(0,null,null,null,0);
     thisCommand.playerID=playerID;
     boolean validType=moveType.equalsIgnoreCase("move")||moveType.equalsIgnoreCase("attack");
     if(!validType){
         throw new Exception("Please provide a valid move type!!!!!");
     }
     thisCommand.moveType=moveType;
     for(int i=0;i< commandArray.length;i++){
        if(i==0){
            String thisC=commandArray[i];
            if(names.contains(thisC)){
               thisCommand.source=myTerritory.get(names.indexOf(thisC));
            }else{
              throw new IllegalArgumentException("Must provide valid territory name");
            }
         }else if(i==1){
           String thisC=commandArray[i];
           if(names.contains(thisC)){
             thisCommand.target=myTerritory.get(names.indexOf(thisC));
           }else{
             throw new IllegalArgumentException("Must provide valid territory name");
           }
         }else{
            String units=commandArray[i];
            try{
              int unit_num=Integer.parseInt(units);
              thisCommand.unitsPower=unit_num;
            }catch(Exception e){
               throw new Exception("You must provide a valid Integer!!!!!!!!");
            }
         }
     }
     return thisCommand;
  }
}
