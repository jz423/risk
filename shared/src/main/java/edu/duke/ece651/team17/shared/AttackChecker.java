package edu.duke.ece651.team17.shared;

import java.util.Iterator;

/**
 * PRE-attack order Class will set up rules to check if the attack command is
 * valid
 */
public class AttackChecker {
  public AttackChecker() {
  }

  /**
   * This is to check if the source and target are neighbours
   * 
   */
  private boolean checkNeighbour(Territory source, Territory target) throws Exception {
    Iterator<Territory> it = source.getNeighbours();
    while (it.hasNext()) {
      if (it.next().getOwnerID() == target.getOwnerID()) {
        return true;
      }
    }
    throw new Exception("The source and target territories are not neighbours! Please enter again: ");
  }

  /**
   * This is to check fi the unit input is valid
   */
  private boolean checkValidUnit(Territory attackSource, int attackUnit, int level) throws Exception {
    if (attackSource.getDefenderMillitaryPower().getLVPower(level) < attackUnit|| attackUnit<0) {
      throw new Exception("The unit you you want attack is larger than the territory, which must be less or equals to "
          + attackSource.getDefenderMillitaryPower().getLVPower(level) + "! Please enter again:");
    }
    return true;
  }

  /**
   * This is to check if the ID is correct
   */
  private boolean checkID(int attackId, Territory attackSource, Territory attackTarget) throws Exception {
    if (attackId != attackSource.getOwnerID()) {
      throw new Exception("The attack source is not your own territory! Please input again: ");
    }
    if (attackId == attackTarget.getOwnerID()) {
      throw new Exception("You cannot attack your own territory! Please input again: ");
    }
    return true;
  }

  /**
   * @param source is the source where the attack starts
   * @param target is the target where the attack heading to
   * @throw Exception if the source and the target are not adjacent
   * @return true if is valid
   */
  public boolean checkValidation(int id, Territory source, Territory target, int unit ,int level) throws Exception {
    return checkNeighbour(source, target) &&
        checkNeighbour(target, source) &&
        checkID(id, source, target) &&
        checkValidUnit(source, unit,level);
  }

  public boolean checkValidation(int id, Territory source, Territory target, int unit) throws Exception {
    return checkValidation(id, source, target, unit,0);
  }
}
