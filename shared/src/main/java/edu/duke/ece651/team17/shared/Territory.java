package edu.duke.ece651.team17.shared;

import java.io.Serializable;
import java.nio.Buffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

/**
 * Territory is the class that will hold information and game state for a
 * territory,
 * it will also performs the attacks.
 */
public class Territory implements Serializable {

  public final String name;
  private ArrayList<Territory> neighbours;
  private HashMap<Territory,Integer> neighbourCost;
  private int OwnerID;
  private HashMap<Integer, Troops> projectedMillitaryPower;
  private Troops defenderMillitaryPower;
  private int diceSide = 20;
  private int techEachTurn;
  private int foodEachTurn;
  public HashMap<Integer, Integer> spies;
  public HashMap<Integer, Integer> spiesBuffer;
  public HashMap<Integer,Integer> visibility;
  public int cloak=0;
  /**
   * Construct a territory with its given name
   * 
   * @param name the name of the territory
   */
  public Territory(String name) {
    this.name = name;
    this.neighbours = new ArrayList<Territory>();
    this.projectedMillitaryPower = new HashMap<Integer, Troops>();
    this.spies = new HashMap<Integer,Integer>();
    this.spiesBuffer = new HashMap<Integer,Integer>();
    this.visibility = new HashMap<Integer,Integer>();
    this.OwnerID = -1;
    this.defenderMillitaryPower = new Troops();
    this.techEachTurn=0;
    this.foodEachTurn=0;
    this.neighbourCost=new HashMap<Territory,Integer> ();
  }
  /**
   * add neighbour cost to cost table
   * @param t neighbour
   * @param cost travel cost
   */
  public void addNeighbourCost(Territory t, int cost){
    this.neighbourCost.put(t, cost);
  }
  
  public HashMap<Territory,Integer> getNeighbourCost(){
    return this.neighbourCost;
  }
  /**
   * Set the tech resource of this territory
   * 
   * @param resource the resource
   */
  public void setTechResource(int resource) {
    this.techEachTurn = resource;
  }
  
  /**
   * get the tech resource of this territory
   * 
   * @param resource the resource
   */
  public int getTechResource() {
    return this.techEachTurn;
  }
  /**
   * Set the food resource of this territory
   * 
   * @param resource the resource
   */
  public void setFoodResource(int resource) {
    this.foodEachTurn = resource;
  }

  /**
   * get the food resource of this territory
   * 
   * @param resource the resource
   */
  public int getFoodResource() {
    return this.foodEachTurn;
  }
  /**
   * Set the owner of this territory
   * 
   * @param newID the owner's ID
   */
  public void setOwnerID(int newID) {
    this.OwnerID = newID;
  }

  /**
   * Get the owner of this territory
   * 
   * @return the owner's ID
   */
  public int getOwnerID() {
    return this.OwnerID;
  }

  /**
   * Get the defense millitary power of this territory
   *
   * @return the defense power
   */
  public Troops getDefenderMillitaryPower() {
    return this.defenderMillitaryPower;
  }

  /**
   * Set the defense millitary power of this territory
   * 
   * @param level the target level
   * @param newPower the defense millitary power, should be greater or equal than
   *                 0
   * @return true if successfully set, false if failed to set.
   */
  public boolean setDefenderMillitaryPower(int level,int newPower) {
    if (newPower < 0) {
      return false;
    }
    
    return this.defenderMillitaryPower.changeTroops(level, newPower);
  }
  public boolean setDefenderMillitaryPower(int newPower) {
    return setDefenderMillitaryPower(0, newPower);
  }
  /**
   * This will initialize projectedMillitaryPower with given playerIDs.It sets
   * each playerID in hashmap with power 0.
   * 
   * @param playerIDs All playerID in game.
   */
  public void initializeProjectedPower(ArrayList<Integer> playerIDs) {
    for (Integer ID : playerIDs) {
      this.projectedMillitaryPower.put(ID, new Troops());
      this.spies.put(ID,0);
      this.spiesBuffer.put(ID,0);
      this.visibility.put(ID,-1);
    }
  }
  public boolean isNeighbour(Territory t){
    return this.neighbours.contains(t);
  }
  /**
   * At the end of each turn, the spy in spyBuffer arrive at the territory.
   */
  public void spyBufferMerge(){
    for(Integer ID:spiesBuffer.keySet()){
      spies.put(ID,spies.get(ID)+spiesBuffer.get(ID));
      spiesBuffer.put(ID,0);
    }
  }
  /**
   * Get the millitary power of the player
   * 
   * @param playerID the player id
   * @return the millitary power of the player,null if player not exist
   */
  public Troops getProjectedPower(int playerID) {
    if (!this.projectedMillitaryPower.containsKey(playerID)) {
      return null;
    }
    return this.projectedMillitaryPower.get(playerID);
  }

  /**
   * Set the millitary power of the player
   * 
   * @param playerID the player id
   * @param power    the new power
   * @param level the level of the unit to be changed
   */
  public void setProjectedPower(int playerID, int level ,int power) {
    if (this.projectedMillitaryPower.containsKey(playerID)) {
      this.projectedMillitaryPower.get(playerID).changeTroops(level, power);
    }
  }

  /**
   * Add neighbour to a territory, if add self, it will do nothing.
   * 
   * @param neighbour the neighbour territory to be added
   */
  public void addNeighbour(Territory neighbour) {
    if (this.equals(neighbour)) {
      return;
    }
    for (Territory t : neighbours) {
      if (t.equals(neighbour)) {
        return;
      }
    }
    this.neighbours.add(neighbour);
  }

  /**
   * getNeighbours will return a iterator of its neighbours.
   * 
   * @return iterator of its neighbours
   */

  public Iterator<Territory> getNeighbours() {
    return this.neighbours.iterator();
  }

  /**
   * this method roll a dice to decide which side wins, the loser will loose one
   * unit
   *@param attacker is the id of one player involved in the combat
   *@param defencer is the id of another player involved in the combat 
   */
  public void doCombat(int attacker, int defencer) {
    Random rand = new Random();
    boolean haveResult = false;
    while (!haveResult) {
      int attackDice = rand.nextInt(getDice(this.projectedMillitaryPower.get(attacker).getMaxLevel()));
      int defenceDice = rand.nextInt(getDice(this.projectedMillitaryPower.get(attacker).getMinLevel()));
      if (attackDice > defenceDice) {
        int unit = getProjectedPower(defencer).getMinPower();
        unit--;
        setProjectedPower(defencer,getProjectedPower(defencer).getMinLevel(), unit);
        haveResult = true;
      } else if (attackDice < defenceDice) {
        int unit = getProjectedPower(attacker).getMaxPower();
        unit--;
        setProjectedPower(attacker,getProjectedPower(attacker).getMaxLevel() ,unit);
        haveResult = true;
      } else {
      }
    }
  }
  /**
   * get Dice
   * @param level
   * @return Dice of a level
   */
  public int getDice(int level){
    switch(level){
      case 0: return diceSide+0;
      case 1: return diceSide+1;
      case 2: return diceSide+3;
      case 3: return diceSide+5;
      case 4: return diceSide+8;
      case 5: return diceSide+11;
      case 6: return diceSide+15;
    }
    return 0;
  }
  /**
   * TrainSpy will try to train a spy in this territory
   * @return true if spy is trained, false otherwise
   */
  public boolean trainSpy(){
    if(this.defenderMillitaryPower.trainSpy()){
      this.spies.put(this.OwnerID,this.spies.get(this.OwnerID)+1);
      return true;
    }
    else{
      return false;
    }
  }

  /*
   * this method will do the attack until there is a winner among them, the winner will become the 
   * new owner of the territory and the defencePower will be updated
   */
  public void decideAttackWinner() {
    projectedMillitaryPower.put(OwnerID, defenderMillitaryPower);
    ArrayList<Integer> players = new ArrayList<Integer>();
    projectedMillitaryPower.forEach((key, value) -> players.add(key));
    boolean isAttack = false;
    while (players.size() != 1) {
      // check if one player's unit is 0, if is, remove from combat list
      for (Map.Entry<Integer, Troops> set : projectedMillitaryPower.entrySet()) {
        if (set.getValue().getMinPower() == 0) {
          players.remove(Integer.valueOf(set.getKey()));
        }
      }

      if ((players.size() == 1)) {
        continue;
      }
      if ((players.size() == 0)) {
        break;
      }

      // one turn of combat
      for (int i = 0; i < players.size(); i++) {
        int attacker = -1;
        int defencer = -1;
        if ((i + 1) < players.size()) {
          attacker = players.get(i);
          defencer = players.get(i + 1);
          if ((getProjectedPower(attacker).getMinPower() == 0) || (getProjectedPower(defencer).getMinPower()  == 0)) {
            continue;
          }
          doCombat(attacker, defencer);
          isAttack = true;
        } else {
          attacker = players.get(i);
          defencer = players.get(0);
          if ((getProjectedPower(attacker).getMinPower()  == 0) || (getProjectedPower(defencer).getMinPower()  == 0)) {
            continue;
          }
          doCombat(attacker, defencer);
          isAttack = true;
        }
      }
    }
    if (isAttack || players.size()>0 &&((int)players.get(0)!=OwnerID)) {
      // winner appears, update owership and defence unit
      int winner = players.get(0);
      this.setOwnerID(winner);
      this.defenderMillitaryPower = getProjectedPower(winner);
      StringBuilder str = new StringBuilder();
      str.append("the winner for combat at ");
      str.append(this.name);
      str.append(" is ");
      str.append(winner);
      System.out.println(str.toString());
    }
  }

  /**
   * @inheritDoc equals
   */
  @Override
  public boolean equals(Object o) {
    if (o.getClass().equals(getClass())) {
      Territory c = (Territory) o;
      return this.name.equals(c.name);
    }
    return false;
  }

  /**
   * @inheritDoc toString()
   */
  @Override
  public String toString() {
    return this.name;
  }

  /**
   * @inheritDoc hashCode
   */
  @Override
  public int hashCode() {
    return toString().hashCode();
  }

}
