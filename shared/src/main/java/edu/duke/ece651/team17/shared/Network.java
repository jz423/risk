package edu.duke.ece651.team17.shared;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.*;

public  class Network {
  public ObjectOutputStream ObjectOut;
  public ObjectInputStream ObjectIn;
  public Network() {

  }

  /**
   * It is for sending any type of message
   */
  public <T> void sendMessage(Socket socket, T message) throws Exception {
      this.ObjectOut = new ObjectOutputStream(socket.getOutputStream());
      this.ObjectOut.writeObject(message);
      this.ObjectOut.flush();
  }

  /**
   * It is for receiving any type of message from socket
   */
  public <V> V recvMessage(Socket socket) throws Exception{
    V message = null;
    this.ObjectIn = new ObjectInputStream(socket.getInputStream());
    message = (V) (this.ObjectIn.readObject());

    return message;
  }

}
