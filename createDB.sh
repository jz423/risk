#!/bin/bash

# Replace with your actual PostgreSQL database credentials
DATABASE_NAME="risk"
SQL_FILE="./query.sql"
PASSWORD="password"
SQL_STATEMENT_1="drop database if exists risk;"
SQL_STATEMENT_2="create database risk;"
SQL_STATEMENT_3="\c risk;"
SQL_STATEMENT_4="\i $SQL_FILE;"

export PGPASSWORD=$PASSWORD
psql -U postgres << EOF
$SQL_STATEMENT_1 
$SQL_STATEMENT_2
$SQL_STATEMENT_3
$SQL_STATEMENT_4
