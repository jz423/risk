package edu.duke.ece651.team17.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import edu.duke.ece651.team17.shared.*;

public class Server {
  public Board board;
  public ArrayList<Socket>client_socket_list;
  public ServerSocket server;
  private int port;
  public int playerNum ;
  public BufferedReader reader;
  public PrintStream out;
  public Network net = new Network();
  /** constructor for server*/
  public Server(int port, BufferedReader reader, PrintStream out) throws Exception {
    this.reader = reader;
    this.out = out;
    this.port = port;
    this.board = new Board();
    initialPlayerNumber();
    this.client_socket_list = new ArrayList<>();
    this.server = new ServerSocket(port);
  }
  /**The code for ending one turn*/
  public void endTurn(){
      ArrayList<Territory> territoys=this.board.getTerritorys();
      for(Territory t:territoys){
//          t.setDefenderMillitaryPower(t.getDefenderMillitaryPower()+1);
      }
      out.println("One turn end!!!!!!!!");
  }
  /** The logic of playing one round for risk game*/
  public void playOneRound() throws Exception {
    ArrayList<Command> moveCommands=new ArrayList<>();
    ArrayList<Command> attackCommands=new ArrayList<>();
    int Iterator = 0;
    while(Iterator < playerNum){
      ArrayList<Command> commands= tryRecvMessage(client_socket_list.get(Iterator));
      if(commands == null) continue;
      for(Command command:commands){
          if(command.getMoveType().equalsIgnoreCase("move")){
              moveCommands.add(command);
          }else if(command.getMoveType().equalsIgnoreCase("attack")){
              attackCommands.add(command);
          }
      }
      ++Iterator;
    }
    for(Command movecommand:moveCommands){
        try{
            System.out.println(this.board.move(movecommand));
        }catch(Exception e){
            out.println(e.getMessage());
        }
    }
    for(Command command:attackCommands){
        try{
            this.board.prepareForAtack(command);
        }catch(Exception e){
            out.println(e.getMessage());
        }
    }

    this.board.doAttack();
    endTurn();
    for(int i=0;i<playerNum;i++){
      trySendMessage(client_socket_list.get(i),this.board);
    }
  }
  /**
   * Send ready Message to client
   */
  public void sendReadyMsg(Socket socket) {
      boolean b = true;
      trySendMessage(socket, b);
  }
  /**
   * Send gameOver Message to client
   */
  public void sendOverMsg(Socket socket) {
      boolean b = true;
      trySendMessage(socket, b);
  }
  /**
   * Send its board to the specificed client
   */
  public void sendBoard(Socket socket) {
      trySendMessage(socket, this.board);
  }


  /**
   * This function tries to send messages, avoiding one client disconnect
   */
  public<T> void trySendMessage(Socket socket, T message){
    try{
      net.sendMessage(socket, message);
    }catch(Exception e){
      this.playerNum--;
      client_socket_list.remove(socket);
      if(this.client_socket_list.size() == 0){
        out.println("No players, game is over");
        return;
      }      
    }
  }  

/**
   * This function tries to receives message, avoiding one client disconnect
   */
  public<T> T tryRecvMessage(Socket socket){
    T message = null;
    try{
      message = net.recvMessage(socket);
    }catch(Exception e){
      this.playerNum--;
      client_socket_list.remove(socket);
      if(this.playerNum == 0){
        out.println("No players, game is over");
        return null;
      }   
    }
    return message;
  }
  
  /**
   * It is for the server to build connection to the client
   */
  public void buildConnection() throws Exception{
      out.println("Server connected");
      for (int i = 0; i < this.playerNum; i++) {
        client_socket_list.add(this.server.accept());
        trySendMessage(client_socket_list.get(i),i+1);
        out.println("Server to Client "+(i+1)+" connection successfull");
      }
  }
  /**
   * This function is for initializing the board for starting game.
   * It gets the classified board.
   * 
   * @return Map<String, ArrayList<Territory>>: color and its territory
   * @throws Exception
   */
  public Board getInitalBoard() throws Exception {
    // get 24 territory
    Board startBoard = new UsBoard();
    int[] listClientID = new int[] { 1, 2, 3, 4 };
    int i = 1;
    for (int j = 0; j < 24; j++) {
      startBoard.getTerritorys().get(j).setOwnerID(listClientID[i-1]);
      if (j % (24 / playerNum) == (24 / playerNum - 1))
        ++i;
      if (j == 23)
        break;
    }
    ArrayList<Integer> ids=new ArrayList<>();
    for(int k=1;k<=playerNum;k++){
        ids.add(k);
    }
    startBoard.initializePlayers(ids);
    return startBoard;
  }
  /** This method will reflect the changes of the territorys on server board*/
  public void updateBoard(ArrayList<Territory> ts){
     ArrayList<Territory> currentTerritorys=this.board.getTerritorys();
     ArrayList<String> names=new ArrayList<>();
     for(Territory territory:ts){
       names.add(territory.name);
     }
     for(int i=0;i<currentTerritorys.size();i++){
         Territory t=currentTerritorys.get(i);
          if(names.contains(t.name)){
              Territory changedTerritory=ts.get(names.indexOf(t.name));
             t.setDefenderMillitaryPower(changedTerritory.getDefenderMillitaryPower().getLVPower(0));
          }
     }
  }
  /** this function is about process territory
   * While the server received error message from one player, it means the player has not finished unit placement
   * Otherwise,the server will successfully recieved changed territory from player which means place unit finished*/
  public void processTerritory(){
      int i=0;
      int cnt=0;
      String errorMessage="error";
      ArrayList<Integer> finishedOnes=new ArrayList<>();
      ArrayList<Territory> temp=new ArrayList<>();
      while(cnt<playerNum){
         Object msg=tryRecvMessage(client_socket_list.get(i));
         if(msg == null){
          continue;
         }
         if(msg.getClass()!=errorMessage.getClass()&&msg.getClass()==temp.getClass()&&(finishedOnes.contains(i)==false)){
            finishedOnes.add(i);
            ArrayList<Territory> ts=(ArrayList<Territory>)msg;
            cnt+=1;
            updateBoard(ts);
         }
          i=(i+1)%playerNum;
      }
      out.println("The Initial Unit Placement Phase is done!!!!!!");
      for(int j=0;j<playerNum;j++){
        sendReadyMsg(client_socket_list.get(j));
      }
  }
  /**
   * This function is for the game prepration: buildconnection, let client assign
   * their unit on each territory
   * Remember that the territories are assigned to the player.
   * 
   * @throws Exception
   */
  private void gamePrepration() throws Exception {
    buildConnection();
    this.board = getInitalBoard();
      
    for (int i = 0; i < playerNum; i++) {
      sendBoard(this.client_socket_list.get(i));
    }
    processTerritory();
    for(int m=0;m< playerNum;m++){
      sendBoard(client_socket_list.get(m));
    }
  }

  /**
   * The main function that playes the game
   */
  public void playGame() throws Exception {
    gamePrepration();
    while (!this.board.isOver) {
      playOneRound();
      checkWinner();
    }
    closeSocket();
    this.server.close();
  }
  /**
   * Close socket
   * @throws IOException
   */
  public void closeSocket() throws IOException{
    if(this.client_socket_list.size() != 0){
      for(Socket socket: this.client_socket_list){
        socket.close();
      }
    }
  }
  /**
   * check if there is a winner, if there is, this.gameOver = true;
   */
  public void checkWinner(){
    int prevId=-1;
    int currId=-1;
    if(playerNum == 0){
      this.out.println("No player");
      this.board.isOver = true;
      return;
    }
    for(int i =0;i<this.board.getTerritorys().size() - 1;i++){
      prevId = board.getTerritorys().get(i).getOwnerID();
      currId = board.getTerritorys().get(i+1).getOwnerID();
      if(prevId == currId){
        continue;
      }
      else{
        return;
      }
    }
    this.out.println("The winner is "+prevId);
    this.board.isOver = true;
  }
    /**
     * This function initalize the number of player
     */
    public void initialPlayerNumber() throws Exception{
      this.out.println("Please enter the number of players You accept(You must enter number between 2 and 4: ");
        while(true){
            try{
                int num =Integer.parseInt(reader.readLine());
                if(num > 4 || num < 2){
                  this.out.println("Please enter a number between 2 to 4");
                  continue;
                }
                this.playerNum = num;
                break;
            }catch(Exception e){
              this.out.println("Please enter a number between 2 to 4");
            }
        }
    }
}
