package edu.duke.ece651.team17.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import edu.duke.ece651.team17.shared.*;
import org.checkerframework.checker.units.qual.s;

import edu.duke.ece651.team17.shared.*;

import static java.lang.System.exit;

public class Game implements Runnable {
    public Board board;
    public boolean started = false;
    public HashMap<Socket, Integer> socket_map;
    public ArrayList<Socket> client_socket_list;
    public ArrayList<Integer> id_new; // for the current: player leave, join
    public ArrayList<Integer> all_id;
    public int capacity = 0;
    public ServerSocket server;
    private int port;
    // public int playerNum;
    public BufferedReader reader;
    public PrintStream out;
    public Network net = new Network();
    public int gameID;
    public ArrayList<Integer> fullKnowers;
    public real_server tserver;
    public boolean ready=false;
    public boolean reopen=false;
    /** constructor for real server*/

    public Game(int playerNum, BufferedReader reader, PrintStream out) throws Exception {
        this.reader = reader;
        this.out = out;
        this.capacity = playerNum;
        this.board = new Board();
        this.client_socket_list = new ArrayList<>();
        this.all_id = new ArrayList<>();
        this.socket_map = new HashMap<>();
        this.id_new = new ArrayList<>();
        this.fullKnowers = new ArrayList<>();
    }

    public void run() {
        try {
            
            playGame();
            
        } catch (Exception e) {
            e.printStackTrace();
            
        }
    }

    public int getId() {
        return gameID;
    }

    public void removeClient(int id) {
        if (id_new.contains(id)) {
            id_new.remove(id);
            for (Socket s : socket_map.keySet()) {
                if (socket_map.get(s) == id) {
                    client_socket_list.remove(s);
                    socket_map.remove(s);
                }
            }
        }
    }

    public boolean isFull() {
        return all_id.size() == this.capacity;
    }

    public boolean addPlayer(Socket socket, int id) {
        
        if(all_id.size()<capacity){
            socket_map.put(socket,id);
            if(id_new.contains(id)==false){
                id_new.add(id);
             }
            all_id.add(id);
            client_socket_list.add(socket);
            return true;
        } else if (all_id.contains(id)) {
            socket_map.put(socket, id);
            client_socket_list.add(socket);
            if(id_new.contains(id)==false){
                id_new.add(id);
             }
            return true;
        } else {
            return false;
        }
    }

    /** The logic of playing one round for risk game */
    /** The logic of playing one round for risk game */
    public void playOneRound() throws Exception {
        this.ready = true;
        ArrayList<Command> allCommands = new ArrayList<>();
       
        
        Collections.sort(id_new);
        if (id_new.size() == all_id.size()) {
            
            if (id_new.size() <= 0) {
                return;
            }
            int i = 0;
            while (i < id_new.size()) {
                int clientID = id_new.get(i);
                // Error here
                // int clientID=socket_map.get(s);
                if (socket_map.values().contains(clientID)) {
                    Socket s = null;
                    for (Socket s1 : socket_map.keySet()) {
                        if (socket_map.get(s1) == clientID) {
                            s = s1;
                        }
                    }
                    if (s == null) {
                        continue;
                    }
                    
                    String playerName = "";
                    
                    ArrayList<Command> commands = tryRecvMessage(s);
                    
                    if (commands == null)
                        continue;
                    for (Command command : commands) {
                       allCommands.add(command); 
                    }
                }
                
                i += 1;
            }
        }
       
        this.board.doCommands(allCommands);

        
        if (id_new.size() == all_id.size()) {
            this.board.doAttack();
            this.board.endTurn();
            this.board.boardHistory.add(convertBoard2Byte(this.board));
            this.tserver.db.updateBoard(this.board,this.gameID);
            for (int i = 0; i < id_new.size(); i++) {
                Iterator<Socket> socketIter = socket_map.keySet().iterator();
                while (socketIter.hasNext()) {
                    Socket s = socketIter.next();
                    int curID = socket_map.get(s);
                    if (curID == id_new.get(i)) {
                        trySendMessage(client_socket_list.get(i), this.board);
                    }
                }
            }
        }
    }
    public byte[]convertBoard2Byte(Board board) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(board);
        byte[] byteArray = outputStream.toByteArray();
        objectOutputStream.close();
        return byteArray;
    }

    /**
     * Send ready Message to client
     */
    public void sendReadyMsg(Socket socket) {
        boolean b = true;
        trySendMessage(socket, b);
    }

    /**
     * Send gameOver Message to client
     */
    public void sendOverMsg(Socket socket) {
        boolean b = true;
        trySendMessage(socket, b);
    }

    /**
     * Send its board to the specificed client
     */
    public void sendBoard(Socket socket) {
        trySendMessage(socket, this.board);
    }

    /**
     * This function tries to send messages, avoiding one client disconnect
     */
    public <T> void trySendMessage(Socket socket, T message) {
        try {
            net.sendMessage(socket, message);
        } catch (Exception e) {
            client_socket_list.remove(socket);
            if (this.client_socket_list.size() == 0) {
                
                return;
            }
        }
    }

    /**
     * This function tries to receives message, avoiding one client disconnect
     */
    public <T> T tryRecvMessage(Socket socket) {
        T message = null;
        try {
            message = net.recvMessage(socket);
        } catch (Exception e) {
            client_socket_list.remove(socket);
            
            if (tserver != null) {
                for (String n : tserver.nameToID.keySet()) {
                    if (tserver.nameToID.get(n) == this.socket_map.get(socket)) {
                        tserver.logStatus.put(n, false);
                        id_new.remove(this.socket_map.get(socket));
                        socket_map.remove(socket);
                        this.fullKnowers.remove(socket_map.get(socket));
                        
                        return null;
                    }
                }
            }
            if (this.client_socket_list.size() == 0) {
                
                return null;
            }
        }
        return message;
    }

    /**
     * It is for the server to build connection to the client
     */
    public void buildConnection() throws Exception {
        
        for (int i = 0; i < this.id_new.size(); i++) {
            client_socket_list.add(this.server.accept());
            trySendMessage(client_socket_list.get(i), i + 1);
            System.out.println("Server to Client " + (i + 1) + " connection successfull");
        }
    }

    /**
     * This function is for initializing the board for starting game.
     * It gets the classified board.
     *
     * @return Map<String, ArrayList<Territory>>: color and its territory
     * @throws Exception
     */
    public Board getInitalBoard() throws Exception {
        // get 24 territory
        Board startBoard = new UsBoard();
        Integer[] listClientID = new Integer[all_id.size()];
        listClientID = all_id.toArray(listClientID);
        int i = 1;
        int curr_i = i;
        int resource = 0;
        for (int j = 0; j < 24; j++) {
            // set owner id
            startBoard.getTerritorys().get(j).setOwnerID(listClientID[i - 1]);
            // set territory resources
            if (curr_i == i) {
                resource += 10;
            } else {
                resource = 10;
                curr_i = i;
            }
            startBoard.getTerritorys().get(j).setFoodResource(resource);
            startBoard.getTerritorys().get(j).setTechResource(resource);
            if (j % (24 / this.id_new.size()) == (24 / this.id_new.size() - 1)) {
                ++i;
            }
            if (j == 23)
                break;
        }
        ArrayList<Integer> ids = new ArrayList<>();
        for (int k = 0; k < this.id_new.size(); k++) {
            
            ids.add(id_new.get(k));
        }
        startBoard.initializePlayers(ids);
        return startBoard;
    }

    /** This method will reflect the changes of the territorys on server board */
    public void updateBoard(ArrayList<Territory> ts) {
        ArrayList<Territory> currentTerritorys = this.board.getTerritorys();
        ArrayList<String> names = new ArrayList<>();
        for (Territory territory : ts) {
            names.add(territory.name);
        }
        for (int i = 0; i < currentTerritorys.size(); i++) {
            Territory t = currentTerritorys.get(i);
            if (names.contains(t.name)) {
                Territory changedTerritory = ts.get(names.indexOf(t.name));
                t.setDefenderMillitaryPower(changedTerritory.getDefenderMillitaryPower().getLVPower(0));
            }
        }
    }
    /** this function is about process territory
     * While the server received error message from one player, it means the player has not finished unit placement
     * Otherwise,the server will successfully recieved changed territory from player which means place unit finished*/
    public void processTerritory(){
        int i=0;
        int cnt=0;
        String errorMessage="error";
        ArrayList<Integer> finishedOnes=new ArrayList<>();
        ArrayList<Territory> temp=new ArrayList<>();
        while(cnt<this.id_new.size()){
            
            int thisID=socket_map.get(client_socket_list.get(i));
            if(this.fullKnowers.contains(thisID)==false){
                continue;
            }

            Object msg = tryRecvMessage(client_socket_list.get(i));
           
            if (msg == null) {
                System.out.println("It is nulll!!!!!!!!");
                continue;
            }
            if (msg.getClass() != errorMessage.getClass() && msg.getClass() == temp.getClass()
                    && (finishedOnes.contains(i) == false)) {
                
                finishedOnes.add(i);
                ArrayList<Territory> ts = (ArrayList<Territory>) msg;
                cnt += 1;
                updateBoard(ts);
            }
            
            i = (i + 1) % this.id_new.size();
        }
       
        for (int j = 0; j < this.id_new.size(); j++) {
            sendReadyMsg(client_socket_list.get(j));
        }
    }

    /**
     * This function is for the game prepration: buildconnection, let client assign
     * their unit on each territory
     * Remember that the territories are assigned to the player.
     *
     * @throws Exception
     */
    private void gamePrepration() throws Exception {
        while (!this.isFull()) {
            
            System.out.print("2");
        }
        if (this.isFull()) {

            // for (int i = 0; i < this.id_new.size(); i++) {
            // Iterator<Socket> iter=socket_map.keySet().iterator();
            // while(iter.hasNext()){
            // Socket s=iter.next();
            // int sid=socket_map.get(s);
            // if(sid==this.id_new.get(i)){
            // sendBoard(s);
            // }
            // }
            // }

            processTerritory();
            /***
             * store board here
             */
            this.board.boardHistory.add(convertBoard2Byte(this.board));
            this.tserver.db.updateBoard(this.board, this.gameID);
            for (int i = 0; i < this.id_new.size(); i++) {
                Iterator<Socket> iter = socket_map.keySet().iterator();
                while (iter.hasNext()) {
                    Socket s = iter.next();
                    int sid = socket_map.get(s);
                    if (sid == this.id_new.get(i)) {
                        sendBoard(s);
                    }
                }
            }
        }
    }

    /**
     * The main function that playes the game
     */
    public void playGame() throws Exception {
        if(!this.reopen){
            gamePrepration();
        }
        this.socket_map.remove(null);
        while (!this.board.isOver) {
            playOneRound();
            checkWinner();
        }
    }

    /**
     * Close socket
     * 
     * @throws IOException
     */
    public void closeSocket() throws IOException {
        if (this.client_socket_list.size() != 0) {
            for (Socket socket : this.client_socket_list) {
                socket.close();
            }
        }
    }

    /**
     * check if there is a winner, if there is, this.gameOver = true;
     */
    public void checkWinner() {
        int prevId = -1;
        int currId = -1;
        if (this.id_new.size() == 0) {
           
            this.board.isOver = true;
            return;
        }
        for (int i = 0; i < this.board.getTerritorys().size() - 1; i++) {
            prevId = board.getTerritorys().get(i).getOwnerID();
            currId = board.getTerritorys().get(i + 1).getOwnerID();
            if (prevId == currId) {
                continue;
            } else {
                return;
            }
        }
        System.out.println("The winner is " + prevId);
        this.board.isOver = true;
    }
}
