package edu.duke.ece651.team17.server;

import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import edu.duke.ece651.team17.shared.Board;
import edu.duke.ece651.team17.shared.Command;
import edu.duke.ece651.team17.server.Game;
import edu.duke.ece651.team17.shared.Network;
import edu.duke.ece651.team17.shared.Territory;
/**The client handler*/
public class ClientHandler implements Runnable {
    private Socket socket;
    private int player_id;
    private BufferedReader in;
    private real_server server;
    private Game game;
    public Network net;

    public ClientHandler(Socket socket, real_server server) {
        this.socket = socket;
        this.server = server;
        this.net = new Network();
    }

    public void run() {
        try {
            // Handle the login or registration process
            boolean isLoggedIn = false;
            while (!isLoggedIn) {
                Command input = net.recvMessage(socket);
                if (input.getOperationType().equals("Login")) {
                    String username = input.getUsername();
                    String password = input.getPassword();
                    boolean player = server.authenticatePlayer(username, password);
                    if (!player) {
                        server.trySendMessage(socket, "Invalid username or password. Please try again.");
                    }  else {
                        isLoggedIn = true;
                        server.logStatus.put(username, true);
                        server.trySendMessage(socket, "Log in successfull.");
                        server.trySendMessage(socket, server.nameToID.get(username));
                        this.player_id = server.nameToID.get(username);
                    }
                } else if (input.getOperationType().equals("Register")) {
                    String username = input.getUsername();
                    String password = input.getPassword();
                    int player = server.registerPlayer(username, password);
                    
                    if (player < 0) {
                        server.trySendMessage(socket, "Register Failed!!!!.");
                    } else {

                        isLoggedIn = true;
                        if(!server.nameToID.keySet().contains(username)){
                            server.nameToID.put(username, player);
                        server.logStatus.put(username, true);
                        server.trySendMessage(socket, "Account created. Welcome!");
                        server.trySendMessage(socket, player);
                        this.player_id = server.nameToID.get(username);
                        }else{
                            server.trySendMessage(socket, "Account created. Welcome!");
                            this.player_id = server.nameToID.get(username);
                            server.trySendMessage(socket,this.player_id);
                        }
                    }
                } else {
                    break;
                }
            }
           
            // Handle the game joining and game playing process
            boolean isPlaying = false;
            while (true) {
                if (isPlaying) {
                  continue;
                }
               
                Command input = null;
                input = net.recvMessage(socket);
                
                if (input.operationType.equals("Choose")) {

                    
                    int gameId = server.tryRecvMessage(socket);
                    Game thisGame = server.getGameById(gameId);
                    this.game = thisGame;
                    if(this.game.reopen){
                        if(this.game.board.playerIDs.contains(this.player_id)) {

                        }else{
                            server.trySendMessage(socket, "Game is already full. Please try joining a different game.");
                        }
                    }
                    boolean addSuccess = thisGame.addPlayer(this.socket, this.player_id);
                   
                    if (!addSuccess) {
                        server.trySendMessage(socket, "Game is already full. Please try joining a different game.");
                    } else {
                        
                        server.trySendMessage(socket,
                                "You have joined game " + game.getId() + ". Waiting for other players to join.");
                        server.trySendMessage(socket, this.game.board);
                     
                    }
                } else if (input.getOperationType().equals("get")) {
                    ArrayList<Integer> gameIDs = new ArrayList<>();
                    Set<Integer> idSet = server.games.keySet();
                    for (Integer thisKey : idSet) {
                        gameIDs.add(thisKey);
                    }
                    server.trySendMessage(socket, gameIDs);
                } else if (input.operationType.equals("Leave")) {
                    
                    if (game != null) {
                        game.removeClient(this.player_id);
                    }
                    for (String curName : this.server.nameToID.keySet()) {
                        if (this.server.nameToID.get(curName) == this.player_id) {
                            this.server.logStatus.put(curName, false);
                        }
                    }
                    break;
                } else if (input.getOperationType().equals("Create")) {
                    
                    int playerNum = input.getPlayerNum();
                    game = server.createGame(playerNum);
                    
                    server.trySendMessage(socket, game.gameID);
                    
                    game.addPlayer(socket, this.player_id);
                    
                } else if (input.getOperationType().equals("checkFULL")) {
                    int gameID = server.tryRecvMessage(socket);
                    game = this.server.games.get(gameID);
                    
                    boolean isFULL = server.games.get(gameID).isFull();
                    
                    if(isFULL){
                        if(!game.fullKnowers.contains(this.player_id)){
                            game.fullKnowers.add(this.player_id);
                        }
                    }
                    boolean allKnowFULL=true;
                    if(game.fullKnowers.size()!=game.all_id.size()){
                        allKnowFULL=false;
                    }
                    if(allKnowFULL){
                        for(int i=0;i<game.all_id.size();i++){
                            
                            if(!game.fullKnowers.contains(game.all_id.get(i))){
                                allKnowFULL=false;
                                break;
                            }
                        }
                    }

                    isPlaying = isFULL&&allKnowFULL;
                    
                    server.trySendMessage(socket, game.fullKnowers.size()==game.capacity);
                    server.trySendMessage(socket,game.ready);
                    if(!this.game.ready){
                    Board initialBoard = this.game.getInitalBoard();
                    this.game.board.copyBoard(initialBoard);
                    
                }else{
                        
                        if(game.id_new.contains(this.player_id)==false){
                            game.id_new.add(this.player_id);
                         }
                        game.socket_map.put(socket,this.player_id);
                    
                }

                   
                    this.server.trySendMessage(socket, this.game.board);
                    if (isPlaying && game.started == false&&allKnowFULL) {
                        game.fullKnowers.removeAll(game.fullKnowers);
                        game.fullKnowers.add(this.player_id);
                        Thread gameThread = new Thread(game);
                        gameThread.start();
                        game.started = true;
                        break;
                    }
                } else {

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}