package edu.duke.ece651.team17.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import edu.duke.ece651.team17.shared.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class real_server {
  // public ArrayList<Game>gameList;
  public Map<String, String> userAccounts = new HashMap<>();
  public Map<Integer, Game> games = new ConcurrentHashMap<>();
  public int incrementGameID = 0;
  public ArrayList<Socket> client_socket_list;
  public int playerNum;
  public ServerSocket server;
  private int port;
  public BufferedReader reader;
  public PrintStream out;
  public Network net;
  public int gameNumber;
  public int incrementPlayerId = 0;
  private static final int MAX_GAMES = 10;
  private ExecutorService threadPool;
  public HashMap<String, Integer> nameToID;
  public HashMap<String, Boolean> logStatus;
  public Database db;

  /** constructor for server */
  public real_server(int port, BufferedReader reader, PrintStream out, int gameNumber) throws Exception {
    this.db = new Database();
    threadPool = Executors.newFixedThreadPool(MAX_GAMES);
    this.reader = reader;
    this.out = out;
    this.port = port;
    // initialPlayerNumber();
    this.net = new Network();
    this.client_socket_list = new ArrayList<>();
    // this.server = new ServerSocket(port);
    // this.gameList = new ArrayList<>();
    this.gameNumber = gameNumber;
    this.nameToID = new HashMap<>();
    this.logStatus = new HashMap<>();
    initializeGameList();
  }
  public real_server(){

  }
  /** initialize gamelist from database */

  public void initializeGameList() throws Exception {
    HashMap<Integer, Board> boardMap = this.db.retriveBoardMap();
    //initialize nameToID
    this.nameToID = this.db.retriveAllCustomer();
    System.out.println("size is: " + this.nameToID.size());
    //initialize Board
    for (Map.Entry<Integer, Board> entry : boardMap.entrySet()) {
      Integer id = entry.getKey();
      Board board = entry.getValue();
      int playerNum = board.playerIDs.size();
      Game game = new Game(playerNum, reader, out);
      // add player into game, use socket = null for setting up and wait for rejoin
//      for (int i = 0; i < board.playerIDs.size(); i++) {
//        int playerId = board.playerIDs.get(i);
//        game.addPlayer(null, playerId);
//      }
      // set game state ?????????
      game.gameID = id;
      game.started = false;
      game.ready = true;
      game.capacity = playerNum;
      game.board = board;
      game.tserver = this;
      game.reopen = true;
      for(int i : game.socket_map.values()){
        System.out.println("id are:" + i);
      }
      //add game to gamelist
      this.games.put(id, game);
    }
  }

  /** authenticate player */
  public synchronized boolean authenticatePlayer(String username, String password) {
    if (db.validateAccount(username, password)) {
      return true;
    } else {
      return false;
    }
  }

  /** register player */
  public synchronized int registerPlayer(String username, String password) {
    if (db.HasUsername(username)) {
      System.out.println("111111");
      return -1;
    }
    System.out.println("2222222");
    int id = db.storeUsernameAndPassword(username, password);
    System.out.println("3333333");
    return id;

    // if(!userAccounts.containsKey(username)){
    // System.out.println("Register hello!!!!");
    // userAccounts.put(username, password);
    // return true;
    // }
    // return false;
  }

  /** return game id */
  public Game getGameById(int id) {
    return games.get(id);
  }

  /**
   * Send ready Message to client
   */
  public void sendReadyMsg(Socket socket) {
    boolean b = true;
    trySendMessage(socket, b);
  }

  /**
   * Send gameOver Message to client
   */
  public void sendOverMsg(Socket socket) {
    boolean b = true;
    trySendMessage(socket, b);
  }

  public void startGame() {
    try (ServerSocket serverSocket = new ServerSocket(port)) {
      this.server = serverSocket;
      System.out.println("Server started on port " + port);

      while (true) {
        Socket clientSocket = serverSocket.accept();
        System.out.println("Client connected from " + clientSocket.getInetAddress().getHostAddress());
        // Create a new thread to handle the client's requests
        // trySendMessage(clientSocket, incrementPlayerId);
        Thread thread = new Thread(new ClientHandler(clientSocket, this));
        thread.start();
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public Game createGame(int playerNum) throws Exception {
    System.out.println("Create game is called!!!!!!");
    Game game = new Game(playerNum, reader, out);
    // this.gameList.add(game);
    game.tserver = this;
    Board board = game.board;
    int gameID = this.db.storeBoard(board);
    this.games.put(gameID, game);
    game.gameID = gameID;
    System.out.println("Game id is: " + gameID);
    return game;
  }

  /**
   * This function tries to send messages, avoiding one client disconnect
   */
  public <T> void trySendMessage(Socket socket, T message) {
    try {
      net.sendMessage(socket, message);
    } catch (Exception e) {
      this.playerNum--;
      client_socket_list.remove(socket);
      if (this.client_socket_list.size() == 0) {
        System.out.println("No players, game is over");
        return;
      }
    }
  }

  /**
   * This function tries to receives message, avoiding one client disconnect
   */
  public <T> T tryRecvMessage(Socket socket) {
    T message = null;
    try {
      message = net.recvMessage(socket);
    } catch (Exception e) {
      this.playerNum--;
      client_socket_list.remove(socket);
      if (this.playerNum == 0) {
        System.out.println("No players, game is over");
        return null;
      }
    }
    return message;
  }

  /**
   * Close socket
   * 
   * @throws IOException
   */
  public void closeSocket() throws IOException {
    if (this.client_socket_list.size() != 0) {
      for (Socket socket : this.client_socket_list) {
        socket.close();
      }
    }
  }

  
}
