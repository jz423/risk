package edu.duke.ece651.team17.server;

import edu.duke.ece651.team17.shared.Board;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Database {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/risk";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "password";
    public HashMap<String, Integer>retriveAllCustomer(){
        HashMap<String, Integer>map = new HashMap<>();
        try {
            // Connect to the database
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            // Retrieve the byte array from the database
            String sql = "SELECT * FROM customer";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                int id = rs.getInt("id");
                String username = rs.getString("username");
                map.put(username, id);
            }
            conn.close();
            return map;
            // Close the connection
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new HashMap<>();
    }
    public boolean HasUsername(String username){
        try {
            // Connect to the database
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            // Retrieve the byte array from the database
            String sql = "SELECT username FROM customer WHERE username = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, username);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                byte[] customer = rs.getBytes("username");
                conn.close();
                String a = new String(customer);
                System.out.println(a);
                if(customer == null){
                    return false;
                }else{
                    return true;
                }
            }
            // Close the connection
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public int getIdFromUsername(String username){
        try {
            // Connect to the database
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            PreparedStatement pstmt = conn.prepareStatement("SELECT id from customer WHERE username = ? ");
            pstmt.setString(1, username);
            ResultSet rs = pstmt.executeQuery();
            int id = -1000;
            if(rs.next()){
                byte[] idChar = rs.getBytes("id");
                String s = new String(idChar);
                id = Integer.parseInt(s);
            }
            // Close the connection
            conn.close();
            System.out.println("Finish insertion, the return id is: " + id);
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
    public int storeUsernameAndPassword(String username, String password){
        try {
            // Connect to the database
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            // Insert the serialized data into the database
            String sql = "INSERT INTO customer(username, password) VALUES (?, ?)";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            pstmt.executeUpdate();
            System.out.println("finish update");
            int id = getIdFromUsername(username);
            conn.close();
            System.out.println("Finish insertion, the return id is: " + id);
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
    public boolean validateAccount(String username, String password){
        try {
            // Connect to the database
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            // Retrieve the byte array from the database
            String sql = "SELECT username FROM customer WHERE username = ? AND password = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, username);
            pstmt.setString(2, password);
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()){
                byte[] customer = rs.getBytes("username");
                conn.close();
                if(customer == null){
                    return false;
                }else{
                    return true;
                }
            }
            // Close the connection
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
    public ArrayList<Board> retriveBoard(){
        try {
            // Connect to the database
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            // Retrieve the byte array from the database
            String sql = "SELECT board FROM game";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            ArrayList<Board>boards = new ArrayList<>();
            while(rs.next()){
                byte[] board = rs.getBytes("board");
                ByteArrayInputStream bis = new ByteArrayInputStream(board);
                ObjectInputStream ois = new ObjectInputStream(bis);
                Object gameObj = ois.readObject();
                Board ret = (Board) gameObj;
                boards.add(ret);
            }
            // Close the connection
            conn.close();
            return boards;
        } catch (SQLException | ClassNotFoundException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public HashMap<Integer, Board> retriveBoardMap() {
        try {
            // Connect to the database
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
    
            // Retrieve the id and board from the database
            String sql = "SELECT id, board FROM game";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            HashMap<Integer, Board> boardMap = new HashMap<>();
    
            while (rs.next()) {
                int id = rs.getInt("id");
                byte[] boardBytes = rs.getBytes("board");
                ByteArrayInputStream bis = new ByteArrayInputStream(boardBytes);
                ObjectInputStream ois = new ObjectInputStream(bis);
                Object gameObj = ois.readObject();
                Board board = (Board) gameObj;
                boardMap.put(id, board);
            }
    
            // Close the connection
            conn.close();
            return boardMap;
        } catch (SQLException | ClassNotFoundException | IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public int storeBoard(Board board){
        try {
            // Connect to the database
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            // Serialize the board to a byte array
            ByteArrayOutputStream bos1 = new ByteArrayOutputStream();
            ObjectOutputStream oos1 = new ObjectOutputStream(bos1);
            oos1.writeObject(board);
            byte[] bytes1 = bos1.toByteArray();


            // Insert the serialized data into the database
            String sql = "INSERT INTO game(board) VALUES (?) RETURNING id;";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setBytes(1, bytes1);
            ResultSet rs = pstmt.executeQuery();
            int gameID = -1;
            if (rs.next()) {
                gameID = rs.getInt("id");
                System.out.println("Generated ID: " + gameID);
            }
            // Close the connection
            conn.close();
            System.out.println("Finish insertion");
            return gameID;
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public int updateBoard(Board board,int gameID){
        try {
            // Connect to the database
            Connection conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            // Serialize the board to a byte array
            ByteArrayOutputStream bos1 = new ByteArrayOutputStream();
            ObjectOutputStream oos1 = new ObjectOutputStream(bos1);
            oos1.writeObject(board);
            byte[] bytes1 = bos1.toByteArray();


            // Insert the serialized data into the database
            String sql = "UPDATE game SET board = ? WHERE id = ?";
            PreparedStatement pstmt = conn.prepareStatement(sql);
            pstmt.setBytes(1, bytes1);
            pstmt.setInt(2, gameID);
            pstmt.executeUpdate();

            // Close the connection
            conn.close();
            System.out.println("Finish insertion");
            return gameID;
        } catch (SQLException | IOException e) {
            e.printStackTrace();
        }
        return -1;

    }
}
