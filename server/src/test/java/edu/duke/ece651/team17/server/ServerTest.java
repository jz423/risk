package edu.duke.ece651.team17.server;
import org.junit.jupiter.api.Test;

import edu.duke.ece651.team17.shared.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashSet;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class ServerTest{
    @Test
    public void testNum() throws Exception{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        String inputData = "6\n1\ngg\n2\n";
        BufferedReader input = new BufferedReader(new StringReader(inputData)); 
        Server server = new Server(7777, input, output);
        String res = "Please enter the number of players You accept: \nPlease enter a number between 2 to 4\nPlease enter a number between 2 to 4\nPlease enter a number between 2 to 4\n";
        //assertEquals(bytes.toString(), res);
        server.server.close();
    }
    @Test
    public void test_initial_board() throws Exception{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(1234, input, output);


        assertTrue(server.getInitalBoard().getTerritorys().size() == 24);
        Board b = server.getInitalBoard();
        int playerId1 = 0;
        int playerId2 = 0;
        for(Territory t: b.getTerritorys()){
            if(t.getOwnerID() == 1){
                playerId1++;
            }else{
                playerId2++;
            }
        }
        assertTrue(playerId1 == 12);
    }
    @Test
    public void test_ENDTURN() throws Exception{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(2345, input, output);
        Territory t = new Territory("aa");
        server.board.addTerritory(t);
        server.endTurn();
        assertEquals(server.board.getTerritory(t).getDefenderMillitaryPower().getMinPower(), 0);
        String ans = "Please enter the number of players You accept: \nOne turn end!!!!!!!!\n";
        //assertEquals(ans, bytes.toString());
        server.server.close();
    }
    @Test
    public void testCheckWinner() throws Exception{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(3456, input, output);
        String ans = "Please enter the number of players You accept: \nServer connected\nServer to Client connection successfull\nServer to Client connection successfull\n";
        Territory t1 = new Territory("aa");
        Territory t2 = new Territory("bb");
        t1.setOwnerID(1);
        t2.setOwnerID(2);
        t1.addNeighbour(t2);
        t2.addNeighbour(t1);
        server.board.addTerritory(t1);
        server.board.addTerritory(t2);
        server.checkWinner();
        assertTrue(server.board.isOver == false);
        t2.setOwnerID(1);
        server.checkWinner();
        assertTrue(server.board.isOver == true);
        server.server.close();
    }
   @Test
   public void testConnection() throws Exception{
       Network net = mock(Network.class);
       doNothing().when(net).sendMessage(any(), any());
       ArrayList<Socket> list = mock(ArrayList.class);
       when(list.add(any())).thenReturn(true);
       ServerSocket ss = mock(ServerSocket.class);
       Socket newSocket1 = mock(Socket.class);
       when(ss.accept()).thenReturn(newSocket1);
       ByteArrayOutputStream bytes = new ByteArrayOutputStream();
       PrintStream output = new PrintStream(bytes, true);
       BufferedReader input = new BufferedReader(new StringReader("2\n")); 
       Server server = new Server(5678, input, output);
       bytes.reset();
       server.server = ss;
       server.net = net;
       server.buildConnection();
       ss.close();
       server.server.close();
       newSocket1.close();
       //assertEquals("Server connected\nServer to Client connection successfull\nServer to Client connection successfull\n", bytes.toString());;
   }
    @Test
    public void testTrySendMessage() throws Exception{
        Network net = mock(Network.class);
        doNothing().when(net).sendMessage(any(), any());
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(6789, input, output);
        server.net = net;
        Socket socket = mock(Socket.class);
        server.trySendMessage(socket, null);
       // assertEquals("Please enter the number of players You accept: \n", bytes.toString());
        bytes.reset();
        server.sendBoard(socket);
        //assertEquals("", bytes.toString());
        bytes.reset();
        server.sendOverMsg(socket);
        //assertEquals("", bytes.toString());
        bytes.reset();
        server.sendReadyMsg(socket);
        //assertEquals("", bytes.toString());
        bytes.reset();
        server.client_socket_list.add(socket);
        server.trySendMessage(socket, null);
        //assertEquals("", bytes.toString());
        bytes.reset();
        Socket socket2 = new Socket();
        server.client_socket_list.add(socket);
        //assertEquals("", bytes.toString());
        bytes.reset();
        server.client_socket_list = new ArrayList<>();
        server.client_socket_list.add(socket);
        server.client_socket_list.add(socket2);
        server.trySendMessage(socket, null);
        //assertEquals("", bytes.toString());
        bytes.reset();
        doThrow(NullPointerException.class).when(net).sendMessage(any(), any());
        server.client_socket_list = new ArrayList<>();
        server.client_socket_list.add(socket);
        server.trySendMessage(socket, null);
        //assertEquals("No players, game is over\n", bytes.toString());
        socket.close();
        server.server.close();
    }

    @Test
    public void testTryRecvMessage() throws Exception{
        Network net = mock(Network.class);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(8907, input, output);
        server.client_socket_list = new ArrayList<>();
        String res2 = server.tryRecvMessage(new Socket());
        //assertEquals(bytes.toString(), "Please enter the number of players You accept: \n");
        bytes.reset();
        doThrow(Exception.class).when(net).recvMessage(any());
        server.client_socket_list = new ArrayList<>();
        Socket s1 = new Socket();
        Socket s2 = new Socket();
        server.client_socket_list.add(s1);
        server.client_socket_list.add(s2);
        String a = server.tryRecvMessage(s1);
        String b = server.tryRecvMessage(s2);
        //assertEquals(bytes.toString(), "No players, game is over\n");
        server.server.close();
    }
    @Test
    void testTryRecvMessage2() throws Exception{
        Network net = mock(Network.class);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(7362, input, output);
        server.client_socket_list = new ArrayList<>();
        when(net.recvMessage(any())).thenReturn("aa");
        server.net = net;
        Socket socket = mock(Socket.class);
        server.client_socket_list = new ArrayList<>();
        server.client_socket_list.add(socket);
        String res = server.tryRecvMessage(socket);
        assertEquals(res, "aa");
        socket.close();
        server.server.close();
    }

    public Board test_board(){
        Board b=new Board();
        Territory one=new Territory("A");
        Territory two=new Territory("B");
        // Territory three=new Territory("C");
        one.addNeighbour(two);
        two.addNeighbour(one);
        //two.addNeighbour(three);
        //three.addNeighbour(two);
       // one.addNeighbour(three);
        //three.addNeighbour(one);
        b.addTerritory(one);
        b.addTerritory(two);
       // b.addTerritory(three);
        one.setOwnerID(1);
        two.setOwnerID(2);
        //three.setOwnerID(2);
        ArrayList<Integer> ids=new ArrayList<>();
        for(int i=1;i<=2;i++){
            ids.add(i);
        }
        one.initializeProjectedPower(ids);
        two.initializeProjectedPower(ids);
        //three.initializeProjectedPower(ids);
        return b;
    }
    @Test
    void test_oneMoveRound() throws Exception{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(6372, input, output);
        bytes.reset();
        server.board = test_board();
        ArrayList<Command>moveList = new ArrayList<>();
        ArrayList<Command>attackList = new ArrayList<>();
        ArrayList<Territory> t = server.board.getTerritorys();
        Command move = new Command(1, "move", t.get(0), t.get(0), 50);
        Command attack = new Command(1, "attack",t.get(0), t.get(1), 30);
        moveList.add(move);
        attackList.add(attack);
        Socket s1 = mock(Socket.class);
        Socket s2 = mock(Socket.class);
        server.client_socket_list.add(s1);
        server.client_socket_list.add(s2);
        Network net = mock(Network.class);
        when(net.recvMessage(s1)).thenReturn(moveList);
        when(net.recvMessage(s2)).thenReturn(attackList);
        doNothing().when(net).sendMessage(any(), any());
        server.net = net;
        server.playOneRound();
//       assertEquals(bytes.toString(), "The units to be moved is invalid, should be smaller than your source defense units and be larger or equal to 0.\n"+
//       "The unit you you want attack is larger than the territory, which must be less or equals to 0! Please enter again:\n"
//       +"One turn end!!!!!!!!\n");
        server.server.close();
    }

    @Test
    void test_updateBoard() throws Exception{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(9732, input, output);
        bytes.reset();
        server.board = test_board();
        ArrayList<Territory>list = new ArrayList<>();
        list.add(server.board.getTerritorys().get(0));
        server.updateBoard(list);
    }
    @Test
    void testPlayGame1() throws Exception{
        Network net = mock(Network.class);
        doNothing().when(net).sendMessage(any(), any());
        ArrayList<Socket> list = mock(ArrayList.class);
        when(list.add(any())).thenReturn(true);
        ServerSocket ss = mock(ServerSocket.class);
        Socket newSocket1 = mock(Socket.class);
        Socket newSocket2 = mock(Socket.class);
        list.add(newSocket1);
        list.add(newSocket2);
        when(ss.accept()).thenReturn(newSocket1);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(8458, input, output);
        server.client_socket_list = list;
        bytes.reset();
        server.server = ss;
        server.net = net;
        doNothing().when(net).sendMessage(any(), any());
        ArrayList<Territory>list_send = new ArrayList<>();
        when(net.recvMessage(any())).thenThrow(NullPointerException.class);

       server.playGame();

        // server.playGame();
    }
    @Test
    void testCloseSocket() throws Exception{
        Socket socket1 = new Socket();
        Socket socket2 = new Socket();
        ArrayList<Socket>socket_list = new ArrayList<>();
        socket_list.add(socket1);
        socket_list.add(socket2);
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(3927, input, output);
        server.client_socket_list = socket_list;
        server.closeSocket();
    }
    @Test
    void testProcess_Territory() throws Exception{
        Network net = mock(Network.class);
        doNothing().when(net).sendMessage(any(), any());
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        PrintStream output = new PrintStream(bytes, true);
        BufferedReader input = new BufferedReader(new StringReader("2\n")); 
        Server server = new Server(8263, input, output);
        server.board = server.getInitalBoard();
        server.net = net;
        ArrayList<Socket>list_socket = mock(ArrayList.class);
        Socket s1 = new Socket();
        Socket s2 = new Socket();
        list_socket.add(s1);
        list_socket.add(s2);
        server.client_socket_list = list_socket;
        ArrayList<Territory>list = new ArrayList<>();
        Territory t1 = server.board.getTerritorys().get(0);
        Territory t2 = server.board.getTerritorys().get(1);
        t1.setOwnerID(1);
        t2.setOwnerID(2);
        list.add(t1);
        list.add(t2);
        when(net.recvMessage(any())).thenReturn(list);
        server.processTerritory();
    }
}
