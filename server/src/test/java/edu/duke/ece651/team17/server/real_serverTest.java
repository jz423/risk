package edu.duke.ece651.team17.server;
import org.junit.jupiter.api.Test;

import edu.duke.ece651.team17.shared.*;

import java.net.Socket;
import java.util.ArrayList;


import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class real_serverTest{
    @Test
    public void testAuthenticatPlayer() throws Exception{
        System.out.println("create server!!!!!!!");
        real_server server = mock(real_server.class);
        System.out.println("server object created yes!!!!!!!");
        server.db=mock(Database.class);
        System.out.println("server created!!!!!!!!!");
        when(server.db.validateAccount(anyString(),anyString())).thenReturn(true);
        System.out.println("test database");
        System.out.println(server.authenticatePlayer("a", "b"));
        System.out.println(server.authenticatePlayer("a", "b"));
        System.out.println(server.authenticatePlayer("a", "c"));
    }
    @Test
    public void testRegisterPlayer() throws Exception{
//        real_server server = new real_server(0, null, null, 0);
        real_server server=new real_server();
        server.db=mock(Database.class);
        when(server.db.HasUsername(anyString())).thenReturn(true);
        System.out.println(server.registerPlayer("a", "b") > 0);
        when(server.db.HasUsername(anyString())).thenReturn(false);
        System.out.println(server.registerPlayer("a", "c") < 0);
    }
    @Test
    public void testGetGameById() throws Exception{
//        real_server server = new real_server(0, null, null, 0);
        real_server server=new real_server();
        Game g = new Game(0, null, null);
        server.games.put(1,g);
        assertSame(server.getGameById(1), g); 
    }
    @Test
    public void testCreateGame() throws Exception{
//        real_server server = new real_server(0, null, null, 0);
        real_server server=new real_server();
        server.db=mock(Database.class);
        when(server.db.storeBoard(new Board())).thenReturn(0);
        Game game = server.createGame(3);
        System.out.println(server.games.size() == 1);
    }
    @Test
    public void testTrySendMessage() throws Exception{
        real_server server = new real_server();
        server.client_socket_list=new ArrayList<>();
        Network net1 = mock(Network.class);
        doNothing().when(net1).sendMessage(any(), any());
        server.trySendMessage(null, null);
        Socket socket1 = new Socket();
        Socket socket2 = new Socket();
        doThrow(Exception.class).when(net1).sendMessage(any(), any());
        server.client_socket_list.add(socket1);
        server.client_socket_list.add(socket2);
        server.playerNum = 2;
        server.trySendMessage(socket1, null);
        System.out.println("here comes it!!!!!!!!!!!");
        server.trySendMessage(socket2, null);
        System.out.println("here comes that!!!!!!!!!!!");
    }
    @Test
    public void testTryRecvMessage() throws Exception{
//        real_server server = new real_server(0, null, null, 0);
        real_server server=new real_server();
        server.client_socket_list=new ArrayList<>();
        Network net1 = mock(Network.class);     
        when(net1.recvMessage(any())).thenReturn("aaa");
        server.net = net1;
        Socket socket1 = new Socket();
        Socket socket2 = new Socket();
        server.client_socket_list.add(socket1);
        server.client_socket_list.add(socket2);
        server.playerNum = 2;
        assertEquals("aaa", server.tryRecvMessage(socket1));
        when(net1.recvMessage(any())).thenThrow(Exception.class);
        assertTrue(server.tryRecvMessage(socket2) == null);
        // assertTrue(server.tryRecvMessage(socket1) == null);
    }
    @Test
    public void testCloseSocket() throws Exception{
        real_server server=new real_server();
//        real_server server = new real_server(0, null, null, 0);
        server.client_socket_list=new ArrayList<>();
        server.closeSocket();
        Socket socket1 = new Socket();
        Socket socket2 = new Socket();
        server.client_socket_list.add(socket1);
        server.client_socket_list.add(socket2);
        server.closeSocket();
    }
}