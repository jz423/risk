package edu.duke.ece651.team17.server;
import edu.duke.ece651.team17.shared.Board;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;

import javax.xml.crypto.Data;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseTest {
    @Test
    public void testGetCustomer(){
        Database db = new Database();
        HashMap<String, Integer>map = db.retriveAllCustomer();
        assertFalse(map.size() == 0);
    }
    @Test
    public void testGetID(){
        Database db = new Database();
        int a = db.getIdFromUsername("111");
        System.out.println(a);
        assertTrue(a == -1000);
    }
    @Test
    public void test() {
        Database db = new Database();
        boolean a = db.HasUsername("12345");
        db.storeUsernameAndPassword("12345","54321");
        boolean b = db.HasUsername("12345");
        assertFalse(a);
        assertTrue(b);
        boolean c = db.validateAccount("12345","54321");
        assertTrue(c);
        boolean d = db.validateAccount("12345","12345");
        assertFalse(d);
    }
    @Test
    public void test2(){
        Database db = new Database();
        Board b1 = new Board();
        ArrayList<Integer>ids = new ArrayList<>();
        ids.add(0);
        ids.add(1);
        ids.add(2);
        b1.playerIDs = ids;


        Board b2 = new Board();
        ArrayList<Integer>ids2 = new ArrayList<>();
        ids2.add(3);
        ids2.add(4);
        b2.playerIDs = ids2;
        db.storeBoard(b1);
        db.storeBoard(b2);
        ArrayList<Board>boards = db.retriveBoard();
        assertTrue(boards.size() == 2);
        Board board1 = boards.get(0);
        Board board2 = boards.get(1);
        assertTrue(board1.playerIDs.size() == 3);
        assertTrue(board2.playerIDs.size() == 2);

    }

    @Test
    public void test3(){
      Board b = new Board();
      Database db = new Database();
      int id = db.storeBoard(b);
      int update_id = db.updateBoard(b, id);
      HashMap<Integer, Board>map = db.retriveBoardMap();

    }

}
