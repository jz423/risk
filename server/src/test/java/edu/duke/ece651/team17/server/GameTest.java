package edu.duke.ece651.team17.server;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import edu.duke.ece651.team17.shared.*;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import static org.mockito.Mockito.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class GameTest{
    @Test
    public void test_closeSocket() throws Exception{
        Game game = new Game(0, null, null);
        ArrayList<Socket>list = new ArrayList<>();
        Socket s1 = new Socket();
        Socket s2 = new Socket();
        list.add(s1);
        list.add(s2);
        game.client_socket_list = list;
        game.closeSocket();
    }
    @Test
    public void test_trySend() throws Exception{

        Game game = new Game(0, null, null);
        Network net1 = mock(Network.class);
        game.net = net1;
        doNothing().when(net1).sendMessage(any(), any());
        game.trySendMessage(null, null);
        Socket socket1 = new Socket();
        Socket socket2 = new Socket();
        doThrow(Exception.class).when(net1).sendMessage(any(), any());
        game.client_socket_list.add(socket1);
        game.client_socket_list.add(socket2);
        game.trySendMessage(socket1, null);
        game.trySendMessage(socket2, null);
    }
    @Test
    public void testTryRecvMessage() throws Exception{
        Game game = new Game(0, null, null);
        Network net1 = mock(Network.class);     
        when(net1.recvMessage(any())).thenReturn("aaa");
        game.net = net1;
        Socket socket1 = new Socket();
        Socket socket2 = new Socket();
        game.client_socket_list.add(socket1);
        game.client_socket_list.add(socket2);
        assertEquals("aaa", game.tryRecvMessage(socket1));
        when(net1.recvMessage(any())).thenThrow(Exception.class);
        assertTrue(game.tryRecvMessage(socket2) == null);
        // assertTrue(server.tryRecvMessage(socket1) == null);
    }
    @Test
    public void test_checkWinner() throws Exception{
        Game game = new Game(0, null, null);
        game.checkWinner();
        game.id_new = new ArrayList<>();
        game.id_new.add(1);
        game.checkWinner();
    }
    @Test
    public void test_Game() throws Exception{
        Game game = new Game(0, null, null);
        Territory t1 = new Territory("a");
        Territory t2 = new Territory("b");
        Board b = new Board();
        b.addTerritory(t1);
        b.addTerritory(t2);
        game.board = b;
        ArrayList<Territory>list = new ArrayList<>();
        list.add(t1);
        list.add(t2);
        game.updateBoard(list);
    }
    @Test
    public void test_buildConnecion() throws Exception{
        Game game = new Game(0, null, null);
        ArrayList<Integer>list = new ArrayList<>();
        list.add(1);
        // list.add(2);
        game.id_new = list;
        Network net = mock(Network.class);     
        doNothing().when(net).sendMessage(any(), any());
        ServerSocket server = mock(ServerSocket.class);
        game.server = server;
        Socket socket = new Socket();
        when(server.accept()).thenReturn(socket);
        game.buildConnection();
    }
    @Test
    public void test_trySendReady() throws Exception{
        Game game = new Game(0, null, null);
        Network net1 = mock(Network.class);
        game.net = net1;
        doNothing().when(net1).sendMessage(any(), any());
        game.trySendMessage(null, null);
        Socket socket1 = new Socket();
        Socket socket2 = new Socket();
        doThrow(Exception.class).when(net1).sendMessage(any(), any());
        game.client_socket_list.add(socket1);
        game.client_socket_list.add(socket2);
        game.sendReadyMsg(socket1);
        game.sendReadyMsg(socket2);
    }
    @Test
    public void test_sendOverMsg() throws Exception{
        Game game = new Game(0, null, null);
        Network net1 = mock(Network.class);
        game.net = net1;
        doNothing().when(net1).sendMessage(any(), any());
        game.trySendMessage(null, null);
        Socket socket1 = new Socket();
        Socket socket2 = new Socket();
        doThrow(Exception.class).when(net1).sendMessage(any(), any());
        game.client_socket_list.add(socket1);
        game.client_socket_list.add(socket2);
        game.sendOverMsg(socket1);
        game.sendOverMsg(socket2);
    }
    @Test
    public void test_board() throws Exception{
        Game game = new Game(0, null, null);
        Network net1 = mock(Network.class);
        Board b = new Board();
        game.board = b;
        game.net = net1;
        doNothing().when(net1).sendMessage(any(), any());
        game.trySendMessage(null, null);
        Socket socket1 = new Socket();
        Socket socket2 = new Socket();
        doThrow(Exception.class).when(net1).sendMessage(any(), any());
        game.client_socket_list.add(socket1);
        game.client_socket_list.add(socket2);
        game.sendBoard(socket1);
        game.sendBoard(socket2);
    }
    @Test
    public void test_isFull() throws Exception{
        Game game = new Game(0, null, null);
        game.capacity = 1;
        ArrayList<Integer>list = new ArrayList<>();
        list.add(1);
        game.all_id = list;
        assertTrue(game.isFull());
        game.capacity = 2;
        assertFalse(game.isFull());
    }
    @Test
    public void test_addPlayer() throws Exception{
        Game game = new Game(0, null, null);
        game.capacity = 1;
        ArrayList<Integer>list = new ArrayList<>();
        list.add(1);
        game.all_id = list;
        assertFalse(game.addPlayer(null, 0));
        game.capacity = 2;
        assertTrue(game.addPlayer(null, 0));
        game.capacity = 0;
        assertTrue(game.addPlayer(null, 1));
    }
    @Test
    public void test_getID() throws Exception{
        Game game = new Game(0, null, null);
        game.gameID = 1;
        assertTrue(game.getId() == 1);
    }
    @Test
    public void test_removeClient() throws Exception{
        Game game = new Game(0, null, null);
        ArrayList<Integer>list = new ArrayList<>();
        list.add(1);
        game.all_id = list;
        game.removeClient(1);
        game.removeClient(0);
        HashMap<Socket, Integer>map =new HashMap<>();
        Socket s1 = new Socket();
        Socket s2 = new Socket();
        game.client_socket_list.add(s1);
        game.client_socket_list.add(s2);
        map.put(s1, 0);
        map.put(s2, 1);
        game.removeClient(1);
    }
    @Test
    public void test_playOneRound() throws Exception{
        Game game = new Game(0, null, null);    
        Board b = mock(Board.class);
        Network net1 = mock(Network.class);
        doNothing().when(b).prepareForAtack(any());
        when(b.upgradeTech(any())).thenReturn(true);
        when(b.upgradeUnit(any())).thenReturn(true);
        doNothing().when(b).doAttack();
        doNothing().when(b).endTurn();
        game.board = b;
        doNothing().when(net1).sendMessage(any(), any());
        game.net = net1;
        ArrayList<Command>list = new ArrayList<>();
        Command c1 = new Command();
        Command c2 = new Command();
        Command c3 = new Command();
        Command c4 = new Command();
        Command c5 = new Command();
        c1.moveType = "move";
        c2.moveType = "attach";
        c3.moveType = "research";
        c4.moveType = "upgrade";
        c5.moveType = "upgrassssde";
        list.add(c1);
        list.add(c2);
        list.add(c3);
        list.add(c4);
        list.add(c5);
        when(net1.recvMessage(any())).thenReturn(list);
        HashMap<Socket, Integer>map =new HashMap<>();
        Socket s1 = new Socket();
        Socket s2 = new Socket();
        game.client_socket_list.add(s1);
        game.client_socket_list.add(s2);
        map.put(s1, 0);
        map.put(s2, 1);
        game.socket_map = map;
        ArrayList<Integer>newID = new ArrayList<>();
        newID.add(0);
        newID.add(1);
        game.id_new = newID;
        game.playOneRound();
    }
    @Test
    public void test_remove_client() throws Exception{
        Game game = new Game(2, null, null);
        game.id_new.add(1);
        game.id_new.add(2);
        game.id_new.add(4);
        game.removeClient(3);
        assertEquals(3,game.id_new.size());
        game.removeClient(2);
        assertEquals(2,game.id_new.size());
    }

}